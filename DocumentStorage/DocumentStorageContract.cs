﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace DocumentStorage
{
    [ServiceContract]
    interface IDocumentStorageContract
    {
        [OperationContract]
        IEnumerable<Document> GetDocuments(string path);

        [OperationContract]
        Document GetDocument(string path, Guid Id);

        [OperationContract]
        Document AddDocument(string path, Guid Id, string content);

        [OperationContract]
        Document UpdateDocument(string path, Guid Id, string content);

        [OperationContract]
        void DeleteDocument(string path, Guid Id);
    }
}
