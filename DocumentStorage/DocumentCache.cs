﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DocumentStorage
{
    public class DocumentsCache
    {
        internal static object _fileLockObject = new object();
        internal static object FileLock
        {
            get
            {
                return _fileLockObject; 
            }
        }

        private string _location = "\\\\localhost\\";
        internal string Location
        {
            get
            {
                return _location; 
            }
        }

        private string _extension = ".txt";
        internal string Extension
        {
            get
            {
                return _extension; 
            }            
        }





        public DocumentsCache(string location = "", string extension = "")
        {
            if (!string.IsNullOrWhiteSpace(location))
            {
                location = location.Trim(); 

                if (!location.EndsWith("\\"))
                    location = location + "\\";

                _location = location;
            }

            if (!string.IsNullOrWhiteSpace(extension))
            {
                extension = extension.Trim(); 

                if (!extension.StartsWith("."))
                    extension = "." + extension;
 
                _extension = extension;
            }
        }






        private Dictionary<string, string> _cache;
        private object _lockCacheAccess = new object();
        internal Dictionary<string, string> Cache
        {
            get
            {
                if (_cache == null)
                {
                    lock (_lockCacheAccess)
                    {
                        return _cache ?? (_cache = new Dictionary<string, string>());
                    }
                }

                return _cache; 
            }
        }










        public IEnumerable<Document> GetDocuments(string path)
        {
            try
            {
                return Directory
                      .EnumerateFiles(GetFullPath(path))
                      .Select(i => new Document(i, this));

            } catch (Exception) 
            {
                return new List<Document>();
            }
        }

        public Document GetDocument(string path, Guid Id)
        {
            return new Document(GetFullFileName(GetFullPath(path), Id, Extension), this);
        }

        public Document AddDocument(string path, Guid Id, string content)
        {
            return new Document(GetFullPath(path), Id, content, this);
        }

        public Document UpdateDocument(string path, Guid Id, string content)
        {
            return new Document(GetFullPath(path), Id, content, this);
        }
        public void DeleteDocument(string path, Guid Id)
        {
            string fileName = GetFullFileName(GetFullPath(path), Id, Extension);

            lock (FileLock)
                if (File.Exists(fileName))
                    File.Delete(fileName);

            lock (this)
            {
                var key = Document.GetFileKey(fileName);
                if (Cache.ContainsKey(key))
                    Cache.Remove(key);
            }
        }







        private static string GetPath(string path)
        {
            path = path.Trim();

            if (!path.EndsWith("\\"))
                path = path + "\\";

            return path;
        }

        internal string GetFullPath(string path)
        {
            return string.Format("{0}{1}", Location, path);
        }
        internal static string GetFullFileName(string path, Guid Id, string extension)
        {
            return string.Format("{0}{1}{2}", GetPath(path), Id.ToString(), extension);
        }
    
    }
}
