﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentStorage
{
    class Program
    {
        static void Main(string[] args)
        {
            using (System.ServiceModel.ServiceHost host = new System.ServiceModel.ServiceHost(typeof(DocumentStorage)))
            {
                host.Open();

                Console.WriteLine("Сервис доступа к файловому хранилищу запущен.");
                Console.WriteLine("Нажмите <Q> для остановки сервиса.");

                while (Console.ReadKey().Key != ConsoleKey.Q) { }

                host.Close();
            }
        }
    }
}
