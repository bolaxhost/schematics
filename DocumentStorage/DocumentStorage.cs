﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace DocumentStorage
{
    public class DocumentStorage : IDocumentStorageContract
    {
        private static DocumentsCache _cache = null;
        private static object _lockCacheAccess = new Object();

        public DocumentStorage()
        {
        }
         
        private DocumentsCache Cache
        {
            get
            {
                if (_cache == null)
                {
                    string location = string.Empty;
                    try
                    {
                        location = ConfigurationManager.AppSettings["storageLocation"];
                    }
                    catch { }

                    lock (_lockCacheAccess)
                    {
                        if (_cache == null)
                            _cache = new DocumentsCache(location);
                    }
                }

                return _cache; 
            }
        }

        public IEnumerable<Document> GetDocuments(string path)
        {
            return this.Cache.GetDocuments(path);
        }

        public Document GetDocument(string path, Guid Id)
        {
            return this.Cache.GetDocument(path, Id);
        }

        public Document AddDocument(string path, Guid Id, string content)
        {
            return this.Cache.AddDocument(path, Id, content);
        }

        public Document UpdateDocument(string path, Guid Id, string content)
        {
            return this.Cache.UpdateDocument(path, Id, content); 
        }

        public void DeleteDocument(string path, Guid Id)
        {
            this.Cache.DeleteDocument(path, Id); 
        }
    }
}
