﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DocumentStorage
{
    public class Document
    {
        internal Document(string fullName, DocumentsCache cache)
        {
            try
            {
                Id = Guid.Parse(Path.GetFileNameWithoutExtension(fullName));
            }
            catch (Exception ex)
            {
                throw new InvalidDataException("Некоректное имя файла, ожидается GUID", ex);
            }

            string key = GetFileKey(fullName); 

            lock(cache)
            {
                if (cache.Cache.ContainsKey(key))
                {
                    Content = cache.Cache[key];
                    return;
                }
            }

            //File reading...
            lock(DocumentsCache.FileLock)
            {
                Content = File.ReadAllText(fullName);
            }

            lock (cache)
            {
                cache.Cache[key] = Content;
            }
        }

        internal Document(string path, Guid Id, string content, DocumentsCache cache)
        {
            lock (DocumentsCache.FileLock)
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path); 
            }

            string fileName = DocumentsCache.GetFullFileName(path, Id, cache.Extension);

            lock (DocumentsCache.FileLock)
            {
                File.WriteAllText(fileName, content);
            }

            lock (cache)
            {
                string key = GetFileKey(fileName);

                if (cache.Cache.ContainsKey(key))
                    cache.Cache.Remove(key);

                cache.Cache.Add(key, content);  
            }
        }

        public Document()
        {

        }
        public Guid Id { get; set; }
        public string Content { get; set; }


        public static string GetFileKey(string fullName)
        {
            return Path.GetFullPath(fullName);
        }
    }
}
