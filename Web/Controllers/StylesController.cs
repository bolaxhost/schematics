﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel.Styles;
using Newtonsoft.Json;
using SchematicsModel.Storage;

namespace Web.Controllers
{
    [RoutePrefix("api/styles")]
    public class StylesController : BaseSchematicsController<Style>
    {
        protected override SchematicsModel.Storage.IStorageCollection<Style> GetDataSet()
        {
            return GetStylesStorage().Styles;
        }

        public override ActionResult Index()
        {
            return base.Index();
        }

        protected override void Commit(IStorageCollection<Style> items)
        {
            items.Commit();
        }


        [HttpGet]
        [Route("list")]
        public override ActionResult GetList()
        {
            try
            {
                var result = GetDataSet().DataSet.Select(i => new { id = i.Id, name = i.Name, @for = i.For, zIndex = i.ZIndex }).OrderBy(i => i.name).ToList();
                return Json(new { list = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item")]
        public override ActionResult GetItem(string id)
        {
            return base.GetItem(id);
        }


        [HttpPut]
        [Route("add")]
        public override ActionResult Add()
        {
            return base.Add();
        }
        
        [HttpPost]
        [Route("update")]
        public override ActionResult Update(string id)
        {
            return base.Update(id);
        }
        
        [HttpDelete]
        [Route("delete")]
        public override ActionResult Delete(string id)
        {
            return base.Delete(id);
        }
    }
}