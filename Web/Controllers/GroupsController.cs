﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel.Net;
using Newtonsoft.Json;
using SchematicsModel.Storage;

namespace Web.Controllers
{
    [RoutePrefix("api/groups")]
    public class GroupsController : BaseSchematicsController<Group>
    {
        protected override SchematicsModel.Storage.IStorageCollection<Group> GetDataSet()
        {
            return GetNetStorage(NetStorageName).Groups;
        }

        public override ActionResult Index()
        {
            return base.Index();
        }
        
        protected override void Commit(IStorageCollection<Group> items)
        {
            SaveNetChanges();
        }

        [HttpGet]
        [Route("list")]
        public override ActionResult GetList()
        {
            return base.GetList();
        }


        [HttpGet]
        [Route("item")]
        public override ActionResult GetItem(string id)
        {
            return base.GetItem(id);
        }

        protected override Group PrepareItemBeforeAdd(Group item)
        {
            item = base.PrepareItemBeforeAdd(item);

            GetNetStorage(NetStorageName).CompleteGroup(item);

            return item;
        }

        [HttpPut]
        [Route("add")]
        public override ActionResult Add()
        {
            return base.Add();
        }

        protected override Group PrepareItemBeforeUpdate(Group item)
        {
            item = base.PrepareItemBeforeAdd(item);

            GetNetStorage(NetStorageName).CompleteGroup(item);

            return item;
        }
        
        [HttpPost]
        [Route("update")]
        public override ActionResult Update(string id)
        {
            return base.Update(id);
        }


        public override Group ValidateDelete(Group item)
        {
            item = base.ValidateDelete(item);

            GetNetStorage(NetStorageName).ValidateDelete(item);

            return item;
        }

        [HttpDelete]
        [Route("delete")]
        public override ActionResult Delete(string id)
        {
            return base.Delete(id);
        }
    }
}