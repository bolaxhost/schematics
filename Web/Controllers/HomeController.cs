﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Styles()
        {
            return RedirectToAction("Index", "Styles");
        }

        public ActionResult States()
        {
            return RedirectToAction("Index", "States");
        }

        public ActionResult NodeTypes()
        {
            return RedirectToAction("Index", "NodeTypes");
        }

        public ActionResult LinkTypes()
        {
            return RedirectToAction("Index", "LinkTypes");
        }

        public ActionResult GroupTypes()
        {
            return RedirectToAction("Index", "GroupTypes");
        }

        public ActionResult Schematics()
        {
            return RedirectToAction("Index", "Schematics");
        }
    }
}