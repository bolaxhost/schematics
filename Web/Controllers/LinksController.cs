﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel.Net;
using Newtonsoft.Json;
using SchematicsModel.Storage;

namespace Web.Controllers
{
    [RoutePrefix("api/links")]
    public class LinksController : BaseSchematicsController<Link>
    {
        protected override SchematicsModel.Storage.IStorageCollection<Link> GetDataSet()
        {
            return GetNetStorage(NetStorageName).Links;
        }

        public override ActionResult Index()
        {
            return base.Index();
        }

        protected override void Commit(IStorageCollection<Link> items)
        {
            SaveNetChanges();
        }

        [HttpGet]
        [Route("list")]
        public override ActionResult GetList()
        {
            return base.GetList();
        }


        [HttpGet]
        [Route("item")]
        public override ActionResult GetItem(string id)
        {
            return base.GetItem(id);
        }

        protected override Link PrepareItemBeforeAdd(Link item)
        {
            item = base.PrepareItemBeforeAdd(item);

            GetNetStorage(NetStorageName).CompleteLink(item);

            return item;
        }

        [HttpPut]
        [Route("add")]
        public override ActionResult Add()
        {
            return base.Add();
        }

        protected override Link PrepareItemBeforeUpdate(Link item)
        {
            item = base.PrepareItemBeforeAdd(item);

            GetNetStorage(NetStorageName).CompleteLink(item);

            return item;
        }
        
        [HttpPost]
        [Route("update")]
        public override ActionResult Update(string id)
        {
            return base.Update(id);
        }

        public override Link ValidateDelete(Link item)
        {
            item = base.ValidateDelete(item);

            GetNetStorage(NetStorageName).ValidateDelete(item);

            return item;
        }

        
        [HttpDelete]
        [Route("delete")]
        public override ActionResult Delete(string id)
        {
            return base.Delete(id);
        }
    }
}