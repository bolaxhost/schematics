﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel.Dictionary;
using Newtonsoft.Json;
using SchematicsModel.Storage;

namespace Web.Controllers
{
    [RoutePrefix("api/states")]
    public class StatesController : BaseSchematicsController<State>
    {
        protected override SchematicsModel.Storage.IStorageCollection<State> GetDataSet()
        {
            return GetDictionaryStorage().States;
        }

        public override ActionResult Index()
        {
            return base.Index();
        }

        protected override void Commit(IStorageCollection<State> items)
        {
            items.Commit(); 
        }


        [HttpGet]
        [Route("list")]
        public override ActionResult GetList()
        {
            return base.GetList();
        }


        [HttpGet]
        [Route("item")]
        public override ActionResult GetItem(string id)
        {
            return base.GetItem(id);
        }


        [HttpPut]
        [Route("add")]
        public override ActionResult Add()
        {
            return base.Add();
        }
        
        [HttpPost]
        [Route("update")]
        public override ActionResult Update(string id)
        {
            return base.Update(id);
        }
        
        [HttpDelete]
        [Route("delete")]
        public override ActionResult Delete(string id)
        {
            return base.Delete(id);
        }
    }
}