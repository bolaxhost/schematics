﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel;
using SchematicsModel.Storage;
using Newtonsoft.Json; 

namespace Web.Controllers
{
    [RoutePrefix("api/schematics")]
    public class SchematicsController : BaseController
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("group-list")]
        public virtual ActionResult GetGroups()
        {
            try
            {
                var result = GetNetStorage(LibraryStorageName).Groups.DataSet.Select(i => new { id = i.Id, name = i.Name, extent = i.Extent }).OrderBy(i => i.name).ToList();
                return Json(new { list = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("compile-extent")]
        public ActionResult CompileExtent(double x1, double y1, double x2, double y2, string mode = "net")
        {
            try
            {                 
                int maxItemsCount = Convert.ToInt32(GetSettingsValue("MaxItemsCountToShow", "0"));

                double offsetx = 0.2 * (x2 - x1);
                double offsety = 0.2 * (y2 - y1);
                var result = SchematicsModel.Compiler.SchematicsCompiler.Compile(
                    GetNetStorage(mode == "lib" ? LibraryStorageName : NetStorageName), 
                    GetStylesStorage(),
                    new double[] { x1 - offsetx, y1 - offsety, x2 + offsetx, y2 + offsety },
                    maxItemsCount > 0 ? (int?)maxItemsCount : null);

                if (result.NodesCount == maxItemsCount || result.LinksCount == maxItemsCount)
                {
                    result.isFull = false;
                }

                var data = SerializeResult(result);

                //Свыше 2 MB
                if (data.Length > 1000000)
                {
                    result.Labels = null;
                    result.Snap = null;

                    if (data.Length > 2000000)
                        result.Active = result.Active.Where(i => i.Geometry != null && i.Geometry.Length > 0 && i.Geometry[0].GeometryType == "LineString").ToArray();   

                    result.isFull = false;

                    data = SerializeResult(result);
                }

                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("compile-within")]
        public ActionResult CompileWithin(double lon, double lat, double xc, double yc, double radius, string mode = "net")
        {
            try
            {
                var result = SchematicsModel.Compiler.SchematicsCompiler.Compile(
                    GetNetStorage(mode == "lib" ? LibraryStorageName : NetStorageName), 
                    GetStylesStorage(), 
                    lon, 
                    lat,
                    xc,
                    yc,
                    radius);

                return Json(new { data = SerializeResult(result) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("compile-style")]
        public ActionResult CompileStyle(string typeId, string stateId, double x, double y)
        {
            try
            {
                var result = SchematicsModel.Compiler.SchematicsCompiler.Compile(
                    GetStylesStorage(),
                    typeId,
                    stateId,
                    new double[] { x, y });

                return Json(new { data = SerializeResult(result) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("copy")]
        public ActionResult Copy(double x1, double y1, double x2, double y2)
        {
            try
            {
                double[] extent = new double[] { x1, y1, x2, y2 };
                var result = GetNetStorage(NetStorageName).Copy(extent);   
                return Json(new { data = SerializeResult(result) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("copy-group")]
        public ActionResult Copy(string id)
        {
            try
            {
                var result = GetNetStorage(LibraryStorageName).Copy(id);
                return Json(new { data = SerializeResult(result) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPut]
        [Route("paste")]
        public virtual ActionResult Paste(double dx, double dy)
        {
            try
            {
                NetScope scope = JsonConvert.DeserializeObject<NetScope>(GetRequestContent());

                var storage = GetNetStorage(NetStorageName);
                storage.Move(scope, dx, dy);
                storage.Paste(scope);

                SaveNetChanges(NetStorageName);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("move")]
        public virtual ActionResult Move(double dx, double dy)
        {
            try
            {
                NetScope scope = JsonConvert.DeserializeObject<NetScope>(GetRequestContent());

                var storage = GetNetStorage(NetStorageName);
                storage.Move(scope, dx, dy);

                foreach (var n in scope.Nodes)
                {
                    storage.Nodes.Update(n); 
                }

                foreach (var l in scope.InternalLinks.Union(scope.ExternalLinks))
                {
                    storage.Links.Update(l);
                }

                foreach (var gr in scope.Groups)
                {
                    storage.Groups.Update(gr);
                }

                //Простое сохранение без постобработок
                storage.Nodes.Commit();
                storage.Links.Commit();
                storage.Groups.Commit();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("rotate")]
        public virtual ActionResult Rotate(double angle)
        {
            try
            {
                NetScope scope = JsonConvert.DeserializeObject<NetScope>(GetRequestContent());

                var storage = GetNetStorage(NetStorageName);
                storage.Rotate(scope, angle);

                foreach (var n in scope.Nodes)
                {
                    storage.Nodes.Update(n);
                }

                foreach (var l in scope.InternalLinks.Union(scope.ExternalLinks))
                {
                    storage.Links.Update(l);
                }

                foreach (var gr in scope.Groups)
                {
                    storage.Groups.Update(gr);
                }

                //Простое сохранение без постобработок
                storage.Nodes.Commit();
                storage.Links.Commit();
                storage.Groups.Commit();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("delete")]
        public virtual ActionResult Delete()
        {
            try
            {
                NetScope scope = JsonConvert.DeserializeObject<NetScope>(GetRequestContent());

                var storage = GetNetStorage(NetStorageName);

                foreach (var n in scope.Nodes)
                {
                    storage.Nodes.Delete(storage.Nodes.DataSet.FirstOrDefault(i => i.Id == n.Id));
                }

                foreach (var l in scope.InternalLinks.Union(scope.ExternalLinks))
                {
                    storage.Links.Delete(storage.Links.DataSet.FirstOrDefault(i => i.Id == l.Id));
                }

                foreach (var gr in scope.Groups)
                {
                    storage.Groups.Delete(storage.Groups.DataSet.FirstOrDefault(i => i.Id == gr.Id));
                }

                SaveNetChanges(NetStorageName);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}