﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel.Net;
using Newtonsoft.Json;
using SchematicsModel.Storage;

namespace Web.Controllers
{
    [RoutePrefix("api/nodes")]
    public class NodesController : BaseSchematicsController<Node>
    {
        protected override SchematicsModel.Storage.IStorageCollection<Node> GetDataSet()
        {
            return GetNetStorage(NetStorageName).Nodes;
        }

        public override ActionResult Index()
        {
            return base.Index();
        }
        
        protected override void Commit(IStorageCollection<Node> items)
        {
            SaveNetChanges();
        }

        [HttpGet]
        [Route("list")]
        public override ActionResult GetList()
        {
            return base.GetList();
        }


        [HttpGet]
        [Route("item")]
        public override ActionResult GetItem(string id)
        {
            return base.GetItem(id);
        }

        protected override Node PrepareItemBeforeAdd(Node item)
        {
            item = base.PrepareItemBeforeAdd(item);

            GetNetStorage(NetStorageName).CompleteNode(item); 

            return item;
        }

        [HttpPut]
        [Route("add")]
        public override ActionResult Add()
        {
            return base.Add();
        }

        protected override Node PrepareItemBeforeUpdate(Node item)
        {
            item = base.PrepareItemBeforeUpdate(item);

            var net = GetNetStorage(NetStorageName);

            net.CompleteNode(item);
            net.UpdateConnectedLinks(item); 

            return item;
        }

        [HttpPost]
        [Route("update")]
        public override ActionResult Update(string id)
        {
            return base.Update(id);
        }

        public override Node ValidateDelete(Node item)
        {
            item = base.ValidateDelete(item);

            GetNetStorage(NetStorageName).ValidateDelete(item);

            return item;
        }
        
        [HttpDelete]
        [Route("delete")]
        public override ActionResult Delete(string id)
        {
            return base.Delete(id);
        }
    }
}