﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchematicsModel;
using SchematicsModel.Storage;
using Newtonsoft.Json; 

namespace Web.Controllers
{
    public abstract class BaseSchematicsController<T> : BaseController
        where T : BaseElement, new()
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        protected abstract IStorageCollection<T> GetDataSet();
        protected abstract void Commit(IStorageCollection<T> items);

        public virtual ActionResult GetList()
        {
            try
            {
                var result = GetDataSet().DataSet.Select(i => new { id = i.Id, name = i.Name }).OrderBy(i => i.name).ToList();  
                return Json(new { list = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        public virtual ActionResult GetItem(string id)
        {
            try
            {
                var result = GetDataSet().DataSet.FirstOrDefault(i => i.Id == id);
                return Json(new { item = SerializeResult(result) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        protected virtual T PrepareItemBeforeAdd(T item)
        {
            return item;
        }

        public virtual ActionResult Add()
        {
            try
            {
                T item = JsonConvert.DeserializeObject<T>(GetRequestContent());
                var items = GetDataSet();

                items.Add(PrepareItemBeforeAdd(item));
                Commit(items);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        protected virtual T PrepareItemBeforeUpdate(T item)
        {
            return item;
        }

        public virtual ActionResult Update(string id)
        {
            try
            {
                var item = JsonConvert.DeserializeObject<T>(GetRequestContent());
                item.Id = id;

                var items = GetDataSet();

                items.Update(PrepareItemBeforeUpdate(item));
                Commit(items);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        public virtual T ValidateDelete(T item)
        {
            return item;
        }

        public virtual ActionResult Delete(string id)
        {
            try
            {
                var items = GetDataSet();

                items.Delete(ValidateDelete(items.DataSet.FirstOrDefault(i => i.Id == id)));
                Commit(items);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}