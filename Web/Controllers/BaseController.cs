﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Text;

using Newtonsoft.Json;
 
using SchematicsModel.Storage; 

namespace Web.Controllers
{
    public abstract class BaseController : Controller
    {
        protected static string GetSettingsValue(string name, string defaultValue = "")
        {
            string result = ConfigurationManager.AppSettings[name];

            if (string.IsNullOrWhiteSpace(result))
                return defaultValue;

            return result;
        }

        private ISchematicsStorage _schematicsStorage = null;
        protected ISchematicsStorage GetSchematicsStorage()
        {
            if (_schematicsStorage != null)
                return _schematicsStorage;

            string storageType = GetSettingsValue("storage", "files");
            if (storageType == "files")
            {
                return _schematicsStorage = new SchematicsModel.Storage.FileStorage.FileDocumentSchematicsStorage();
            } 
            else if (storageType == "arango")
            {
                return _schematicsStorage = new SchematicsModel.Storage.ArangoDB.ArangoDBSchematicsStorage(
                      GetSettingsValue("arangoUrl", "127.0.0.1:8529")
                    , GetSettingsValue("arangoUser", "root")
                    , GetSettingsValue("arangoPassword", "admin")
                    );
            }
            else if (storageType == "mongo")
            {
                return _schematicsStorage = new SchematicsModel.Storage.MongoDB.MongoDBSchematicsStorage();
            }

            throw new ArgumentOutOfRangeException(string.Format("Тип хранилища {0} не поддерживается!", storageType));
        }

        protected INetStorage GetNetStorage(string name = "")
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                name = NetStorageName;
            }

            return GetSchematicsStorage().GetNetStorage(name, GetDictionaryStorage(), GetStylesStorage());
        }
        
        protected IDictionaryStorage GetDictionaryStorage()
        {
            return GetSchematicsStorage().GetDictionaryStorage(DictionaryStorageName);
        }

        protected IStylesStorage GetStylesStorage()
        {
            return GetSchematicsStorage().GetStylesStorage(StylesStorageName);
        }

        protected static string StylesStorageName
        {
            get
            {
                return GetSettingsValue("styles", "schematics_styles");
            }
        }

        protected static string NetStorageName
        {
            get
            {
                return GetSettingsValue("net", "schematics_net");
            }
        }

        protected static string LibraryStorageName
        {
            get
            {
                return GetSettingsValue("library", "schematics_lib");
            }
        }

        protected static string DictionaryStorageName
        {
            get
            {
                return GetSettingsValue("dictionary", "schematics_dictionary");
            }
        }

        public void SaveNetChanges(string name = "")
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                name = NetStorageName;
            }

            GetNetStorage(name).Commit();
        }

        public JsonResult EmptySuccessResult()
        {
            return Json(new
            {
                state = "success"
            });
        }

        public JsonResult ErrorResult(Exception ex)
        {
            return ErrorResult(ex.Message);
        }

        public JsonResult ErrorResult(string message)
        {
            return Json(new
            {
                state = "error",
                message = message
            }, JsonRequestBehavior.AllowGet);
        }

        public string GetRequestContent()
        {
            var req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);

            return new System.IO.StreamReader(req).ReadToEnd();
        }

        public string ExceptionReport(Exception ex)
        {
            StringBuilder report = new StringBuilder();
            report.AppendLine(ex.Message);

            if (ex.InnerException != null)
                report.AppendLine(ExceptionReport(ex.InnerException));

            return report.ToString();
        }

        public string SerializeResult(object result)
        {
            return JsonConvert.SerializeObject(
                                result, 
                                Formatting.None,
                                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

    }
}