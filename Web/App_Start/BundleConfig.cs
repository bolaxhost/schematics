﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/schematics").Include(
                        "~/Scripts/Schematics/forms/ui.js",
                        "~/Scripts/Schematics/forms/style.js",
                        "~/Scripts/Schematics/forms/dictionary.js",
                        "~/Scripts/Schematics/forms/net.js",
                        "~/Scripts/Schematics/ol/ol.js",
                        "~/Scripts/Schematics/ol/layout.js",
                        "~/Scripts/Schematics/ol/style-layout.js",
                        "~/Scripts/Schematics/ol/net-layout.js",
                // "~/Scripts/Schematics/canvas/*.js",
                        "~/Scripts/Schematics/assembly.js"));


            
            
            
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/schematics").Include(
                      "~/Content/Schematics/ol/*.css",
                // "~/Content/Schematics/canvas/*.css",
                      "~/Content/Schematics/*.css"));

        }
    }
}
