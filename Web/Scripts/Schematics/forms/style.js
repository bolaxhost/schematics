﻿"use strict";

console.log("модуль schematics.forms.style.js загружен");

(function () {
    //export
    window.f$api.CreateStyleForm = function (id, onSubmit, onApply, onClear) {
        return new StyleForm(id, onSubmit, onApply, onClear);
    }

    window.f$api.CreateStyleList = function (id, callback) {
        return new StyleList(id, callback);
    }




    //import
    var BaseForm = window.f$api.BaseForm;
    var BaseList = window.f$api.BaseList;






    //implementation

    //-------Форма для редактирования стиля-------
    function StyleForm(id, onSubmit, onApply, onClear) {
        BaseForm.apply(this, arguments);

        var _this = this;

        var buttons = this.appendButtonGroup(this.formDiv, "actions");

        //Добавить или Сохранить
        this.submit = this.appendSubmit(buttons, "Сохранить",
            {
                rules: {
                    "name": "required"
                },
                messages: {
                    "name": "Имя стиля обязательно!"
                }
            },
            function (data) {
                if (onSubmit) {
                    onSubmit(_this.serializeStyle(data));
                }

                _this.loadDefault();
            });

        //Применить
        if (onApply) {
            this.appendButton(buttons, "btn_apply", "Применить", function () {
                onApply(_this.serializeStyle(_this.serialize()));
            })
        }

        //Очистить
        this.appendButton(buttons, "btn_clear", "Очистить", function () {
            if (onClear) {
                onClear();
            }

            _this.loadDefault();
        })



        //Элементы формы
        var panels = this.appendPanelGroup(this.formDiv, "accordion");
        var panelMain = this.appendPanel(panels, "main_panel", "Основные свойства", true);
        var panelStrokeFill = this.appendPanel(panels, "stroke_fill_panel", "Свойства линии и заливки");
        var panelImage = this.appendPanel(panels, "image_panel", "Свойства точки");
        var panelText = this.appendPanel(panels, "text_panel", "Свойства текста");

        this.appendInputGroup(panelMain, "name", "text", "Имя стиля");
        
        this.appendSelectGroup(panelMain, "for", "Назначение", [
            { value: 0, text: "Стиль общего назначения" },
            { value: 1, text: "Стиль узла" },
            { value: 2, text: "Стиль связи" },
            { value: 1001, text: "(Оформление) Выбранные терминалы" },
        ]).find('select').change(function () {
            var _for = $(this).val();
            switch (_for) {
                case "0":
                case "1001":
                    selectNodeType.hide();
                    selectLinkType.hide();
                    selectConnectivityType.hide();
                    break;
                case "1":
                    selectNodeType.show();
                    selectLinkType.hide();
                    selectConnectivityType.show();
                    break;
                case "2":
                    selectNodeType.hide();
                    selectLinkType.show();
                    selectConnectivityType.hide();
                    break;
            }
        });

        var selectNodeType = this.appendSelectGroup(panelMain, "nodeTypeId", "Тип узла", [{ value: "", text: " - не определен - " }], "api/node-types");
        var selectLinkType = this.appendSelectGroup(panelMain, "linkTypeId", "Тип связи", [{ value: "", text: " - не определен - " }], "api/link-types");

        var selectConnectivityType = this.appendSelectGroup(panelMain, "connectivity_type", "Топология точек привязки", [
            { value: 0, text: "Последовательное соединение" },
            { value: 1, text: "Звезда" },
            { value: 2, text: "Произвольный" },
        ]);
        
        selectConnectivityType.find('select').change(function () {
            var _ct = $(this).val();

            if (_this._styleLayout) {
                _this._styleLayout.drawConnectivityTopology(_ct, _this.style.connectivity);
            }

            if (parseInt(_ct || 0) == 2) {
                connectivityText.show();
            } else {
                connectivityText.hide();
            }
        });

        var connectivityText = this.appendTextAreaGroup(panelMain, "connectivity", "Описание топологии", "[n1, n2], [n3, n4], ...");

        this.appendSelectGroup(panelMain, "stateId", "Состояние", [{ value: "", text: "Любое" }], "api/states");
        this.appendInputGroup(panelMain, "zIndex", "number", "Z-Индекс");

        this.appendInputGroup(panelStrokeFill, "stroke_color", "color", "Цвет линии");
        this.appendInputGroup(panelStrokeFill, "stroke_width", "number", "Толщина линии");
        this.appendSelectGroup(panelStrokeFill, "stroke_lineCap", "Окончание линии", [
            { value: "butt", text: "Обрезанное" },
            { value: "round", text: "Круглое" },
            { value: "square", text: "Квадратное" },
        ])
        this.appendSelectGroup(panelStrokeFill, "stroke_lineJoin", "Соединение линии", [
            { value: "bevel", text: "Скошенное" },
            { value: "round", text: "Круглое" },
            { value: "mitter", text: "Угловое" },
        ])
        this.appendInputGroup(panelStrokeFill, "stroke_lineDash", "text", "Штриховка ([a,b,...])");
        this.appendInputGroup(panelStrokeFill, "fill", "checkbox", "Заливка");
        this.appendInputGroup(panelStrokeFill, "fill_color", "color", "Цвет заливки");

        this.appendInputGroup(panelImage, "image_radius", "number", "Радиус");
        this.appendInputGroup(panelImage, "image_stroke_color", "color", "Цвет границы");
        this.appendInputGroup(panelImage, "image_stroke_width", "number", "Толщина линии");
        this.appendInputGroup(panelImage, "image_fill", "checkbox", "Заливка");
        this.appendInputGroup(panelImage, "image_fill_color", "color", "Цвет заливки");

        this.appendSelectGroup(panelText, "label-source", "Источник текста", [
            { value: 0, text: "- не определен -" },
            { value: 1, text: "Значение поля 'Имя'" },
            { value: 2, text: "Значение поля 'Примечание'" },
        ]);
        this.appendSelectGroup(panelText, "text_font", "Шрифт", [
            { value: "Arial", text: "Arial" },
            { value: "Arial Black", text: "Arial Black" },
            { value: "Book Antiqua", text: "Book Antiqua" },
            { value: "Comic Sans MS", text: "Comic Sans MS" },
            { value: "Courier New", text: "Courier New" },
            { value: "Gadget", text: "Gadget" },
            { value: "Geneva", text: "Geneva" },
            { value: "Georgia", text: "Georgia" },
            { value: "bevel", text: "Helvetica" },
            { value: "Impact", text: "Impact" },
            { value: "Lucida Console", text: "Lucida Console" },
            { value: "Lucida Grande", text: "Lucida Grande" },
            { value: "Lucida Sans Unicode", text: "Lucida Sans Unicode" },
            { value: "Monaco", text: "Monaco" },
            { value: "monospace", text: "Monospace" },
            { value: "MS Sans Serif", text: "MS Sans Serif" },
            { value: "MS Serif", text: "MS Serif" },
            { value: "New York", text: "New York" },
            { value: "Palatino", text: "Palatino" },
            { value: "Palatino Linotype", text: "Palatino Linotype" },
            { value: "sans-serif", text: "Sans-serif" },
            { value: "Symbol", text: "Symbol" },
            { value: "Tahoma", text: "Tahoma" },
            { value: "Times New Roman", text: "Times New Roman" },
            { value: "Trebuchet MS", text: "Trebuchet MS" },
            { value: "Verdana", text: "Verdana" },
            { value: "Webdings", text: "Webdings" },
            { value: "Zapf Dingbats", text: "Zapf Dingbats" },
        ])
        this.appendInputGroup(panelText, "text_size", "number", "Размер текста");
        this.appendInputGroup(panelText, "text_color", "color", "Цвет текста");
        this.appendSelectGroup(panelText, "text_textAlign", "Выравнивание текста", [
            { value: "left", text: "По левому краю" },
            { value: "right", text: "По правому краю" },
            { value: "center", text: "По центру" },
            { value: "end", text: "По концу" },
            { value: "start", text: "По началу" },
        ])

        this.appendSelectGroup(panelText, "text_textBaseline", "Базисная линия", [
            { value: "bottom", text: "Снизу" },
            { value: "top", text: "Сверху" },
            { value: "middle", text: "Посередине" },
            { value: "alphabetic", text: "Алфавитный" },
            { value: "hanging", text: "Подвешивание" },
            { value: "ideographic", text: "Идеографический" },
        ])

        this.appendSelectGroup(panelText, "text_rotation", "Поворот", [
            { value: 0, text: "0" },
            { value: (Math.PI / 4), text: "45" },
            { value: (Math.PI / 2), text: "90" },
            { value: (Math.PI / 2 + Math.PI / 4), text: "135" },
            { value: (Math.PI), text: "180" },
            { value: (Math.PI + Math.PI / 4), text: "225" },
            { value: (Math.PI + Math.PI / 2), text: "270" },
            { value: (Math.PI + Math.PI / 2 + Math.PI / 4), text: "315" },
        ])

        //Загрузка по умолчанию
        this.loadDefault()
    }

    StyleForm.prototype = Object.create(BaseForm.prototype);

    StyleForm.prototype.constructor = StyleForm;



    //public
    StyleForm.prototype.setStyleLayout = function (layout) {
        this._styleLayout = layout;
    }

    StyleForm.prototype.getStyleLayout = function (layout) {
        return this._styleLayout;
    }

    StyleForm.prototype.loadDefault = function () {
        this.load(null)
    }

    //Нормализация структуры стиля
    function normalizeStyle(style) {
        if (!style.stroke) {
            style.stroke = {};
        }

        if (!style.fill) {
            style.fill = {};
        }

        if (!style.text) {
            style.text = {};
        }

        if (!style.image) {
            style.image = {};
        }

        if (!style.image.stroke) {
            style.image.stroke = {};
        }

        if (!style.image.fill) {
            style.image.fill = {};
        }

        return style;
    }

    StyleForm.prototype.load = function (style) {
        this.style = normalizeStyle(style || {});

        var connectivity = this.style.connectivity || [];
        var connectivityJSON = JSON.stringify(connectivity);

        if (connectivityJSON.length > 1) {
            connectivityJSON = connectivityJSON.slice(1, connectivityJSON.length - 1);
        }

        if (style) {
            this.deserialize({
                name: this.style.name,
                "for": this.style.for,
                nodeTypeId: this.style.for == 1 ? this.style.type : "",
                linkTypeId: this.style.for == 2 ? this.style.type : "",
                stateId: this.style.state,
                zIndex: this.style.zIndex ? this.style.zIndex : 0,
                stroke_color: this.style.stroke.color,
                stroke_width: this.style.stroke.width,
                stroke_lineCap: this.style.stroke.lineCap,
                stroke_lineJoin: this.style.stroke.lineJoin,
                stroke_lineDash:  this.style.stroke.lineDash ? JSON.stringify(this.style.stroke.lineDash) : "",
                fill: this.style.fill.fill,
                fill_color: this.style.fill.color,
                "label-source": this.style["label-source"],
                text_color: this.style.text.color,
                text_font: this.style.text.font,
                text_size: this.style.text.size,
                text_textAlign: this.style.text.textAlign,
                text_textBaseline: this.style.text.textBaseline,
                text_rotation: this.style.text.rotation,
                image_radius: this.style.image.radius,
                image_stroke_color: this.style.image.stroke.color,
                image_stroke_width: this.style.image.stroke.width,
                image_fill: this.style.image.fill.fill,
                image_fill_color: this.style.image.fill.color,
                connectivity_type: this.style["connectivity-type"],
                connectivity: connectivityJSON,
            });
        } else {
            this.deserialize({
                name: "Новый стиль",
                "for": 0,
                nodeTypeId: "",
                linkTypeId: "",
                stateId: "",
                zIndex: 0,
                stroke_color: "#000000",
                stroke_lineCap: "round",
                stroke_lineJoin: "round",
                stroke_lineDash: undefined,
                stroke_width: 1,
                fill: false,
                fill_color: "#000000",
                "label-source": 0,
                text_color: "#000000",
                text_font: "Courier New",
                text_size: 10,
                text_textAlign: "start",
                text_textBaseline: "alphabetic",
                text_rotation: 0,
                image_radius: 5,
                image_stroke_color: "#000000",
                image_stroke_width: 1,
                image_fill: true,
                image_fill_color: "#000000",
                connectivity_type: 0,
                connectivity: connectivityJSON,
            });
        }

        this.submit.val(this.style.id ? "Сохранить" : "Добавить");

        //Отрисовка топологии связности
        if (this._styleLayout) {
            this._styleLayout.drawConnectivityTopology(this.style["connectivity-type"], this.style.connectivity);
        }
    }

    //protected
    StyleForm.prototype.serializeStyle = function (data) {
        var style = normalizeStyle(this.style || {});

        style.name = data.name;
        style.for = data.for;

        if (style.for == 0) {
            style.type = ""
        } else {
            style.type = (style.for == 1) ? data.nodeTypeId : data.linkTypeId;
        }

        style.state = data.stateId;
        style.zIndex = data.zIndex;

        style.stroke.color = data.stroke_color;
        style.stroke.width = data.stroke_width;
        style.stroke.lineCap = data.stroke_lineCap;
        style.stroke.lineJoin = data.stroke_lineJoin;

        try {
            style.stroke.lineDash = data.stroke_lineDash ? JSON.parse(data.stroke_lineDash) : undefined;
        } catch (e) {
            style.stroke.lineDash = undefined;
        }

        style.fill.fill = data.fill;
        style.fill.color = data.fill_color;

        style["label-source"] = data["label-source"];
        style.text.color = data.text_color;
        style.text.font = data.text_font;
        style.text.size = data.text_size;
        style.text.textAlign = data.text_textAlign;
        style.text.textBaseline = data.text_textBaseline;
        style.text.rotation = data.text_rotation;

        style.image.radius = data.image_radius;
        style.image.stroke.color = data.image_stroke_color;
        style.image.stroke.width = data.image_stroke_width;
        style.image.fill.fill = data.image_fill;
        style.image.fill.color = data.image_fill_color;

        style["connectivity-type"] = data.connectivity_type;

        var connectivity = [];
        try {
            connectivity = JSON.parse("[" + data.connectivity + "]"); 
        } catch(e) {}

        style.connectivity = connectivity;

        return style;
    }















    //-------Список стилей-------
    function StyleList(id, callback) {
        this.addCopy = true;

        BaseList.call(this, id, "api/styles", callback);
    }

    StyleList.prototype = Object.create(BaseList.prototype);

    StyleList.prototype.constructor = StyleList;

    StyleList.prototype.itemMarkup = function (item) {
        return "<span>" +
            "<span class='badge'>" + (item.for == 1 ? "узел" : (item.for == 2 ? "связь" : "")) + "</span>" +
            (item.zIndex && item.zIndex > 0 ? "<span class='label label-info'>" + ((item.zIndex > 0) ? "+" : "") + item.zIndex + "</span>" : "") +
            "&nbsp;" + item.name + "</span>";
    }
})();