﻿"use strict";

console.log("модуль schematics.forms.dictionary.js загружен");

(function () {
    //export
    window.f$api.CreateStateForm = function (id, onSubmit, onClear) {
        return new StateForm(id, onSubmit, onClear);
    }

    window.f$api.CreateNodeTypeForm = function (id, onSubmit, onClear) {
        return new NodeTypeForm(id, onSubmit, onClear);
    }

    window.f$api.CreateLinkTypeForm = function (id, onSubmit, onClear) {
        return new LinkTypeForm(id, onSubmit, onClear);
    }

    window.f$api.CreateGroupTypeForm = function (id, onSubmit, onClear) {
        return new GroupTypeForm(id, onSubmit, onClear);
    }




    window.f$api.CreateStateList = function (id, callback) {
        return new StateList(id, callback);
    }

    window.f$api.CreateNodeTypeList = function (id, callback) {
        return new NodeTypeList(id, callback);
    }

    window.f$api.CreateLinkTypeList = function (id, callback) {
        return new LinkTypeList(id, callback);
    }

    window.f$api.CreateGroupTypeList = function (id, callback) {
        return new GroupTypeList(id, callback);
    }




    window.f$api.CreateStateDataSet = function (callback) {
        return new StateDataSet(callback);
    }

    window.f$api.CreateNodeTypeDataSet = function (callback) {
        return new NodeTypeDataSet(callback);
    }

    window.f$api.CreateLinkTypeDataSet = function (callback) {
        return new LinkTypeDataSet(callback);
    }

    window.f$api.CreateGroupTypeDataSet = function (callback) {
        return new GroupTypeDataSet(callback);
    }





    //import
    var BaseForm = window.f$api.BaseForm;
    var BaseList = window.f$api.BaseList;
    var BaseDataSet = window.f$api.BaseDataSet;



    //implementation

    //-------Форма для редактирования элемента справочника-------
    function DictionaryForm(id, onSubmit, onClear) {
        BaseForm.apply(this, arguments);

        var _this = this;

        var buttons = this.appendButtonGroup(this.formDiv, "actions");

        //Добавить или Сохранить
        this.submit = this.appendSubmit(buttons, "Сохранить",
            {
                rules: {
                    "name": "required"
                },
                messages: {
                    "name": "Имя обязательно!"
                }
            },
            function (data) {
                if (onSubmit) {
                    onSubmit(_this.serializeDictionaryItem(data));
                }

                _this.loadDefault();
            });

        //Очистить
        this.appendButton(buttons, "btn_clear", "Очистить", function () {
            if (onClear) {
                onClear();
            }

            _this.loadDefault();
        })


        this.appendInputGroup(this.formDiv, "name", "text", "Имя");
        this.appendTextAreaGroup(this.formDiv, "description", "Описание");


        //Загрузка по умолчанию
        this.loadDefault()
    }

    DictionaryForm.prototype = Object.create(BaseForm.prototype);

    DictionaryForm.prototype.constructor = DictionaryForm;

    //public
    DictionaryForm.prototype.loadDefault = function () {
        this.load(null)
    }

    DictionaryForm.prototype.load = function (item) {
        this.item = item || {};

        if (item) {
            this.deserialize({
                name: this.item.name,
                description: this.item.description,
            });
        } else {
            this.deserialize({
                name: "Новое значение",
                description: "Описание",
            });

        }

        this.submit.val(this.item.id ? "Сохранить" : "Добавить");
    }

    //protected
    DictionaryForm.prototype.serializeDictionaryItem = function (data) {
        var item = this.item || {};

        item.name = data.name;
        item.description = data.description;

        return item;
    }











    //-------Форма для редактирования состояния-------
    function StateForm(id, onSubmit, onClear) {
        DictionaryForm.apply(this, arguments);

        this.appendSelectGroup(this.formDiv, "state", "Состояние", [
            { value: 0, text: "Закрыто" },
            { value: 1, text: "Открыто" }
        ])
    }

    StateForm.prototype = Object.create(DictionaryForm.prototype);

    StateForm.prototype.constructor = StateForm;

    StateForm.prototype.load = function (item) {
        DictionaryForm.prototype.load.apply(this, arguments);
        this.deserialize({ "state": this.item["graph-state-id"] })
    }

    //overrided
    StateForm.prototype.serializeDictionaryItem = function (data) {
        var item = DictionaryForm.prototype.serializeDictionaryItem.apply(this, arguments);
        item["graph-state-id"] = data.state;

        return item;
    }




    //-------Форма для редактирования типа узла-------
    function NodeTypeForm(id, onSubmit, onClear) {
        DictionaryForm.apply(this, arguments);

        this.appendSelectGroup(this.formDiv, "view", "Представление", [
            { value: 0, text: "Символ" },
            { value: 1, text: "Линейная геометрия" }
        ])
    }

    NodeTypeForm.prototype = Object.create(DictionaryForm.prototype);

    NodeTypeForm.prototype.constructor = NodeTypeForm;

    NodeTypeForm.prototype.load = function (item) {
        DictionaryForm.prototype.load.apply(this, arguments);
        this.deserialize({ "view": this.item["view-type"] })
    }

    //overrided
    NodeTypeForm.prototype.serializeDictionaryItem = function (data) {
        var item = DictionaryForm.prototype.serializeDictionaryItem.apply(this, arguments);
        item["view-type"] = data.view;

        return item;
    }





    //-------Форма для редактирования типа связи-------
    function LinkTypeForm(id, onSubmit, onClear) {
        DictionaryForm.apply(this, arguments);

    }

    LinkTypeForm.prototype = Object.create(DictionaryForm.prototype);

    LinkTypeForm.prototype.constructor = LinkTypeForm;





    //-------Форма для редактирования типа групп-------
    function GroupTypeForm(id, onSubmit, onClear) {
        DictionaryForm.apply(this, arguments);

    }

    GroupTypeForm.prototype = Object.create(DictionaryForm.prototype);

    GroupTypeForm.prototype.constructor = GroupTypeForm;








    //-------Список состояний-------
    function StateList(id, callback) {
        BaseList.call(this, id, "api/states", callback);


    }

    StateList.prototype = Object.create(BaseList.prototype);

    StateList.prototype.constructor = StateList;

    StateList.prototype.itemMarkup = function (item) {
        return item.name;
    }


    //-------Список типов узлов-------
    function NodeTypeList(id, callback) {
        BaseList.call(this, id, "api/node-types", callback);


    }

    NodeTypeList.prototype = Object.create(BaseList.prototype);

    NodeTypeList.prototype.constructor = NodeTypeList;

    NodeTypeList.prototype.itemMarkup = function (item) {
        return item.name;
    }




    //-------Список типов связей-------
    function LinkTypeList(id, callback) {
        BaseList.call(this, id, "api/link-types", callback);


    }

    LinkTypeList.prototype = Object.create(BaseList.prototype);

    LinkTypeList.prototype.constructor = LinkTypeList;

    LinkTypeList.prototype.itemMarkup = function (item) {
        return item.name;
    }




    //-------Список типов групп-------
    function GroupTypeList(id, callback) {
        BaseList.call(this, id, "api/group-types", callback);


    }

    GroupTypeList.prototype = Object.create(BaseList.prototype);

    GroupTypeList.prototype.constructor = GroupTypeList;

    GroupTypeList.prototype.itemMarkup = function (item) {
        return item.name;
    }






    //-------DataSets-----------
    //-------DataSet состояний-------
    function StateDataSet(callback) {
        BaseDataSet.call(this, "api/states", callback);
    }

    StateDataSet.prototype = Object.create(BaseDataSet.prototype);

    StateDataSet.prototype.constructor = StateDataSet;









    //-------DataSet типов узлов-------
    function NodeTypeDataSet(callback) {
        BaseDataSet.call(this, "api/node-types", callback);
    }

    NodeTypeDataSet.prototype = Object.create(BaseDataSet.prototype);

    NodeTypeDataSet.prototype.constructor = NodeTypeDataSet;










    //-------DataSet типов связей-------
    function LinkTypeDataSet(callback) {
        BaseDataSet.call(this, "api/link-types", callback);
    }

    LinkTypeDataSet.prototype = Object.create(BaseDataSet.prototype);

    LinkTypeDataSet.prototype.constructor = LinkTypeDataSet;










    //-------DataSet типов групп-------
    function GroupTypeDataSet(callback) {
        BaseDataSet.call(this, "api/group-types", callback);
    }

    GroupTypeDataSet.prototype = Object.create(BaseDataSet.prototype);

    GroupTypeDataSet.prototype.constructor = GroupTypeDataSet;
})();