﻿"use strict";

console.log("модуль schematics.forms.net.js загружен");

(function () {
    //export
    window.f$api.CreateNodeForm = function (id, onSubmit, onClear) {
        return new NodeForm(id, onSubmit, onClear);
    }

    window.f$api.CreateNodeDataSet = function (callback) {
        return new NodeDataSet(callback);
    }

    window.f$api.CreateLinkForm = function (id, onSubmit, onClear) {
        return new LinkForm(id, onSubmit, onClear);
    }

    window.f$api.CreateLinkDataSet = function (callback) {
        return new LinkDataSet(callback);
    }

    window.f$api.CreateGroupForm = function (id, onSubmit, onClear) {
        return new GroupForm(id, onSubmit, onClear);
    }

    window.f$api.CreateGroupDataSet = function (callback) {
        return new GroupDataSet(callback);
    }


    //import
    var BaseDataSet = window.f$api.BaseDataSet;
    var BaseForm = window.f$api.BaseForm;



    //implementation
    //-------Список узлов-------
    function NodeDataSet(callback) {
        BaseDataSet.call(this, "api/nodes",  callback);
    }

    NodeDataSet.prototype = Object.create(BaseDataSet.prototype);

    NodeDataSet.prototype.constructor = NodeDataSet;






    //-------Список связей-------
    function LinkDataSet(callback) {
        BaseDataSet.call(this, "api/links", callback);
    }

    LinkDataSet.prototype = Object.create(BaseDataSet.prototype);

    LinkDataSet.prototype.constructor = LinkDataSet;






    //-------Список групп-------
    function GroupDataSet(callback) {
        BaseDataSet.call(this, "api/groups", callback);
    }

    GroupDataSet.prototype = Object.create(BaseDataSet.prototype);

    GroupDataSet.prototype.constructor = GroupDataSet;




    //-------Форма для редактирования узла-------
    function NodeForm(id, onSubmit, onClear) {
        BaseForm.apply(this, arguments);

        var _this = this;
        

        var onTypeOrStateChanged = function () {
            if (_this._blockEventHandlers)
                return;

            var typeId = $("#" + _this.formId + "_nodeTypeId").val();
            var stateId = $("#" + _this.formId + "_stateId").val();

            _this.submit.prop('disabled', !typeId);

            if (_this._nodeLayout == null)
                return;

            if (typeId) {
                _this._nodeLayout.showStyle(typeId, stateId);
            }
        }
        this._$onTypeOrStateChangedHandler = onTypeOrStateChanged;


        this.appendInputGroup(this.formDiv, "name", "text", "Имя");
        this.appendSelectGroup(this.formDiv, "nodeTypeId", "Тип", [{ value: "", text: "- не определен -" }], "api/node-types").find('select').change(function () {
            onTypeOrStateChanged();
        });
        this.appendSelectGroup(this.formDiv, "stateId", "Состояние", [{ value: "", text: "- не определено -" }], "api/states").find('select').change(function () {
            onTypeOrStateChanged();
        });
        this.appendSelectGroup(this.formDiv, "rotation", "Поворот", [
            { value: "0", text: "0" },
            { value: "90", text: "90" },
            { value: "180", text: "180" },
            { value: "270", text: "270" },
        ]);
        this.appendTextAreaGroup(this.formDiv, "comments", "Примечание");


        var buttons = this.appendButtonGroup(this.formDiv, "actions");

        //Добавить или Сохранить
        this.submit = this.appendSubmit(buttons, "Сохранить",
            {
                rules: {
                    "name": "required",
                    "nodeTypeId": "required",
                },
                messages: {
                    "name": "Имя обязательно!",
                    "nodeTypeId": "Необходимо выбрать тип узла!",
                }
            },
            function (data) {
                if (onSubmit) {
                    onSubmit(_this.serializeNode(data));
                }

                _this.loadDefault();
            });
        this.submit.prop('disabled', true);

        //Копировать
        this.copy = this.appendSubmit(buttons, "Копировать",
            {
                rules: {
                    "name": "required",
                    "nodeTypeId": "required",
                },
                messages: {
                    "name": "Имя обязательно!",
                    "nodeTypeId": "Необходимо выбрать тип узла!",
                }
            },
            function (data) {
                var n = _this.serializeNode(data);
                delete n.id;

                if (n.terminals) {
                    n.terminals.forEach(function (t) {
                        delete t.id;
                    });
                }

                if (onSubmit) {
                    onSubmit(n);
                }

                _this.loadDefault();
            });
        this.copy.addClass('schematics-hidden');

        //Очистить
        this.appendButton(buttons, "btn_clear", "Очистить", function () {
            if (onClear) {
                onClear();
            }

            _this.loadDefault();
        })




        //Загрузка по умолчанию
        this.loadDefault()
    }

    NodeForm.prototype = Object.create(BaseForm.prototype);

    NodeForm.prototype.constructor = NodeForm;






    //public
    NodeForm.prototype.setNodeLayout = function (layout) {
        this._nodeLayout = layout;
        this._$onTypeOrStateChangedHandler();
    }

    NodeForm.prototype.loadDefault = function () {
        this.load(null)
    }

    NodeForm.prototype.load = function (node) {
        this.node = node || {};
        this._blockEventHandlers = true;

        if (node) {
            this.deserialize({
                name: this.node.name,
                comments: this.node.comments,
                nodeTypeId: this.node.type,
                stateId: this.node.state,
                rotation: this.node.rotation,
            });
        } else {
            this.deserialize({
                name: "Имя узла",
                comments: "",
                nodeTypeId: "",
                stateId: "",
                rotation: 0,
            });

        }

        if (this.node.id) {
            this.copy.removeClass('schematics-hidden');
        } else {
            this.copy.addClass('schematics-hidden');
        }

        this.submit.val(this.node.id ? "Сохранить" : "Нарисовать узел");

        delete this._blockEventHandlers;
        this._$onTypeOrStateChangedHandler();
    }

    //protected
    NodeForm.prototype.serializeNode = function (data) {
        var node = this.node || {};

        node.name = data.name;
        node.comments = data.comments;
        node.type = data.nodeTypeId;
        node.state = data.stateId;
        node.rotation = data.rotation;

        return node;
    }












    //-------Форма для редактирования связи-------
    function LinkForm(id, onSubmit, onClear) {
        BaseForm.apply(this, arguments);

        var _this = this;

        var onTypeOrStateChanged = function () {
            var typeId = $("#" + _this.formId + "_linkTypeId").val();
            _this.submit.prop('disabled', !typeId);
        }

        this.appendInputGroup(this.formDiv, "name", "text", "Имя");
        this.appendSelectGroup(this.formDiv, "linkTypeId", "Тип", [{ value: "", text: "- не определен -" }], "api/link-types").find('select').change(function () {
            onTypeOrStateChanged();
        });
        this.appendSelectGroup(this.formDiv, "stateId", "Состояние", [{ value: "", text: "- не определено -" }], "api/states");
        this.appendTextAreaGroup(this.formDiv, "comments", "Примечание");

        var buttons = this.appendButtonGroup(this.formDiv, "actions");

        //Добавить или Сохранить
        this.submit = this.appendSubmit(buttons, "Сохранить",
            {
                rules: {
                    "name": "required",
                    "linkTypeId": "required",
                },
                messages: {
                    "name": "Имя обязательно!",
                    "linkTypeId": "Необходимо выбрать тип линии!",
                }
            },
            function (data) {
                if (onSubmit) {
                    onSubmit(_this.serializeLink(data));
                }

                _this.loadDefault();
            });
        this.submit.prop('disabled', true);

        //Очистить
        this.appendButton(buttons, "btn_clear", "Очистить", function () {
            if (onClear) {
                onClear();
            }

            _this.loadDefault();
        })


        //Загрузка по умолчанию
        this.loadDefault()
    }

    LinkForm.prototype = Object.create(BaseForm.prototype);

    LinkForm.prototype.constructor = LinkForm;

    //public
    LinkForm.prototype.loadDefault = function () {
        this.load(null)
    }

    LinkForm.prototype.load = function (link) {
        this.link = link || {};

        if (link) {
            this.deserialize({
                name: this.link.name,
                comments: this.link.comments,
                linkTypeId: this.link.type,
                stateId: this.link.state,
            });
        } else {
            this.deserialize({
                name: "Имя связи",
                comments: "",
                linkTypeId: "",
                stateId: "",
            });

        }

        this.submit.val(this.link.id ? "Сохранить" : "Нарисовать связь");
    }

    //protected
    LinkForm.prototype.serializeLink = function (data) {
        var link = this.link || {};

        link.name = data.name;
        link.comments = data.comments;
        link.type = data.linkTypeId;
        link.state = data.stateId;

        return link;
    }
























    //-------Форма для редактирования группы-------
    function GroupForm(id, onSubmit, onClear) {
        BaseForm.apply(this, arguments);

        var _this = this;

        var onTypeChanged = function () {
            var typeId = $("#" + _this.formId + "_linkTypeId").val();
        }

        this.appendInputGroup(this.formDiv, "name", "text", "Имя");
        this.appendSelectGroup(this.formDiv, "groupTypeId", "Тип", [{ value: "", text: "- не определен -" }], "api/group-types").find('select').change(function () {
            onTypeChanged();
        });



        var buttons = this.appendButtonGroup(this.formDiv, "actions");

        //Добавить или Сохранить
        this.submit = this.appendSubmit(buttons, "Сохранить",
            {
                rules: {
                    "name": "required",
                },
                messages: {
                    "name": "Имя обязательно!",
                }
            },
            function (data) {
                if (onSubmit) {
                    onSubmit(_this.serializeGroup(data));
                }

                _this.loadDefault();
            });

        //Очистить
        this.appendButton(buttons, "btn_clear", "Очистить", function () {
            if (onClear) {
                onClear();
            }

            _this.loadDefault();
        })


        //Загрузка по умолчанию
        this.loadDefault()
    }

    GroupForm.prototype = Object.create(BaseForm.prototype);

    GroupForm.prototype.constructor = GroupForm;

    //public
    GroupForm.prototype.loadDefault = function () {
        this.load(null)
    }

    GroupForm.prototype.load = function (group) {
        this.group = group || {};

        if (group) {
            this.deserialize({
                name: this.group.name,
                groupTypeId: this.group.type || "",
            });
        } else {
            this.deserialize({
                name: "Имя группы",
                groupTypeId: "",
            });

        }

        this.submit.val(this.group.id ? "Сохранить" : "Нарисовать группу");
    }

    //protected
    GroupForm.prototype.serializeGroup = function (data) {
        var group = this.group || {};

        group.name = data.name;
        group.type = data.groupTypeId;

        return group;
    }
})();
