﻿"use strict";

console.log("модуль schematics.forms.ui.js загружен");

(function () {
    //export

    if (!window.f$api)
        window.f$api = {};

    window.f$api.BaseForm = BaseForm;
    window.f$api.BaseList = BaseList;
    window.f$api.BaseDataSet = BaseDataSet;




    //implementation

    //----------------Базовая форма-----------------
    function BaseForm(id, callback) {
        this.id = id;
        this.formId = id + "_form";
        this.callback = callback;

        (this.dialogDiv = $("#" + id))
                    .append(this.formDiv = $("<form/>")
                        .attr("id", id)
                        .attr("method", "get")
                        .attr("action", ""));
    }



    //public / protected
    BaseForm.prototype.show = function () {
        this.dialogDiv.removeClass("schematics-hidden")
                      .addClass("schematics-visible");
    }

    BaseForm.prototype.hide = function () {
        this.dialogDiv.removeClass("schematics-visible")
                      .addClass("schematics-hidden");
    }

    BaseForm.prototype.serialize = function () {
        var result = {};

        $(this.formDiv).find("input, textarea, select")
            .each(function (index, input) {
                if (input.name) {
                    result[input.name] = (input.type == "checkbox") ? $(input).is(':checked') : input.value;
                }
            });

        return result;
    }

    BaseForm.prototype.deserialize = function (data) {
        var inputs = $(this.formDiv).find("input, textarea, select");

        $(this.formDiv).find("input, textarea, select")
            .each(function (index, input) {
                if (input.name) {
                    var value = data[input.name];
                    if (value !== undefined) {
                        if (input.type == "checkbox")
                            $(input).prop("checked", value);
                        else
                            input.value = value;

                        $(input).trigger("change");
                    }
                }
            });
    }

    BaseForm.prototype.appendSubmit = function (parent, text, validation, click) {
        var id = this.formId + "_commit";

        var _this = this;

        var btn = $("<input/>")
        .attr("type", "submit")
        .attr("id", id)
        .val(text)
        .addClass("btn")
        .addClass("btn-success")
        .click(function () {
            _this.formDiv.validate(validation).settings.submitHandler = function () {
                if (click)
                    click(_this.serialize());
            };
        });

        parent.append(btn);
        return btn;
    }

    BaseForm.prototype.appendButton = function (parent, name, text, click) {
        var id = this.formId + "_" + name;

        var btn = $("<input/>")
        .attr("type", "button")
        .attr("id", id)
        .val(text)
        .addClass("btn")
        .addClass("btn-default")
        .click(click);

        parent.append(btn);
        return btn;
    }

    function _appendGroupDiv(parent, id, label) {
        var groupDiv = $("<div/>")
        .addClass("form-group")
        .append($("<label class='small'/>")
                .attr("for", id)
                .text(label));

        parent.append(groupDiv);
        return groupDiv;
    }

    BaseForm.prototype.appendInputGroup = function (parent, name, type, label) {
        var id = this.formId + "_" + name;

        var groupDiv = _appendGroupDiv(parent, id, label);

        groupDiv.append($("<input/>").attr("type", type)
        .attr("id", id)
        .attr("name", name)
        .addClass("form-control"));

        return groupDiv;
    }

    BaseForm.prototype.appendTextAreaGroup = function (parent, name, label, placeholder) {
        var id = this.formId + "_" + name;

        var groupDiv = _appendGroupDiv(parent, id, label);

        groupDiv.append($("<textarea/>")
        .attr("id", id)
        .attr("name", name)
        .attr("placeholder", placeholder)
        .addClass("form-control"));

        return groupDiv;
    }

    BaseForm.prototype.appendSelectGroup = function (parent, name, label, values, url) {
        var id = this.formId + "_" + name;

        var groupDiv = _appendGroupDiv(parent, id, label);

        var selectEl = $("<select/>")
        .attr("id", id)
        .attr("name", name)
        .addClass("form-control");

        if (url) {
            $.get(url + "/list?ts=" + getTimeStamp())
            .done(function (data) {
                data.list.forEach(function (value) {
                    selectEl.append("<option value='" + value.id + "'>" + value.name + "</option>")
                });
            })
            .fail(function () { });
        }

        (values || []).forEach(function (value) {
            selectEl.append("<option value='" + value.value + "'>" + value.text + "</option>")
        });

        groupDiv.append(selectEl);
        return groupDiv;
    }

    BaseForm.prototype.appendButtonGroup = function (parent, name) {
        var buttonGroupDiv = $("<div/>")
        .addClass("btn-group schematics-toolbar");

        if (name) {
            var id = this.formId + "_" + name;
            buttonGroupDiv.attr("id", id);
        }

        parent.append(buttonGroupDiv);
        return buttonGroupDiv;
    }

    BaseForm.prototype.appendPanelGroup = function (parent, name) {
        var panelGroupDiv = $("<div/>")
        .addClass("panel-group");

        if (name) {
            var id = this.formId + "_" + name;
            panelGroupDiv.attr("id", id);
        }

        parent.append(panelGroupDiv)
        return panelGroupDiv;
    }

    BaseForm.prototype.appendPanel = function (parent, name, label, open) {
        var id = this.formId + "_" + name;
        var parentId = parent.attr("id");

        var anchor = $("<a/>")
        .attr("data-toggle", "collapse")
        .attr("href", "#" + id)
        .append(label);

        if (parentId) {
            anchor.attr("data-parent", "#" + parentId);
        }

        var panelContentDiv = $("<div/>")
        .addClass("panel-collapse collapse")
        .attr("id", id);

        if (open) {
            panelContentDiv.addClass("in");
        }

        parent.append($("<div/>")
                .addClass("panel panel-default")
                .append($("<div/>")
                    .addClass("panel-heading")
                    .append($("<h4/>")
                        .addClass("panel-title")
                        .append(anchor)))
                .append(panelContentDiv)
            );

        var panelBodyDiv = $("<div/>")
        .addClass("panel-body");

        panelContentDiv.append(panelBodyDiv);
        return panelBodyDiv;
    }













    //----------------Базовый датасет-----------------
    function BaseDataSet(url, callback) {
        this.url = url;
        this.callback = callback;
    }

    //Event handlers
    BaseDataSet.prototype._afterAddCallback = function (data) {
        this.callback("after-add", data);
    };

    BaseDataSet.prototype._afterUpdateCallback = function (id, data) {
        this.callback("after-update", id, data);
    };

    BaseDataSet.prototype._afterDeleteCallback = function (id) {
        this.callback("after-delete", id)
    };

    function getTimeStamp() {
        var dt = new Date();
        return dt.getHours() + "-" + dt.getMinutes() + "-" + dt.getSeconds() + "-" + dt.getMilliseconds();
    }

    //public
    BaseDataSet.prototype.getList = function (handler) {
        $.get(this.url + "/list?ts=" + getTimeStamp())
        .done(function (data) {
            handler(data.list);
        })
        .fail(function () { console.log("Get list is failed!") });
    }

    BaseDataSet.prototype.getItem = function (id, handler) {
        $.get(this.url + "/item?id=" + id + "&ts=" + getTimeStamp())
        .done(function (data) {
            handler(JSON.parse(data.item));
        })
        .fail(function () { console.log("Get item is failed!") });
    }

    BaseDataSet.prototype.add = function (data) {
        var _this = this;
        $.ajax({
            url: this.url + "/add",
            type: "put",
            data: JSON.stringify(data),
        })
        .done(function (result) {
            validateRequestState(result);
            _this._afterAddCallback(data);
        })
        .fail(function () { console.log("Add is failed!") })
    }

    BaseDataSet.prototype.update = function (id, data) {
        var _this = this;
        $.ajax({
            url: this.url + "/update?id=" + id,
            type: "post",
            data: JSON.stringify(data),
        })
        .done(function (result) {
            validateRequestState(result);
            _this._afterUpdateCallback(id, data);
        })
        .fail(function () { console.log("Update is failed!") })
    }

    BaseDataSet.prototype.delete = function (id) {
        var _this = this;
        $.ajax({
            url: this.url + "/delete?id=" + id,
            type: "delete",
        })
        .done(function (result) {
            validateRequestState(result);
            _this._afterDeleteCallback(id);
        })
        .fail(function () { console.log("Delete is failed!") })
    }

    function validateRequestState(data) {
        if (data.state == "error") {
            alert("Ошибка: " + data.message);
        }
    }















    //----------------Базовый список-----------------
    function BaseList(id, url, callback) {
        BaseDataSet.call(this, url, callback);

        this.id = id;
        this.dialogDiv = $("#" + id);

        this.listDiv = this.dialogDiv
                .append($("<div/>")
                    .addClass("list-group"));

        this.current = null;
        this.refresh();
    }

    BaseList.prototype = Object.create(BaseDataSet.prototype);

    BaseList.prototype.constructor = BaseList;

    //Event handlers
    BaseList.prototype._afterAddCallback = function (data) {
        BaseDataSet.prototype._afterAddCallback.call(this, data);

        this.refresh();
    }

    BaseList.prototype._afterUpdateCallback = function (id, data) {
        BaseDataSet.prototype._afterUpdateCallback.call(this, id, data);

        if (this.itemMarkup) {
            $("#item_text_" + id)
                .empty()
                .append(this.itemMarkup(data));
        }
    }

    BaseList.prototype._afterDeleteCallback = function (id) {
        BaseDataSet.prototype._afterDeleteCallback.call(this, id);

        $("#item_" + id).remove();
    };





    //public
    BaseList.prototype.show = function () {
        this.dialogDiv.removeClass("schematics-hidden")
                      .addClass("schematics-visible");
    }

    BaseList.prototype.hide = function () {
        this.dialogDiv.removeClass("schematics-visible")
                      .addClass("schematics-hidden");
    }

    BaseList.prototype.refresh = function () {
        this.listDiv.empty();

        var _this = this;
        this.getList(function (list) {
            list.forEach(function (i) {
                _this._appendToList(i)
            });
        })
    }

    //protected/private
    BaseList.prototype._appendToList = function(item) {
        var _this = this;

        //actions
        var itemActionsDiv = $("<div/>")
        .addClass("btn-group-xs pull-right");

        if (this.addCopy) { //add
            itemActionsDiv.append($("<button/>")
                .addClass("btn btn-default glyphicon glyphicon-copy")
                .attr("item_id", item.id)
                .attr("item_name", item.name)
                .attr("title", "Сделать копию")
                .click(function () {
                    _this.callback("copy", $(this).attr("item_id"));
                }));            
        }

        //edit
        itemActionsDiv.append($("<button/>")
            .addClass("btn btn-default glyphicon glyphicon-pencil")
            .attr("item_id", item.id)
            .attr("item_name", item.name)
            .attr("title", "Редактировать")
            .click(function () {
                _this.callback("update", $(this).attr("item_id"));
            }));

        //remove
        itemActionsDiv.append($("<button/>")
            .addClass("btn btn-danger glyphicon glyphicon-remove")
            .attr("item_id", item.id)
            .attr("item_name", item.name)
            .attr("title", "Удалить")
            .click(function () {
                if (confirm("Удалить элемент " + $(this).attr("item_name") + "?"))
                    _this.delete($(this).attr("item_id"));
            }));


        //list
        this.listDiv.append($("<div/>")
            .attr("id", "item_" + item.id)
            .addClass("list-group-item")
            .append(itemActionsDiv)
            .append($("<div/>")
                .attr("id", "item_text_" + item.id)
                .append(_this.itemMarkup(item))));
    }
})();