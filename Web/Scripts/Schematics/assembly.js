﻿"use strict";

console.log("модуль schematics.assembly.js загружен");

(function () {
    //Графические элементы управления схематики
    var schematics = {
        //редактор стиля
        styleLayout: window.s$api.CreateStyleLayout,

        //редактор сети
        netLayout: window.s$api.CreateNetLayout,
    }

    //Формы и списки редактирования объектов схематики
    var forms = {
        //стили
        styleForm: window.f$api.CreateStyleForm,
        styleList: window.f$api.CreateStyleList,

        //справочник состояний
        stateForm: window.f$api.CreateStateForm,
        stateList: window.f$api.CreateStateList,
        stateDataSet: window.f$api.CreateStateDataSet,

        //справочник типов узлов
        nodeTypeForm: window.f$api.CreateNodeTypeForm,
        nodeTypeList: window.f$api.CreateNodeTypeList,
        nodeTypeDataSet: window.f$api.CreateNodeTypeDataSet,

        //справочник типов связей
        linkTypeForm: window.f$api.CreateLinkTypeForm,
        linkTypeList: window.f$api.CreateLinkTypeList,
        linkTypeDataSet: window.f$api.CreateLinkTypeDataSet,

        //справочник типов групп
        groupTypeForm: window.f$api.CreateGroupTypeForm,
        groupTypeList: window.f$api.CreateGroupTypeList,
        groupTypeDataSet: window.f$api.CreateGroupTypeDataSet,

        //узлы
        nodeForm: window.f$api.CreateNodeForm,
        nodeDataSet: window.f$api.CreateNodeDataSet,

        //связи
        linkForm: window.f$api.CreateLinkForm,
        linkDataSet: window.f$api.CreateLinkDataSet,

        //группы
        groupForm: window.f$api.CreateGroupForm,
        groupDataSet: window.f$api.CreateGroupDataSet,
    }

    //Глобальная ссылка на сервисы схематики
    window.s$ = schematics;
    window.f$ = forms;
})();