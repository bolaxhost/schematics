﻿"use strict";

console.log("модуль schematics.ol.net-layout.js загружен");

(function () {
    //export
    window.s$api.CreateNetLayout = function (id, mode) {
        return new NetLayout(id, mode);
    }




    //import
    var BaseLayout = window.s$api.BaseLayout;


    //implementation
    var MIN_LABELS_ZOOM = 18;

    //-------layout редактора схематики-------
    function NetLayout(id, mode) {
        BaseLayout.apply(this, arguments);

        this._mode = mode || "net";

        var _this = this;

        //запуск перекомпиляции схемы при изменении extent
        this._map.on("moveend", function () {
            _this._recompile();
        });

        var featuresToReferences = function (f) {
            var result = [];
            f.forEach(function (f) {
                result.push({
                    id: f.get("elementId"),
                    type: f.get("elementType"),
                    parentId: f.get("parentElementId"),
                    parentType: f.get("parentElementType"),
                    geometry: _this._getFeatureGeometry(f),
                });
            });

            return result;
        }

        //привязка событий
        this.on = function (event, callback) {
            if (event == "drawend") {
                this._commonLayer.getSource().on("change", function () {
                    callback(_this.getGeometry());
                });
                return;
            }

            if (event == "modifyend") {
                this._map._modify.modify.on("modifyend", function (e) {
                    callback(featuresToReferences(e.features))
                });
                return;
            }

            if (event == "selectionend") {
                this._map._selectedFeatures.on("change:length", function () {
                    callback(featuresToReferences(_this._map._selectedFeatures));
                });

                this._map._featuresToModify.on("change:length", function () {
                    callback(featuresToReferences(_this._map._featuresToModify));
                });
                return;
            }

            if (event == "deleteend") {
                this._map._delete._onAfterDelete = function (f) {
                    callback(featuresToReferences(f));
                };
                return;
            }

            if (event == "actioncancelled") {
                if (!this._stopActionCallbacks)
                    this._stopActionCallbacks = [];

                this._stopActionCallbacks.push(callback);
                return;
            }
    }

        this.boxOperationCallback = function () {
            _this.refresh()
            _this.setGeometry([]);
        }

        //Скрыть карту
        this.toggleGeo();

        //Первоначальная прорисовка
        this.refresh();
    }


    NetLayout.prototype = Object.create(BaseLayout.prototype);

    NetLayout.prototype.constructor = NetLayout;







    //public
    NetLayout.prototype.fit = function () {
        this._fitByLayer(this._activeLayer);
    }

    NetLayout.prototype.showStyle = function (typeId, stateId, coordinates) {
        coordinates = coordinates || [0, 0];

        var _this = this;
        $.get("api/schematics/compile-style", { typeId: typeId, stateId: stateId, x: coordinates[0], y: coordinates[1], ts: getTimeStamp() })
                .done(function (data) {
                    if (!data.data) {
                        console.log("Compile style is failed (internal error)!")
                        return;
                    }

                    var result = JSON.parse(data.data);
                    _this._drawSchematics(result);

                    _this._shownStyles = [];
                    result.styles.forEach(function (s) {
                        _this._shownStyles.push(s.v);
                    });

                    try {
                        var fs = _this._activeLayer.getSource().getFeatures();
                        if (fs.length > 0) {
                            var f = fs[0];
                            _this._map.getView().fit(f.getStyle().getGeometry()(f).getExtent());
                        }
                    } catch (ex) {}
                })
                .fail(function () {
                    console.log("Compile style is failed!")
                });
    }

    NetLayout.prototype.getShownStyles = function () {
        return this._shownStyles || [];
    }

    NetLayout.prototype.cancelAction = function () {
        this._stopAction(true);
    }

    NetLayout.prototype.setStatusBar = function (statusBarId) {
        if (!statusBarId) {
            delete this._statusBar;
            return;
        }

        this._statusBar = $("#" + statusBarId);
    }

    NetLayout.prototype.showStatus = function (text) {
        if (!this._statusBar) {
            return;
        }

        this._statusBar.empty()
                    .append("<span>" + text + "</span>");
    }

    NetLayout.prototype.toggleGeo = function () {
        this._geoLayer.setVisible(!this._geoLayer.getVisible());
    }

    NetLayout.prototype.setVisibleGeo = function (visible) {
        this._geoLayer.setVisible(visible);
    }

    NetLayout.prototype.getVisibleGeo = function () {
        return this._geoLayer.getVisible();
    }

    NetLayout.prototype.toggleAuxiliary = function () {
        this._snapLayer.setVisible(!this._snapLayer.getVisible());
    }

    NetLayout.prototype.setVisibleAuxiliary = function (visible) {
        this._snapLayer.setVisible(visible);
    }

    NetLayout.prototype.getVisibleAuxiliary = function () {
        return this._snapLayer.getVisible();
    }

    NetLayout.prototype.toggleLabels = function () {
        this._labelsLayer.setVisible(!this._labelsLayer.getVisible());
    }

    NetLayout.prototype.setVisibleLabels = function (visible) {
        this._labelsLayer.setVisible(visible);
    }

    NetLayout.prototype.getVisibleLabels = function () {
        return this._labelsLayer.getVisible();
    }

    NetLayout.prototype.getTerminalFeatureAtCoordinate = function (coordinate, tolerance) {
        var snap = this._snapLayer.getSource().getClosestFeatureToCoordinate(coordinate);

        if (!snap) {
            return null;
        }

        var cp = snap.getGeometry().getClosestPoint(coordinate);

        tolerance = tolerance || (this._gridStep ? this._gridStep / 4 : 0) || 1;

        if (Math.abs(coordinate[0] - cp[0]) > tolerance || Math.abs(coordinate[1] - cp[1]) > tolerance) {
            return null;
        }

        return snap;
    }

    NetLayout.prototype.getTerminalInfoAtCoordinate = function (coordinate) {
        var f = this.getTerminalFeatureAtCoordinate(coordinate);

        if (!f)
            return null;

        var parentId = f.get("parentElementId");
        if (parentId) {
            return {
                nodeId: parentId,
                terminalId: f.get("elementId"),
            };
        } else {
            return {
                nodeId: f.get("elementId"),
                terminalId: null,
            };
        }
    }

    NetLayout.prototype.getLinkSourceInfo = function (g) {
        return this.getTerminalInfoAtCoordinate(g["coordinates-string"][0]);
    }

    NetLayout.prototype.getLinkDestinationInfo = function (g) {
        return this.getTerminalInfoAtCoordinate(g["coordinates-string"][g["coordinates-string"].length - 1]);
    }

    NetLayout.prototype.validateLinkGeometry = function (g) {
        return this.getLinkSourceInfo(g) && this.getLinkDestinationInfo(g);
    }

    NetLayout.prototype.updateLinkConnectivity = function (link, g) {
        link.geometry = [g];

        var ts = this.getLinkSourceInfo(g);
        if (ts) {
            link.from = ts.nodeId;
            link["terminal-from"] = ts.terminalId;
        }

        var td = this.getLinkDestinationInfo(g);
        if (td) {
            link.to = td.nodeId;
            link["terminal-to"] = td.terminalId;
        }
    }

    NetLayout.prototype.startDrawExtentPoint = function (extent) {
        this.startDraw("Point", [{
            type: "Polygon",
            "coordinates-strings": [[
                [extent[0], extent[1]],
                [extent[2], extent[1]],
                [extent[2], extent[3]],
                [extent[0], extent[3]],
            ]],
        }], [extent[0], extent[3]]);
    }


    NetLayout.prototype.copyPasteByGroupId = function (groupId, coordinates) {
        var _this = this;
        $.get("api/schematics/copy-group", { id: groupId, ts: getTimeStamp() })
                .done(function (data) {
                    if (!data.data) {
                        console.log("CopyPasteByGroupId is failed (internal error)!")
                        return;
                    }

                    _this._copyPaste(JSON.parse(data.data), coordinates);
                })
                .fail(function () {
                    console.log("CopyPasteByGroupId is failed!")
                });

    }

    NetLayout.prototype.copyBox = function (box, callback) {
        var cs = box["coordinates-strings"][0];

        var x1, x2, y1, y2 = null;
        cs.forEach(function (c) {
            if (x1 == null || c[0] < x1) {
                x1 = c[0];
            }
            if (y1 == null || c[1] < y1) {
                y1 = c[1];
            }

            if (x2 == null || c[0] > x2) {
                x2 = c[0];
            }
            if (y2 == null || c[1] > y2) {
                y2 = c[1];
            }
        });

        var _this = this;
        $.get("api/schematics/copy", { x1: x1, y1: y1, x2: x2, y2: y2, ts: getTimeStamp() })
                .done(function (data) {
                    if (!data.data) {
                        console.log("CopyByBox is failed (internal error)!")
                        return;
                    }

                    _this._saveCopyContext(JSON.parse(data.data), box);

                    if (callback) {
                        callback(_this._copyScope);
                    }
                })
                .fail(function () {
                    console.log("CopyByBox is failed!")
                });
    }

    NetLayout.prototype.pasteBox = function (coordinates) {
        this._copyPaste(this._copyScope, coordinates);
    }

    NetLayout.prototype.moveBox = function (coordinates) {
        var box = this._copyBox;
        var extent = this._copyScope.extent;

        var dx = coordinates[0] - extent[0];
        var dy = coordinates[1] - extent[3];
        box["coordinates-strings"][0].forEach(function (c) {
            c[0] += dx;
            c[1] += dy;
        });

        var _this = this;
        this._moveBox(this._copyScope, coordinates, function () {
            _this.boxOperationCallback();
            _this.copyBox(box);
        });
        this._removeCopyContext();

    }

    NetLayout.prototype.rotateBox = function (angle) {
        var box = this._copyBox;
        var extent = this._copyScope.extent;
        var center = [(extent[0] + extent[2]) / 2, (extent[1] + extent[3]) / 2];

        box["coordinates-strings"][0].forEach(function (c) {
            var dx = c[0] - center[0];
            var dy = c[1] - center[1];

            var angleR = angle / 180 * Math.PI;

            c[0] = center[0] + dx * Math.cos(angleR) - dy * Math.sin(angleR);
            c[1] = center[1] + dx * Math.sin(angleR) + dy * Math.cos(angleR);
        });

        var _this = this;
        this._rotateBox(this._copyScope, angle, function () {
            _this.boxOperationCallback();
            _this.copyBox(box);
        });
        this._removeCopyContext();
    }

    NetLayout.prototype.deleteBox = function () {
        this._deleteBox(this._copyScope);
        this._removeCopyContext();
    }

    NetLayout.prototype.startDrawBoxPoint = function () {
        var extent = this._copyScope.extent;
        this.startDraw("Point", [this._copyBox], [extent[0], extent[3]]);
    }

    NetLayout.prototype.clearBox = function () {
        this._removeCopyContext();
    }




    //--protected--
    NetLayout.prototype._saveCopyContext = function (scope, box) {
        this._copyScope = scope;
        this._copyBox = box;
        this._setGeometry(this._boxLayer, [box]);
    }

    NetLayout.prototype._removeCopyContext = function () {
        delete this._copyScope;
        delete this._copyBox;
        this._boxLayer.getSource().clear();
    }

    NetLayout.prototype._copyPaste = function (scope, coordinates) {
        var extent = scope.extent;
        var offset = [coordinates[0] - extent[0], coordinates[1] - extent[3]];

        var _this = this;
        //Обновление location для узлов
        scope.nodes.filter(function (node) { return node.position }).forEach(function (node) {
            var np = [node.position[0] + offset[0], node.position[1] + offset[1]];
            node.location = _this.transformToLonLat(np);
        });

        $.ajax({
            url: "api/schematics/paste?dx=" + offset[0] + "&dy=" + offset[1],
            type: "put",
            data: JSON.stringify(scope),
        })
        .done(function (data) {
            _this.boxOperationCallback();
        })
        .fail(function () { console.log("Paste is failed!") })
    }

    NetLayout.prototype._moveBox = function (scope, coordinates, callback) {
        var extent = scope.extent;
        var offset = [coordinates[0] - extent[0], coordinates[1] - extent[3]];

        var _this = this;
        //Обновление location для узлов
        scope.nodes.filter(function (node) { return node.position }).forEach(function (node) {
            var np = [node.position[0] + offset[0], node.position[1] + offset[1]];
            node.location = _this.transformToLonLat(np);
        });

        $.ajax({
            url: "api/schematics/move?dx=" + offset[0] + "&dy=" + offset[1],
            type: "post",
            data: JSON.stringify(scope),
        })
        .done(function (data) {
            (callback || _this.boxOperationCallback)();
        })
        .fail(function () { console.log("Move is failed!") })
    }

    NetLayout.prototype._rotateBox = function (scope, angle, callback) {
        var extent = scope.extent;
        var center = [(extent[0] + extent[2]) / 2, (extent[1]+ extent[3]) / 2];

        var _this = this;

        //Обновление location для узлов
        scope.nodes.filter(function (node) { return node.position }).forEach(function (node) {
            var dx = node.position[0] - center[0];
            var dy = node.position[1] - center[1];

            var angleR = angle / 180 * Math.PI;

            var np = [center[0] + dx * Math.cos(angleR) - dy * Math.sin(angleR), center[1] + dx * Math.sin(angleR) + dy * Math.cos(angleR)];
            node.location = _this.transformToLonLat(np);
        });

        $.ajax({
            url: "api/schematics/rotate?angle=" + angle,
            type: "post",
            data: JSON.stringify(scope),
        })
        .done(function (data) {
            (callback || _this.boxOperationCallback)();
        })
        .fail(function () { console.log("Rotate is failed!") })
    }

    NetLayout.prototype._deleteBox = function (scope) {
        var _this = this;
        $.ajax({
            url: "api/schematics/delete",
            type: "delete",
            data: JSON.stringify(scope),
        })
        .done(function (data) {
            _this.boxOperationCallback();
        })
        .fail(function () { console.log("Delete is failed!") })
    }





    //рекомпиляция, если extent поменялся и выходит за границы предыдущего
    NetLayout.prototype._recompile = function () {
        var extent = this.getExtent();

        if (!this._oldExtent) {
            this._compile();
            return;
        }

        if ((this._$full != undefined && !this._$full) || (extent[0] < this._oldExtent[0] || extent[1] < this._oldExtent[1] || extent[2] > this._oldExtent[2] || extent[3] > this._oldExtent[3])) {
            this._compile();
            return;
        }
    }

    //компиляция схемы
    NetLayout.prototype._compile = function () {
        if (this._mode == "blank") {
            return;
        }

        var extent = this.getExtent()

        var _this = this;
        var callback = function (data) {
            _this._$full = data.full;

            _this._drawSchematics(data);
            _this._$t3 = new Date();

            var loadStatus = data.full ? "" : "ПРЕВЫШЕН ЛИМИТ ОБЪЕМА ДАННЫХ! ";


            _this.showStatus(loadStatus + "Загружено узлов: " + data.nodesCount + ", связей: " + data.linksCount + ", групп: " + data.groupsCount + ". Время обработки: " + (_this._$t3 - _this._$t1) + " мс., В том числе работа сервера: "+ (_this._$t2 - _this._$t1) + " мс.");
        }

        this.showStatus("Загрузка схемы...");

        this._$t1 = new Date();

        this._compileByExtent(extent, callback);
        //this._compileByLonLatRadius(extent, callback);

        //Попытка запросить данные через geospatial within
        //Если ошибка, то запрос через extent
        //this._compileByLonLatRadius(extent, callback, this._compileByExtent);

        this._oldExtent = extent;
    }

    function getTimeStamp() {
        var dt = new Date();
        return dt.getHours() + "-" + dt.getMinutes() + "-" + dt.getSeconds() + "-" + dt.getMilliseconds();
    }

    //compile by extent
    NetLayout.prototype._compileByExtent = function (extent, callback) {
        var _this = this;

        $.get("api/schematics/compile-extent", { x1: extent[0], y1: extent[1], x2: extent[2], y2: extent[3], mode: this._mode, ts: getTimeStamp() })
                .done(function (data) {
                    if (!data.data) {
                        console.log("Compile 'extent' is failed (internal error)!")
                        return;
                    }

                    _this._$t2 = new Date();

                    callback(JSON.parse(data.data));
                })
                .fail(function () {
                    console.log("Compile 'extent' is failed!")
                });
    }

    //compile by lol lat radius (geo)
    NetLayout.prototype._compileByLonLatRadius = function (extent, callback, notImplementedCallback) {
        var radius = Math.max(extent[2] - extent[0], extent[3] - extent[1]) / 2;
        var xc = (extent[0] + extent[2]) / 2;
        var yc = (extent[1] + extent[3]) / 2;
        var lonlat = this.transformToLonLat([xc, yc]);

        var _this = this;

        $.get("api/schematics/compile-within", { lon: lonlat[0], lat: lonlat[1], xc: xc, yc: yc, radius: radius, mode: this._mode, ts: getTimeStamp() })
                .done(function (data) {
                    if (!data.data) {
                        if (notImplementedCallback) {
                            notImplementedCallback.call(_this, extent, callback);
                        }

                        console.log("Compile 'within' is failed (internal error)!")
                        return;
                    }

                    _this._$t2 = new Date();

                    callback(JSON.parse(data.data));
                })
                .fail(function () {
                    console.log("Compile 'within' is failed!")
                });
    }

    NetLayout.prototype._drawSchematics = function (data) {
        //Подготовка стилей
        delete this._olStyles;

        var styles = {};
        var olStyles = {};

        var _this = this;
        data.styles.forEach(function (s) {
            styles[s.n] = s.v;
            olStyles[s.n] = _this._getStyle(s.v);
        });

        this._olStyles = olStyles;

        this._setGeometry(this._activeLayer, null);
        this._setGeometry(this._snapLayer, null);
        this._setGeometry(this._labelsLayer, null);

        this._drawSchematicsData(data.active, this._activeLayer, styles);
        this._drawSchematicsData(data.snap, this._snapLayer, styles);
        this._drawSchematicsData(data.labels, this._labelsLayer, styles);

        this._updateStyles();

        if (!this._initialized && !this._fitDefined && !this._environmentRestored) {
            this._fitByLayer(this._activeLayer);
            this._initialized = true;
        }
    }

    NetLayout.prototype._drawSchematicsData = function (primitives, layer, styles) {
        if (!primitives) {
            return;
        }

        var _this = this;

        primitives.forEach(function (p) {
            var f = _this._buildFeatures(p.g, styles[p.s], p.txt, p.r);

            f.forEach(function (i) {
                i.set("elementType", p.t);
                i.set("elementId", p.id);

                if (p.p) {
                    i.set("parentElementType", p.p.t);
                    i.set("parentElementId", p.p.id);
                }
            });


            layer.getSource().addFeatures(f);
        });
    }

    NetLayout.prototype._initLayers = function () {
        BaseLayout.prototype._initLayers.apply(this, arguments);

        //geo        
        this._geoLayer = new ol.layer.Tile({
            source: new ol.source.OSM()
        })
        this._geoLayer.setZIndex(-100);

        //box
        this._boxLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                zIndex: -99,
                stroke: {
                    color: "rgba(255, 0, 0, 0.1)",
                    width: 0,
                },
                fill: {
                    fill: true,
                    color: "rgba(255, 0, 0, 0.1)",
                },
            })
        });
        this._boxLayer.set("role", "system");

        //labels
        this._labelsLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
        });
        this._labelsLayer.set("role", "auxiliary");
        this._labelsLayer.set("zoom-min", MIN_LABELS_ZOOM);

        //snap
        this._snapLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                zIndex: 100,
                stroke: {
                    color: "#00FFFF",
                    width: 2,
                },
                image: {
                    radius: 5,
                    stroke: {
                        color: "#0000FF",
                        width: 1
                    },
                    fill: {
                        color: "#00FFFF",
                    }
                }
            })
        });
        this._snapLayer.set("role", "system");
        this._snapLayer.set("snap", true);

        //active
        this._activeLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                zIndex: -1,
                stroke: {
                    color: "#000000",
                    width: 1,
                    lineDash: [10, 10],
                },
                fill: {
                    fill: true,
                    color: "rgba(0, 0, 0, 0.1)",
                },
                image: {
                    radius: 5,
                    stroke: {
                        color: "#0000FF",
                        width: 1
                    },
                    fill: {
                        color: "#FF0000",
                    }
                }
            })
        });

        this._layers.push(this._geoLayer);
        this._layers.push(this._boxLayer);
        this._layers.push(this._labelsLayer);
        this._layers.push(this._activeLayer);
        this._layers.push(this._snapLayer);
    }







    //public
    NetLayout.prototype.refresh = function () {
        this._compile();
    }
})();