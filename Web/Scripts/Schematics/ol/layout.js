﻿"use strict";

console.log("модуль schematics.ol.layout.js загружен");

(function () {
    //export

    if (!window.s$api)
        window.s$api = {};

    window.s$api.BaseLayout = BaseLayout;



    //constants
    var CENTER = [4187600, 7509200]
    var ZOOM = 20;
    var MIN_ZOOM = 16;
    var MAX_ZOOM = 28;

    var MIN_GRID_CELL_SIZE = 0.25;
    var MIN_GRID_ZOOM = 16;
    var MIN_GRID_CELLS = 4;
    var MAX_GRID_CELLS = 100;




    //implementation
    //-------Базовый layout-------
    function BaseLayout(id) {
        this.id = id;
        this.mapDiv = $("#" + id);

        //protected инициализация слоев
        this._initLayers();

        //инициализация карты
        this._map = new ol.Map({
            controls: ol.control.defaults({
                attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                    collapsible: false
                })
            }).extend([new ol.control.ScaleLine()]),
            layers: this._layers,
            target: this.id,
            view: new ol.View({
                center: CENTER,
                zoom: ZOOM,
                minZoom: MIN_ZOOM,
                maxZoom: MAX_ZOOM,
                projection: new ol.proj.Projection({
                    code: 'EPSG:3857',
                    units: 'm'
                })
            })
        });

        //инициализация инструментов рисования
        this._initDrawing();

        //Перерисовка сетки
        var _this = this;
        this._map.on('moveend', function () {
            if (!_this._showGrid)
                return;

            _this._drawGrid();
        });

        this._map.on("pointermove", function (e) {
            if (!_this._animateMouseMove)
                return;

            _this._animateMouseMove.forEach(function (callback) {
                callback(e);
            });
        });


        //remove DoubleClickZoom
        this._map.getInteractions().getArray().forEach(function (i) {
            if (i instanceof ol.interaction.DoubleClickZoom) {
                _this._map.removeInteraction(i);
                return;
            }
        });

        this._map.getView().on('change:resolution', function (e) {
            var zoom = _this._map.getView().getZoom();
            if (zoom - Math.floor(zoom) != 0)
                return;

            _this._showZoom();
            _this._updateStyles();

            _this._layers.forEach(function (l) {
                var zoomMin = l.get("zoom-min");
                var visible = !zoomMin || zoomMin < zoom;

                var zoomMax = l.get("zoom-max");
                visible = visible && (!zoomMax || zoomMax > zoom);

                if (zoomMin == undefined && zoomMax == undefined) {
                    return;
                }

                if (l.getVisible() != visible) {
                    l.setVisible(visible);
                }
            });
        })
        
        //Включение сетки
        this._showGrid = true;
        setTimeout(function () {
            _this.showGrid(_this._showGrid);
        }, 500);
    }










    //public
    BaseLayout.prototype.setZoomBar = function (zoomBarId) {
        if (!zoomBarId) {
            delete this._zoomBar;
            return;
        }

        this._zoomBar = $("#" + zoomBarId);
    }

    BaseLayout.prototype._showZoom = function () {
        if (!this._zoomBar) {
            return;
        }

        this._zoomBar.empty()
                    .append("<span>Текущий масштаб: <b>" + this._map.getView().getZoom() + "</b></span>");
    }

    BaseLayout.prototype.getZooms = function () {
        var result = [];
        for (var i = MIN_ZOOM; i < MAX_ZOOM + 1; i++) {
            result.push(i);
        }

        return result;
    }

    BaseLayout.prototype.getZoom = function () {
        return this._map.getView().getZoom();
    }

    BaseLayout.prototype.setZoom = function (zoom) {
        this._map.getView().setZoom(zoom);
        this._updateStyles();
    }

    BaseLayout.prototype.getNominalZoom = function () {
        return this._nominalZoom;
    }

    BaseLayout.prototype.setNominalZoom = function (zoom) {
        if (this._nominalZoom == zoom)
            return;

        this._nominalZoom = zoom;
        this._updateStyles();
    }

    BaseLayout.prototype.saveEnvironment = function () {
        if (typeof (Storage) == "undefined") {
            console.log("Local storage is unsupported!");
        }

        localStorage.setItem("map-center", JSON.stringify(this._map.getView().getCenter()));
        localStorage.setItem("map-zoom", JSON.stringify(this._map.getView().getZoom()));
        localStorage.setItem("nominal-zoom", JSON.stringify(this.getNominalZoom()));
    }

    BaseLayout.prototype.restoreEnvironment = function () {
        if (typeof (Storage) == "undefined") {
            console.log("Local storage is unsupported!");
        }

        try {
            var nzoom = parseInt(localStorage.getItem("nominal-zoom"));

            if (nzoom) {
                this.setNominalZoom(nzoom);
            }

            var center = localStorage.getItem("map-center");
            var zoom = parseInt(localStorage.getItem("map-zoom"));

            if (center && zoom) {
                this._map.getView().setCenter(JSON.parse(center));
                this._map.getView().setZoom(JSON.parse(zoom));
            }

            this._environmentRestored = true;

        } catch (ex) {}
    }

    BaseLayout.prototype.show = function () {
        this.mapDiv.removeClass("schematics-hidden")
                      .addClass("schematics-visible");
    }

    BaseLayout.prototype.hide = function () {
        this.mapDiv.removeClass("schematics-visible")
                      .addClass("schematics-hidden");
    }

    BaseLayout.prototype.toggleGrid = function () {
        this._showGrid = !this._showGrid;
        this.showGrid(this._showGrid);
    }

    BaseLayout.prototype.showGrid = function (show) {
        this._showGrid = show;

        if (!show) {
            this._gridLinearLayer.getSource().clear();
            this._gridPointLayer.getSource().clear();
            return;
        }

        this._drawGrid();
    }

    BaseLayout.prototype.getDefaultStyle = function () {
        return {
            stroke: {
                color: "#000000",
                width: 1
            },
            fill: {
                fill: false,
                color: "#000000",
            },
            image: {
                radius: 5,
                stroke: {
                    color: "#000000",
                    width: 1
                },
                fill: {
                    fill: true,
                    color: "#000000",
                }
            },
            text: {
                font: "Courier New",
                color: "#000000",
                size: 10,
            }
        }
    }

    BaseLayout.prototype.transformToLonLat = function (coordinate) {
        return ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326');
    }

    BaseLayout.prototype.getExtent = function () {
        return this._map.getView().calculateExtent(this._map.getSize());
    }

    BaseLayout.prototype.startDrawSymbol = function (animationStyles, rotation) {
        this.startDraw("Point", [{ type: "Point", coordinates: [0, 0] }], [0, 0], animationStyles, rotation);
    }

    BaseLayout.prototype.startDraw = function (geometryType, animationGeometry, animationStartPosition, animationStyles, rotation) {
        this._startAction(2, this._commonLayer, geometryType);

        if (animationGeometry) {
            var _this = this;

            if (animationStyles && animationStyles.length) {
                animationStyles.forEach(function (s) {
                    _this._animationLayer.getSource().addFeatures(_this._buildFeatures(animationGeometry, s, undefined, rotation));
                });

            } else {
                this._animationLayer.getSource().addFeatures(this._buildFeatures(animationGeometry));
            }

            var cagp = animationStartPosition || [0, 0];
            cagp = [cagp[0], cagp[1]];
            this._drawAnimation = function (e) {
                var c = e.coordinate;
                var delta = [c[0] - cagp[0], c[1] - cagp[1]];
                _this._animationLayer.getSource().getFeatures().forEach(function (f) {
                    f.getGeometry().translate(delta[0], delta[1]);
                });

                cagp[0] += delta[0];
                cagp[1] += delta[1];
            };

            this.addMouseMoveAnimation(this._drawAnimation);
        }
    };

    BaseLayout.prototype.addMouseMoveAnimation = function (f) {
        this._animateMouseMove = this._animateMouseMove || [];
        this._animateMouseMove.push(f);
    }

    BaseLayout.prototype.removeMouseMoveAnimation = function (f) {
        if (!this._animateMouseMove) {
            return;
        }

        for (var i = 0; i < this._animateMouseMove.length; i++) {
            if (this._animateMouseMove[i] === f) {
                this._animateMouseMove.splice(i, 1);
                break;
            }
        }
    }

    BaseLayout.prototype.startModify = function () {
        this._startAction(1);
    };

    BaseLayout.prototype.startSelect = function () {
        this._startAction(0);
    };

    BaseLayout.prototype.startDelete = function () {
        this._startAction(3);
    };

    BaseLayout.prototype.getCurrentAction = function () {
        return this._action;
    };

    BaseLayout.prototype.getStyle = function () {
        return this._style;
    }

    BaseLayout.prototype.setStyle = function (style) {
        this._stopAction();
        this._style = style;
        this._refreshLayer(this._commonLayer);
    }

    BaseLayout.prototype.getGeometry = function () {
        return this._getGeometry(this._commonLayer);
    }

    BaseLayout.prototype.setGeometry = function (geometry) {
        this._stopAction();
        this._setGeometry(this._commonLayer, geometry || []);
    }

    BaseLayout.prototype.getAnimationGeometry = function () {
        return this._getGeometry(this._animationLayer);
    }

    BaseLayout.prototype.setAnimationGeometry = function (geometry) {
        this._setGeometry(this._animationLayer, geometry || []);
    }

    BaseLayout.prototype.fit = function () {
        this._fitByLayer(this._commonLayer);
    }

    BaseLayout.prototype.fitByExtent = function (extent) {
        this._fitByExtent(extent);
    }

    BaseLayout.prototype.scale = function (factor) {
        this._layers.forEach(function (l) {
            if (l.get("role") != "system") {
                l.getSource().getFeatures().forEach(function (f) {
                    f.getGeometry().scale(factor, factor, [0, 0]);
                });
            }
        })
    }
    
    BaseLayout.prototype.makeLineStringOrthogonal = function (geometry) {
        var coordinates = geometry["coordinates-string"];

        if (coordinates.length > 2) {
            var dx = 0;
            var dy = 0;

            for (var i = 1; i < coordinates.length - 1; i++) {
                dx = Math.abs(coordinates[i][0] - coordinates[i - 1][0]);
                dy = Math.abs(coordinates[i][1] - coordinates[i - 1][1]);

                if (dx != 0 && dy != 0) {
                    if (dx > dy) {
                        coordinates[i][1] = coordinates[i - 1][1]
                    } else {
                        coordinates[i][0] = coordinates[i - 1][0]
                    }
                }
            }

            dx = coordinates[coordinates.length - 2][0] - coordinates[coordinates.length - 3][0];
            dy = coordinates[coordinates.length - 2][1] - coordinates[coordinates.length - 3][1];

            if (dx == 0) {
                coordinates[coordinates.length - 2][1] = coordinates[coordinates.length - 1][1]
            } else {
                coordinates[coordinates.length - 2][0] = coordinates[coordinates.length - 1][0]
            }
        }
    }













    //protected

    //-------drawing fit and grid-------
    BaseLayout.prototype._updateStyles = function () {
        var zoom = this.getZoom();
        var nzoom = this.getNominalZoom();

        var k = (nzoom) ? Math.pow(2, (zoom - nzoom)) : 1;

        var _this = this;
        this._layers.forEach(function (l) {
            if (l.getSource().getFeatures) {
                l.getSource().getFeatures().forEach(function (f) {
                    var s = f.get("_style");
                    if (s) {
                        if (f.get("_znzratio") != k) {
                            var fs = f.getStyle();
                            var stroke = fs.getStroke();
                            if (stroke) {
                                var cwidth = f.get("_style").stroke.width * k;

                                if (cwidth < 1)
                                    cwidth = 0;

                                if (cwidth)
                                    stroke.setWidth(cwidth);
                            }

                            if (s.text && s.text._getFont && fs.getText()) {
                                var nfsize = s.text.size * k;

                                if (nfsize < 1)
                                    nfsize = 0;

                                if (nfsize)
                                    fs.getText().setFont(s.text._getFont(nfsize));
                            }

                            f.set("_znzratio", k);
                        }
                    }
                });
            }
        });
    }

    BaseLayout.prototype._fitByLayer = function (layer) {
        var s = layer.getSource();

        if (s.getFeatures().length == 0)
            return;

        this._fitByExtent(s.getExtent());
    }

    BaseLayout.prototype._fitByExtent = function (extent) {
        this._map.getView().fit(extent, this._map.getSize());
        this._fitDefined = true;
    }

    BaseLayout.prototype._startAction = function (action, layer, type, callback) {
        this._stopAction();

        this._action = action;
        this._actionLayer = layer;
        this._actionType = type;
        this._actionCallback = callback;

        this._map._select.setActive(action == 0);
        this._map._modify.setActive(action == 1);
        this._map._delete.setActive(action == 3);

        if (layer) {
            layer._draw.setActive(action == 2, type);
        }
    }

    BaseLayout.prototype._stopAction = function (fireOnStop) {
        if (this._action == undefined) {
            return;
        }

        if (this._drawAnimation) {
            this._animationLayer.getSource().clear();
            this.removeMouseMoveAnimation(this._drawAnimation);

            delete this._drawAnimation;
        }

        var restoreFeatureStyle = function(f) {
            var fs = f.get("fs");
            if (fs) {
                f.setStyle(fs);
            }
        }

        this._map._selectedFeatures.forEach(function (f) {
            restoreFeatureStyle(f);
        });

        this._map._featuresToModify.forEach(function (f) {
            restoreFeatureStyle(f);
        });

        this._map._select.setActive(false);
        this._map._modify.setActive(false);
        this._map._delete.setActive(false);

        if (this._actionLayer) {
            this._actionLayer._draw.setActive(false);
        }

        if (fireOnStop) {
            var _this = this;
            if (this._stopActionCallbacks) {
                this._stopActionCallbacks.forEach(function (c) {
                    c(_this._action);
                });
            }
        }

        delete this._action;
    }

    BaseLayout.prototype._buildSelectionStyle = function(style, feature) {
        var result = this._getStyle({
            stroke: {
                color: "#00FFFF",
                width: 1
            },
            image: {
                radius: 5,
                stroke: {
                    color: "#FF0000",
                    width: 1
                },
                fill: {
                    color: "#00FFFF",
                }
            }
        });

        if (style.getText()) {
            result.setText(style.clone().getText());
            result.getText().setStroke(result.getStroke());

            return result;
        }

        var g = style.getGeometry();
        if (g) {
            result.setGeometry(function (f) {
                var gc = g(f);
                var ga = gc.getGeometries();

                ga.push(feature.getGeometry());
                gc.setGeometries(ga);

                return gc;
            });
        }

        result.getStroke().setWidth(style.getStroke().getWidth() + 2);
        return result;
    }
        
    BaseLayout.prototype._initDrawing = function () {
        var _this = this;
        var _map = this._map;

        //Список слоев для привязок
        _map._snap = [];

        //Поиск других слоев для snap
        this._layers.forEach(function (l) {
            if (l.get("snap")) {
                _map._snap.push(new ol.interaction.Snap({
                    source: l.getSource(),
                }));
            }
        });

        //Snap к сетке
        _map._snap.push(new ol.interaction.Snap({
            source: this._gridPointLayer.getSource(),
        }));

        var setActiveSnap = function (active) {
            _map._snap.forEach(function (s) {
                s.setActive(active);
            });
        }

        var selectionFilter = function (feature, layer) {
            if (!layer)
                return false;

            return layer.get("role") != "system";
        }

        var selectionModifyFilter = function (feature, layer) {
            if (!layer)
                return false;

            return layer.get("role") != "system" && layer.get("role") != "auxiliary";
        }

        var onSelect = function (evt) {
            evt.selected.forEach(function (feature) {
                var fs = feature.getStyle();
                if (fs) {
                    if (!feature.get("fss")) {
                        feature.set("fs", fs);
                        feature.setStyle(_this._buildSelectionStyle(fs, feature));
                    }

                    feature.set("fss", "s");
                }
            });

            evt.deselected.forEach(function (feature) {
                var fs = feature.get("fs");
                if (fs && feature.get("fss") == "s") {
                    feature.setStyle(fs);
                    feature.set("fss", undefined);
                }
            });
        };


        var onHighlight = function (evt) {
            evt.selected.forEach(function (feature) {
                var fs = feature.getStyle();
                if (fs && !feature.get("fss")) {
                    feature.set("fs", fs);
                    feature.set("fss", "h");
                    feature.setStyle(_this._buildSelectionStyle(fs, feature));
                }
            });

            evt.deselected.forEach(function (feature) {
                var fs = feature.get("fs");
                if (fs && feature.get("fss") == "h") {
                    feature.setStyle(fs);
                    feature.set("fss", undefined);
                }
            });
        };

        var Select = {
            init: function () {
                //selection
                this.select = new ol.interaction.Select({ filter: selectionFilter });
                this.select.on("select", onSelect);

                _map.addInteraction(this.select);

                //highlight
                this.highlight = new ol.interaction.Select({
                    condition: ol.events.condition.pointerMove,
                    filter: selectionFilter,
                });
                this.highlight.on("select", onHighlight);

                _map.addInteraction(this.highlight);

                //selected features
                _this._map._selectedFeatures = this.select.getFeatures();

                //setEvents
                this.setEvents();
                this.setActive(false);
            },
            setEvents: function () {
                this.select.on('change:active', function () {
                    _this._map._selectedFeatures.clear();
                });
            },
            setActive: function (active) {
                this.select.setActive(active);
                this.highlight.setActive(active);
            }
        };
        Select.init();

        var Modify = {
            init: function () {
                //selection
                this.select = new ol.interaction.Select({ filter: selectionModifyFilter });
                this.select.on("select", onSelect);

                _map.addInteraction(this.select);

                //highlight
                this.highlight = new ol.interaction.Select({
                    condition: ol.events.condition.pointerMove,
                    filter: selectionModifyFilter,
                });
                this.highlight.on("select", onHighlight);

                _map.addInteraction(this.highlight);

                //modify
                this.modify = new ol.interaction.Modify({
                    features: this.select.getFeatures()
                });
                _map.addInteraction(this.modify);

                //features to modify
                _this._map._featuresToModify = this.select.getFeatures();

                //setEvents
                this.setEvents();
                this.setActive(false);
            },
            setEvents: function () {
                this.select.on('change:active', function () {
                    _this._map._featuresToModify.clear();
                });
            },
            setActive: function (active) {
                this.select.setActive(active);
                this.modify.setActive(active);
                this.highlight.setActive(active);
                setActiveSnap(active)
            }
        };
        Modify.init();

        var Delete = {
            init: function () {
                //selection
                this.select = new ol.interaction.Select({ filter: selectionFilter });
                this.select.on("select", onSelect);

                _map.addInteraction(this.select);

                //highlight
                this.highlight = new ol.interaction.Select({
                    condition: ol.events.condition.pointerMove,
                    filter: selectionFilter,
                });
                this.highlight.on("select", onHighlight);

                _map.addInteraction(this.highlight);

                //setEvents
                this.setEvents();
                this.setActive(false);
            },
            setEvents: function () {
                var selectedFeatures = this.select.getFeatures();

                this.select.on('change:active', function () {
                    selectedFeatures.clear();
                });

                var _select = this.select;
                this.select.on('select', function () {
                    if (selectedFeatures.getLength() > 0) {
                        if (confirm("Удалить выбранный элемент?")) {
                            selectedFeatures.forEach(function (f) {
                                _select.getLayer(f).getSource().removeFeature(f);                                
                            }, selectedFeatures);

                            if (_this._map._delete._onAfterDelete) {
                                _this._map._delete._onAfterDelete(selectedFeatures);
                            }

                            selectedFeatures.clear();
                        }
                    }
                });
            },
            setActive: function (active) {
                this.select.setActive(active);
                this.highlight.setActive(active);
            }
        };
        Delete.init();

        _map._modify = Modify;
        _map._select = Select;
        _map._delete = Delete;


        this._layers.forEach(function (layer, index, array) {
            var Draw = {
                init: function () {
                    _map.addInteraction(this.Point);
                    this.Point.setActive(false);

                    _map.addInteraction(this.SinglePoint);
                    this.SinglePoint.setActive(false);

                    this.SinglePoint.on('drawend', function (e) {
                        var s = layer.getSource();
                        if (s.getFeatures().length > 0) {
                            s.clear();
                        }

                        if (_this._actionCallback) {
                            _this._actionCallback(e);
                        }
                    });

                    _map.addInteraction(this.LineString);
                    this.LineString.setActive(false);

                    _map.addInteraction(this.OrthogonalLineString);
                    this.OrthogonalLineString.setActive(false);

                    _map.addInteraction(this.Polygon);
                    this.Polygon.setActive(false);

                    _map.addInteraction(this.Circle);
                    this.Circle.setActive(false);

                    _map.addInteraction(this.Box);
                    this.Box.setActive(false);

                    _map.addInteraction(this.Square);
                    this.Square.setActive(false);

                    _map.addInteraction(this.Polygon36);
                    this.Polygon36.setActive(false);

                    this.setActive(false);
                },
                SinglePoint: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Point"
                }),
                Point: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Point"
                }),
                LineString: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "LineString"
                }),
                OrthogonalLineString: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "LineString",
                    geometryFunction: function (coordinates, geometry) {
                        if (!geometry) {
                            geometry = new ol.geom.LineString();
                        }

                        if (coordinates.length > 2) {
                            var dx = Math.abs(coordinates[coordinates.length - 2][0] - coordinates[coordinates.length - 3][0]);
                            var dy = Math.abs(coordinates[coordinates.length - 2][1] - coordinates[coordinates.length - 3][1]);

                            if (dx > dy) {
                                coordinates[coordinates.length - 2][0] = coordinates[coordinates.length - 1][0]
                                coordinates[coordinates.length - 2][1] = coordinates[coordinates.length - 3][1]
                            } else {
                                coordinates[coordinates.length - 2][1] = coordinates[coordinates.length - 1][1]
                                coordinates[coordinates.length - 2][0] = coordinates[coordinates.length - 3][0]
                            }

                            _this._$lastCoordinate = coordinates[coordinates.length - 1];
                        }

                        geometry.setCoordinates(coordinates);
                        return geometry;
                    }
                }),
                Polygon: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Polygon"
                }),
                Circle: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Circle"
                }),
                Box: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Circle",
                    geometryFunction: ol.interaction.Draw.createBox()
                }),
                Square: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Circle",
                    geometryFunction: ol.interaction.Draw.createRegularPolygon(4)
                }),
                Polygon36: new ol.interaction.Draw({
                    source: layer.getSource(),
                    type: "Circle",
                    geometryFunction: ol.interaction.Draw.createRegularPolygon(36)
                }),
                getActive: function () {
                    return this.activeType ? this[this.activeType].getActive() : false;
                },
                setActive: function (active, type) {
                    setActiveSnap(active)
                    if (active) {
                        this.activeType && this[this.activeType].setActive(false);
                        this[type].setActive(true);
                        this.activeType = type;
                    } else {
                        this.activeType && this[this.activeType].setActive(false);
                        this.activeType = null;
                    }
                }
            };
            Draw.init();

            layer._draw = Draw;
        });

        _map._snap.forEach(function (s) {
            _map.addInteraction(s);
        });

        setActiveSnap(false);
    }

    BaseLayout.prototype._drawGrid = function () {
        var sl = this._gridLinearLayer.getSource();
        sl.clear();

        var sp = this._gridPointLayer.getSource();
        sp.clear();

        var zoom = this._map.getView().getZoom();
        if (zoom < MIN_GRID_ZOOM) {
            return;
        }            

        var extent = this._map.getView().calculateExtent(this._map.getSize())

        var xmin = Math.floor(extent[0]);
        var ymin = Math.floor(extent[1]);

        var xmax = Math.floor(extent[2]) + 1;
        var ymax = Math.floor(extent[3]) + 1;

        var step = MIN_GRID_CELL_SIZE;
        
        //---Расчет размера сетки---
        var lmax = Math.max(xmax - xmin, ymax - ymin);

        if (lmax / step < MIN_GRID_CELLS) {
            return;
        }

        while (lmax / step > MAX_GRID_CELLS) {
            step *= 2;
        }
        //--------------------------

        this._gridStep = step;

        var xs = xmin - xmin % step;
        var ys = ymin - ymin % step;

        var multiLineH = {
            type: "Feature",
            properties: null,
            geometry: {
                type: "MultiLineString",
                coordinates: []
            }
        }

        var multiLineV = {
            type: "Feature",
            properties: null,
            geometry: {
                type: "MultiLineString",
                coordinates: []
            }
        }

        var lines = {
            type: "FeatureCollection",
            features: [multiLineH, multiLineV]
        };

        var points = {
            type: "FeatureCollection",
            features: []
        }

        var x = xs;
        while (x < xmax)
        {            
            multiLineV.geometry.coordinates.push([[x, ymin], [x, ymax]]);

            var multiPoint = {
                type: "Feature",
                properties: null,
                geometry: {
                    type: "MultiPoint",
                    coordinates: []
                }
            }

            for (var i = ys; i < ymax; i += step) {
                multiPoint.geometry.coordinates.push([x, i]);
            }

            points.features.push(multiPoint);

            x += step;
        }

        var y = ys;
        while (y < ymax) {
            multiLineH.geometry.coordinates.push([[xmin, y], [xmax, y]]);

            y += step;
        }

        var geojson = new ol.format.GeoJSON();

        sl.addFeatures(geojson.readFeatures(lines));
        sp.addFeatures(geojson.readFeatures(points));
    }








    //-------geometry convertation-------
    BaseLayout.prototype._convertGeometryFromGeoJSON = function (geometry) {
        var result = {
            type: geometry.type
        }

        switch (geometry.type)
        {
            case "Point": 
                result.coordinates = geometry.coordinates;
                break;
            case "LineString":
            case "MultiPoint":
                result["coordinates-string"] = geometry.coordinates;
                break;
            case "Polygon":
            case "MultiLineString":
                result["coordinates-strings"] = geometry.coordinates;
                break;
            case "MultiPolygon":
                result["coordinates-strings-array"] = geometry.coordinates;
                break;
        }

        return result;
    }

    BaseLayout.prototype._convertGeometryToGeoJSON = function (geometry) {
        var result = {
            type: geometry.type
        }

        switch (geometry.type) {
            case "Point": 
                result.coordinates = geometry.coordinates;
                break;
            case "LineString":
            case "MultiPoint":
                result.coordinates = geometry["coordinates-string"];
                break;
            case "Polygon":
            case "MultiLineString":
                result.coordinates = geometry["coordinates-strings"];
                break;
            case "MultiPolygon": 
                result.coordinates = geometry["coordinates-strings-array"];
                break;
        }

        return result;
    }

    function getCircleGeometry(g) {
        var center = g.getCenter();
        return {
            type: "Circle",
            coordinates: [center[0], center[1], g.getRadius()],
        }
    }

    BaseLayout.prototype._getFeatureGeometry = function (f) {
        var g = f.getGeometry();

        if (g.getType() == "Circle") {
            return getCircleGeometry(g);
        }

        return this._convertGeometryFromGeoJSON(JSON.parse(new ol.format.GeoJSON().writeFeature(f)).geometry);
    }

    BaseLayout.prototype._getGeometry = function (data) {
        var _this = this;
        var result = [];

        var f = data.getSource ? data.getSource().getFeatures() : data;
        var features = JSON.parse(new ol.format.GeoJSON().writeFeatures(f));

        var gc = [];
        features.features.forEach(function (f) {
            gc.push(_this._convertGeometryFromGeoJSON(f.geometry));
        })

        //Включаем только то, что сконвертировано из JSON
        gc.forEach(function (g) {
            if (g.type != "GeometryCollection")
                result.push(g);
        });

        //Добавляем окружности отдельно
        f.forEach(function (i) {
            var g = i.getGeometry();
            if (g.getType() == "Circle") {
                result.push(getCircleGeometry(g))
            }
        })

        return result;
    }

    BaseLayout.prototype._setGeometry = function (layer, geometry, style) {
        var s = layer.getSource();
        s.clear();
        s.addFeatures(this._buildFeatures(geometry, style));
    }

    BaseLayout.prototype._buildFeatures = function (geometry, style, text, rotation) {
        var _izoom = this._map.getView().getZoom();
        var _this = this;

        var features = {
            type: "FeatureCollection",
            features: []
        }

        //Список оркужностей
        var circles = [];

        //Если не окружность, то конвертируем в JSON и добавляем в список features
        if (geometry) {
            geometry.forEach(function (g) {
                if (g.type == "Circle") {
                    circles.push(g);
                } else {
                    features.features.push({
                        type: "Feature",
                        properties: null,
                        geometry: _this._convertGeometryToGeoJSON(g)
                    })
                }
            });
        }

        //features openlayers по features JSON
        var f = new ol.format.GeoJSON().readFeatures(features);

        //Отдельно добавляем окружности
        circles.forEach(function (c) {
            f.push(new ol.Feature({
                geometry: new ol.geom.Circle([c.coordinates[0], c.coordinates[1]], c.coordinates[2]),
            }))
        });

        if (rotation) {
            f.forEach(function (f) {
                f.set("_rotation", rotation);
            });
        }

        if (style) {
            var s = this._getStyle(style, text, rotation);
            f.forEach(function (f) {
                f.set("_style", style);
                f.setStyle(s);
            });
        }

        return f;
    }









    //-------layers-------
    BaseLayout.prototype._refreshLayer = function (layer) {
        var s = layer.getSource();
        var f = s.getFeatures();

        s.clear();
        s.addFeatures(f);
    }

    BaseLayout.prototype._initLayers = function () {
        var gridStyle = this._getStyle({
            stroke: {
                color: "#CCCCCC",
                width: 1,
            }
        });

        this._gridLinearLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: gridStyle
        });
        this._gridLinearLayer.set("role", "system");

        this._gridPointLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: gridStyle
        });
        this._gridPointLayer.set("role", "system");

        this._animationLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle(this.getDefaultStyle()),
        });
        this._animationLayer.set("role", "system");

        var _this = this;
        this._commonLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: function () {
                return _this._style ? _this._getStyle(_this._style) : _this._getStyle(_this.getDefaultStyle())
            }
        });
        this._commonLayer.set("snap", true);


        this._layers = [
            this._gridLinearLayer,
            this._gridPointLayer,
            this._animationLayer,
            this._commonLayer,
        ];
    }












    //-------styles-------
    BaseLayout.prototype._getStyle = function (style, text, rotation) {
        var olStyle = this._olStyles ? this._olStyles[style.id] : null;

        if (olStyle && !text) {
            return olStyle;
        }

        if (text) {
            return new ol.style.Style({
                zIndex: getPropertyValue(style, "zIndex", 0),
                stroke: this._getStyleStroke(style.stroke),
                text: this._getStyleText(style.text, text, rotation)
            })
        }

        var result = new ol.style.Style({
            zIndex: getPropertyValue(style, "zIndex", 0),
            fill: this._getStyleFill(style.fill),
            stroke: this._getStyleStroke(style.stroke),
            image: this._getStyleImage(style.image),
        })

        if (style.useStyleGeometry && style.geometry && style.geometry.length) {
            var _this = this;
            result.setGeometry(function (feature) {
                var g = feature.getGeometry();

                var p = g.getCoordinates();
                var f = _this._buildFeatures(style.geometry);

                var g = [];
                f.forEach(function (i) {
                    g.push(i.getGeometry());
                });

                var gs = new ol.geom.GeometryCollection(g);
                gs.translate(p[0], p[1]);

                var r = feature.get("_rotation") || rotation;
                if (r) {
                    gs.rotate(r / 180 * Math.PI, p);
                }

                return gs;
            });
        }

        return result
    }

    BaseLayout.prototype._getStyleStroke = function (stroke) {
        if (!stroke)
            return null;

        return new ol.style.Stroke({
            color: getPropertyValue(stroke, "color", "#000000"),
            width: getPropertyValue(stroke, "width", 1),
            lineCap: getPropertyValue(stroke, "lineCap", undefined),
            lineJoin: getPropertyValue(stroke, "lineJoin", undefined),
            lineDash: getPropertyValue(stroke, "lineDash", undefined),
        })
    }

    BaseLayout.prototype._getStyleFill = function (fill) {
        if (!fill || (fill.fill != undefined && !fill.fill))
            return null;

        return new ol.style.Fill({
            color: getPropertyValue(fill, "color", "#FFFFFF")
        })
    }

    BaseLayout.prototype._getStyleImage = function (image) {
        if (!image)
            return null;

        return new ol.style.Circle({
            radius: getPropertyValue(image, "radius", 1),
            fill: this._getStyleFill(image.fill),
            stroke: this._getStyleStroke(image.stroke)
        })
    }

    BaseLayout.prototype._getStyleText = function (textStyle, text, rotation) {
        if (!textStyle || !text)
            return null;

        textStyle._getFont = function (size) {
            return getPropertyValue(textStyle, "weight", "normal") + " " + size + "px " + getPropertyValue(textStyle, "font", "Times New Roman")
        }

        return new ol.style.Text({
            textAlign: getPropertyValue(textStyle, "textAlign", "center"),
            textBaseline: getPropertyValue(textStyle, "textBaseline", "bottom"),
            rotation: getPropertyValue(textStyle, "rotation", 0) + rotation,
            font: textStyle._getFont(getPropertyValue(textStyle, "size", 20)),
            fill: new ol.style.Fill({ color: getPropertyValue(textStyle, "color", "black") }),
            stroke: new ol.style.Stroke({ color: getPropertyValue(textStyle, "color", "black"), width: 1 }),
            text: text
        })

     }













    //utilities
    function getPropertyValue(object, name, defaultValue)
    {
        if (!object)
            return defaultValue;

        var value = object[name];

        if (value == undefined)
            return defaultValue;

        return value;
    }
})();