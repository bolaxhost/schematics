﻿"use strict";

console.log("модуль schematics.ol.style-layout.js загружен");

(function () {
    //export
    window.s$api.CreateStyleLayout = function (id) {
        return new StyleLayout(id);
    }

    //constants
    var ZOOM = 24;


    //import
    var BaseLayout = window.s$api.BaseLayout;





    //implementation
    //-------layout для стиля-------
    function StyleLayout(id) {
        BaseLayout.apply(this, arguments);

        var _this = this;
        this.on = function (event, callback) {
            if (event == "modifyscopechanged") {
                _this._map._featuresToModify.on("add", function () {
                    callback(_this._map._featuresToModify.getLength());
                });
                _this._map._featuresToModify.on("remove", function () {
                    callback(_this._map._featuresToModify.getLength());
                });
                return;
            }
        }

        this._map.getView().setZoom(ZOOM);
        this._map.getView().setCenter([0, 0]);
    }
    
    StyleLayout.prototype = Object.create(BaseLayout.prototype);

    StyleLayout.prototype.constructor = StyleLayout;





    //--protected--
    StyleLayout.prototype._initLayers = function () {
        BaseLayout.prototype._initLayers.apply(this, arguments);

        this._auxiliaryLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                image: {
                    radius: 5,
                    stroke: {
                        color: "#000000",
                        width: 1
                    },
                    fill: {
                        color: "#FF0000",
                    }
                }
            })
        });

        this._terminalsLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                image: {
                    radius: 5,
                    stroke: {
                        color: "#0000FF",
                        width: 1
                    },
                    fill: {
                        color: "#00FFFF",
                    }
                }
            })
        });

        var _this = this;
        this._terminalsLayer.getSource().on("change", function () {
            _this.drawConnectivityTopology(_this._ct, _this._cn);
        });


        this._connectivityLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                stroke: {
                    color: "rgba(255, 0, 0, 0.2)",
                    width: 2,
                    lineDash: [10, 10],
                }
            })
        });
        this._connectivityLayer.set("role", "system");

        this._bindingPointLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: this._getStyle({
                image: {
                    radius: 7,
                    stroke: {
                        color: "#0000FF",
                        width: 1
                    },
                    fill: {
                        color: "#FF0000",
                    }
                }
            })
        });

        var _this = this;
        this._labelsLayer = new ol.layer.Vector({
            source: new ol.source.Vector({ wrapX: false }),
            style: function () {
                var text = "текст";
                if (_this.getStyle()) {
                    text = _this.getStyle()["label-source"] == 1 ? "Имя" : "Примечание";
                }

                return _this._getStyle({
                    image: {},
                    text: (_this.getStyle() || _this.getDefaultStyle()).text
                }, text);
            }
        });


        this._layers.push(this._auxiliaryLayer);
        this._layers.push(this._terminalsLayer);
        this._layers.push(this._bindingPointLayer);
        this._layers.push(this._connectivityLayer);
        this._layers.push(this._labelsLayer);
    }

    StyleLayout.prototype.setStyle = function (style) {
        BaseLayout.prototype.setStyle.call(this, style);
        this._refreshLayer(this._labelsLayer);

        if (style) {
            this.drawConnectivityTopology(style["connectivity-type"], style.connectivity);
        }
    }





    //public
    StyleLayout.prototype.drawConnectivityTopology = function (ct, cn) {
        this._ct = parseInt(ct || 0);
        this._cn = cn;

        this._setGeometry(this._connectivityLayer, []);

        var p = this._getGeometry(this._terminalsLayer);
        var ga = [];

        switch (this._ct) {
            case 0: {
                var l = { type: "LineString", "coordinates-string": [] }
                p.forEach(function (g) {
                    l["coordinates-string"].push(g.coordinates);
                })

                ga.push(l);

                break;
            }
            case 1: {
                var bp = this._getGeometry(this._bindingPointLayer);

                if (!bp || !bp.length) {
                    return;
                }

                var center = bp[0].coordinates;
                p.forEach(function (g) {
                    ga.push({ type: "LineString", "coordinates-string": [center, g.coordinates] });
                })

                break;
            }
            case 2: {
                if (!cn || !cn.length) {
                    return;
                }

                cn.forEach(function (i) {
                    if (i && i.length && i.length == 2) {
                        var i1 = parseInt(i[0]);
                        var i2 = parseInt(i[1]);

                        if (i1 != i2 && i1 >= 0 && i2 >= 0 && i1 < p.length && i2 < p.length) {
                            ga.push({ type: "LineString", "coordinates-string": [p[i1].coordinates, p[i2].coordinates] });
                        }
                    }
                })

                break;
            }
        }

        if (ga.length) {
            this._setGeometry(this._connectivityLayer, ga);
        }
    }

    StyleLayout.prototype.rotateScope = function (angle) {
        angle = angle / 180 * Math.PI;
        this._map._featuresToModify.forEach(function (f) {
            var g = f.getGeometry();
            var extent = g.getExtent();
            g.rotate(angle, [(extent[0] + extent[2]) / 2, (extent[1] + extent[3]) / 2])
        });
    };

    StyleLayout.prototype.copyScope = function () {
        this._copyScope = this._getGeometry(this._map._featuresToModify.getArray());
        this._copyPosition = [null, null];
        var _this = this;

        this._map._featuresToModify.forEach(function (f) {
            var g = f.getGeometry();
            var extent = g.getExtent();

            if (_this._copyPosition[0] == null || _this._copyPosition[0] > extent[0])
                _this._copyPosition[0] = extent[0]

            if (_this._copyPosition[1] == null || _this._copyPosition[1] < extent[3])
                _this._copyPosition[1] = extent[3]
        });
    };

    StyleLayout.prototype.drawPastePosition = function () {
        this._startAction(2, this._auxiliaryLayer, "SinglePoint", function (e) {
            this.pasteScope(e.feature.getGeometry().getCoordinates());
            this._stopAction();
        });
    }

    StyleLayout.prototype.pasteScope = function (position) {
        var paste = this._buildFeatures(this._copyScope);

        var dx = position[0] - this._copyPosition[0];
        var dy = position[1] - this._copyPosition[1];

        paste.forEach(function (f) {
            var g = f.getGeometry();
            g.translate(dx, dy);
        });

        this._commonLayer.getSource().addFeatures(paste);

        var _this = this;
        setTimeout(function () {
                    _this._auxiliaryLayer.getSource().clear();
        },
        100);
    }

    StyleLayout.prototype.startDrawTerminals = function () {
        this._startAction(2, this._terminalsLayer, "Point");
    };

    StyleLayout.prototype.startDrawLabels = function () {
        this._startAction(2, this._labelsLayer, "Point");
    };

    StyleLayout.prototype.startDrawBindingPoint = function () {
        this._bindingPointLayer.getSource().clear();
        this._startAction(2, this._bindingPointLayer, "SinglePoint");
    };

    StyleLayout.prototype.getBindingPoint = function () {
        var geometry = this._getGeometry(this._bindingPointLayer);
        if (!geometry || geometry.length == 0)
            return null;

        return geometry[0].coordinates;
    }

    StyleLayout.prototype.setBindingPoint = function (coordinates) {
        if (!coordinates) {
            this._bindingPointLayer.getSource().clear();
            return;
        }

        this._stopAction();
        this._setGeometry(this._bindingPointLayer, [{
            type: "Point",
            coordinates: coordinates
        }]);
    }

    StyleLayout.prototype.getTerminals = function () {
        var geometry = this._getGeometry(this._terminalsLayer);
        if (!geometry || geometry.length == 0)
            return null;

        var result = [];
        geometry.forEach(function (e) {
            result.push(e.coordinates);
        })

        return result;
    }

    StyleLayout.prototype.setTerminals = function (terminals) {
        if (!terminals) {
            this._terminalsLayer.getSource().clear();
            return;
        }

        this._stopAction();

        var geometry = [];
        terminals.forEach(function (e) {
            geometry.push({
                type: "Point",
                coordinates: e
            });
        });

        this._setGeometry(this._terminalsLayer, geometry);
    }

    StyleLayout.prototype.getLabels = function () {
        var geometry = this._getGeometry(this._labelsLayer);
        if (!geometry || geometry.length == 0)
            return null;

        var result = [];
        geometry.forEach(function (e) {
            result.push(e.coordinates);
        })

        return result;
    }

    StyleLayout.prototype.setLabels = function (labels) {
        if (!labels) {
            this._labelsLayer.getSource().clear();
            return;
        }

        this._stopAction();

        var geometry = [];
        labels.forEach(function (e) {
            geometry.push({
                type: "Point",
                coordinates: e
            });
        });

        this._setGeometry(this._labelsLayer, geometry);
    }

    //загрузка стиля
    StyleLayout.prototype.load = function (style) {
        if (!style) {
            this.setStyle(null);

            this.setGeometry([]);
            this.setLabels(null);

            this.setBindingPoint(null);
            this.setTerminals(null);

            return;
        }

        this.setStyle(style);

        this.setGeometry(style.geometry);
        this.setLabels(style.labels);

        this.setBindingPoint(style["binding-point"]);
        this.setTerminals(style["terminal-points"]);

        this.fit();
    }

    //сохранение параметров стиля
    StyleLayout.prototype.save = function (style) {
        if (!style) {
            return;
        }

        style.geometry = this.getGeometry();
        style.labels = this.getLabels();

        style["binding-point"] = this.getBindingPoint();
        style["terminal-points"] = this.getTerminals();
    }
})();