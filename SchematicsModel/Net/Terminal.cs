﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  



namespace SchematicsModel.Net
{
    public class Terminal
    {
        [JsonProperty("id")]
        [BsonElement("id")]
        [DocumentProperty(PropertyName = "id")]
        public string Id { get; set; }






        [JsonProperty("coordinates")]
        [BsonElement("coordinates")]
        [DocumentProperty(PropertyName = "coordinates")]
        public double[] Coordinates { get; set; }





        internal bool IsIncident { get; set; }
    }
}
