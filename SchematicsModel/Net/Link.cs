﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

namespace SchematicsModel.Net
{
    public class Link : SchematicsElement
    {
        [JsonProperty("from")]
        [BsonElement("from")]
        [DocumentProperty(PropertyName = "from", Identifier = IdentifierType.EdgeFrom)]
        public string From { get; set; }





        [JsonProperty("to")]
        [BsonElement("to")]
        [DocumentProperty(PropertyName = "to", Identifier = IdentifierType.EdgeTo)]
        public string To { get; set; }






        [JsonProperty("terminal-from")]
        [BsonElement("terminal-from")]
        [DocumentProperty(PropertyName = "terminal-from")]
        public string TerminalFrom { get; set; }







        [JsonProperty("terminal-to")]
        [BsonElement("terminal-to")]
        [DocumentProperty(PropertyName = "terminal-to")]
        public string TerminalTo { get; set; }

        



        [JsonProperty("coordinates-from")]
        [BsonElement("coordinates-from")]
        [DocumentProperty(PropertyName = "coordinates-from")]
        public double[] CoordinatesFrom { get; set; }





        [JsonProperty("coordinates-to")]
        [BsonElement("coordinates-to")]
        [DocumentProperty(PropertyName = "coordinates-to")]
        public double[] CoordinatesTo { get; set; }
    }
}
