﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;

namespace SchematicsModel.Net
{
    public class Edge : GraphElement
    {
        [JsonProperty("from")]
        [BsonElement("from")]
        [DocumentProperty(PropertyName = "from", Identifier = IdentifierType.EdgeFrom)]
        public string From { get; set; }





        [JsonProperty("to")]
        [BsonElement("to")]
        [DocumentProperty(PropertyName = "to", Identifier = IdentifierType.EdgeTo)]
        public string To { get; set; }

        
        
        
        
        /// <summary>
        /// Состояние элемента
        /// </summary>
        [JsonProperty("state")]
        [BsonElement("state")]
        [DocumentProperty(PropertyName = "state")]
        public short? State { get; set; }
    }
}
