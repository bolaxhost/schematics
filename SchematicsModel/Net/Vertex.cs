﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  


namespace SchematicsModel.Net
{
    public class Vertex : GraphElement
    {
        /// <summary>
        /// Projection x,y
        /// </summary>
        [JsonProperty("position")]
        [BsonElement("position")]
        [DocumentProperty(PropertyName = "position")]
        public double[] Position { get; set; }
    }
}
