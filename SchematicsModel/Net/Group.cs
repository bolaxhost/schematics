﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  



namespace SchematicsModel.Net
{
    public class Group : SchematicsElement
    {
        /// <summary>
        /// Extent x1,y1,x2,y2
        /// </summary>
        [JsonProperty("extent")]
        [BsonElement("extent")]
        [DocumentProperty(PropertyName = "extent")]
        public double[] Extent { get; set; }
    }
}
