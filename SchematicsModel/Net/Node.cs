﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  



namespace SchematicsModel.Net
{
    public class Node : SchematicsElement
    {
        [JsonProperty("terminals")]
        [BsonElement("terminals")]
        [DocumentProperty(PropertyName = "terminals")]
        public Terminal[] Terminals { get; set; }



        [JsonProperty("rotation")]
        [BsonElement("rotation")]
        [DocumentProperty(PropertyName = "rotation")]
        public double Rotation { get; set; }


        /// <summary>
        /// Geo lon, lat
        /// </summary>
        [JsonProperty("location")]
        [BsonElement("location")]
        [DocumentProperty(PropertyName = "location")]
        public double[] Location { get; set; }

        
        /// <summary>
        /// Projection x,y
        /// </summary>
        [JsonProperty("position")]
        [BsonElement("position")]
        [DocumentProperty(PropertyName = "position")]
        public double[] Position { get; set; }

        
        
        
        
        
        
        
        /// <summary>
        /// Extent x1,y1,x2,y2
        /// </summary>
        [JsonProperty("extent")]
        [BsonElement("extent")]
        [DocumentProperty(PropertyName = "extent")]
        public double[] Extent { get; set; }




        /// <summary>
        /// Тип формируемой связности: 0 - последовательность, 1 - звезда
        /// </summary>
        [JsonProperty("connectivity-type")]
        [BsonElement("connectivity-type")]
        [DocumentProperty(PropertyName = "connectivity-type")]
        public short? ConnectivityType { get; set; }




        /// <summary>
        /// Описание связности
        /// </summary>
        [JsonProperty("connectivity")]
        [BsonElement("connectivity")]
        [DocumentProperty(PropertyName = "connectivity")]
        public short[][] Connectivity { get; set; }
    }
}
