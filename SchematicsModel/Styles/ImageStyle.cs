﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

namespace SchematicsModel.Styles
{
    public class ImageStyle : BaseStyle
    {
        [JsonProperty("radius")]
        [BsonElement("radius")]
        [DocumentProperty(PropertyName = "radius")]
        public int Radius { get; set; }





        [JsonProperty("stroke")]
        [BsonElement("stroke")]
        [DocumentProperty(PropertyName = "stroke")]
        public StrokeStyle StrokeStyle { get; set; }

        
        
        
        
        [JsonProperty("fill")]
        [BsonElement("fill")]
        [DocumentProperty(PropertyName = "fill")]
        public FillStyle FillStyle { get; set; }
    }
}
