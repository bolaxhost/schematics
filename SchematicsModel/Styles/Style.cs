﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  


namespace SchematicsModel.Styles
{
    public class Style : SchematicsElement  
    {
        [JsonProperty("useStyleGeometry")]
        [BsonIgnore()]
        [DocumentProperty(IgnoreProperty = true)]
        public bool UseStyleGeometry { get; set; }




        [JsonProperty("for")]
        [BsonElement("for")]
        [DocumentProperty(PropertyName = "for")]
        public uint For { get; set; }





        [JsonProperty("binding-point")]
        [BsonElement("binding-point")]
        [DocumentProperty(PropertyName = "binding-point")]
        public double[] BindingPoint { get; set; }






        [JsonProperty("terminal-points")]
        [BsonElement("terminal-points")]
        [DocumentProperty(PropertyName = "terminal-points")]
        public double[][] TerminalPoints { get; set; }

        




        [JsonProperty("fill")]
        [BsonElement("fill")]
        [DocumentProperty(PropertyName = "fill")]
        public FillStyle FillStyle { get; set; }






        [JsonProperty("stroke")]
        [BsonElement("stroke")]
        [DocumentProperty(PropertyName = "stroke")]
        public StrokeStyle StrokeStyle { get; set; }







        [JsonProperty("image")]
        [BsonElement("image")]
        [DocumentProperty(PropertyName = "image")]
        public ImageStyle ImageStyle { get; set; }







        [JsonProperty("text")]
        [BsonElement("text")]
        [DocumentProperty(PropertyName = "text")]
        public TextStyle TextStyle { get; set; }




        
        
        [JsonProperty("zIndex")]
        [BsonElement("zIndex")]
        [DocumentProperty(PropertyName = "zIndex")]
        public int? ZIndex { get; set; }

        
        
        
        /// <summary>
        /// Тип формируемой связности: 0 - последовательность, 1 - звезда
        /// </summary>
        [JsonProperty("connectivity-type")]
        [BsonElement("connectivity-type")]
        [DocumentProperty(PropertyName = "connectivity-type")]
        public short? ConnectivityType { get; set; }




        /// <summary>
        /// Описание связности
        /// </summary>
        [JsonProperty("connectivity")]
        [BsonElement("connectivity")]
        [DocumentProperty(PropertyName = "connectivity")]
        public short[][] Connectivity { get; set; }
    }
}
