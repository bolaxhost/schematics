﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

namespace SchematicsModel.Styles
{
    public class BaseStyle
    {
        [JsonProperty("color")]
        [BsonElement("color")]
        [DocumentProperty(PropertyName = "color")]
        public string Color { get; set; }
    }
}
