﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

namespace SchematicsModel.Styles
{
    public class FillStyle : BaseStyle
    {
        [JsonProperty("fill")]
        [BsonElement("fill")]
        [DocumentProperty(PropertyName = "fill")]
        public bool Fill { get; set; }
    }
}
