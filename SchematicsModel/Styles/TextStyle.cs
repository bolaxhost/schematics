﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

namespace SchematicsModel.Styles
{
    public class TextStyle : BaseStyle
    {
        [JsonProperty("font")]
        [BsonElement("font")]
        [DocumentProperty(PropertyName = "font")]
        public string FontName { get; set; }




        [JsonProperty("offsetX")]
        [BsonElement("offsetX")]
        [DocumentProperty(PropertyName = "offsetX")]
        public int OffsetX { get; set; }




        [JsonProperty("offsetY")]
        [BsonElement("offsetY")]
        [DocumentProperty(PropertyName = "offsetY")]
        public int OffsetY { get; set; }




        [JsonProperty("scale")]
        [BsonElement("scale")]
        [DocumentProperty(PropertyName = "scale")]
        public int Scale { get; set; }



        [JsonProperty("size")]
        [BsonElement("size")]
        [DocumentProperty(PropertyName = "size")]
        public int Size { get; set; }



        [JsonProperty("rotateWithView")]
        [BsonElement("rotateWithView")]
        [DocumentProperty(PropertyName = "rotateWithView")]
        public bool RotateWithView { get; set; }




        [JsonProperty("rotation")]
        [BsonElement("rotation")]
        [DocumentProperty(PropertyName = "rotation")]
        public double Rotation { get; set; }



        [JsonProperty("text")]
        [BsonElement("text")]
        [DocumentProperty(PropertyName = "text")]
        public string Text { get; set; }



        [JsonProperty("textAlign")]
        [BsonElement("textAlign")]
        [DocumentProperty(PropertyName = "textAlign")]
        public string TextAlign { get; set; }



        [JsonProperty("textBaseline")]
        [BsonElement("textBaseline")]
        [DocumentProperty(PropertyName = "textBaseline")]
        public string TextBaseline { get; set; }




        [JsonProperty("fill")]
        [BsonElement("fill")]
        [DocumentProperty(PropertyName = "fill")]
        public FillStyle FillStyle { get; set; }





        [JsonProperty("stroke")]
        [BsonElement("stroke")]
        [DocumentProperty(PropertyName = "stroke")]
        public StrokeStyle StrokeStyle { get; set; }
    }
}
