﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

namespace SchematicsModel.Styles
{
    public class StrokeStyle : BaseStyle
    {
        [JsonProperty("lineCap")]
        [BsonElement("lineCap")]
        [DocumentProperty(PropertyName = "lineCap")]
        public string LineCap { get; set; }



        [JsonProperty("lineJoin")]
        [BsonElement("lineJoin")]
        [DocumentProperty(PropertyName = "lineJoin")]
        public string LineJoin { get; set; }



        [JsonProperty("lineDash")]
        [BsonElement("lineDash")]
        [DocumentProperty(PropertyName = "lineDash")]
        public int[] LineDash { get; set; }




        [JsonProperty("lineDashOffset")]
        [BsonElement("lineDashOffset")]
        [DocumentProperty(PropertyName = "lineDashOffset")]
        public int LineDashOffset { get; set; }




        [JsonProperty("miterLimit")]
        [BsonElement("miterLimit")]
        [DocumentProperty(PropertyName = "miterLimit")]
        public int MiterLimit { get; set; }




        [JsonProperty("width")]
        [BsonElement("width")]
        [DocumentProperty(PropertyName = "width")]
        public int Width { get; set; }
    }
}
