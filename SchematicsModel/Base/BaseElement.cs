﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

using SchematicsModel; 

namespace SchematicsModel
{
    public abstract class BaseElement
    {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        [JsonProperty("id")]
        [BsonId] [BsonElement("id")]
        [DocumentProperty(PropertyName = "id", Identifier = IdentifierType.Key)]
        public string Id { get; set; }



        
        /// <summary>
        /// Наименование
        /// </summary>
        [JsonProperty("name")]
        [BsonElement("name")]
        [DocumentProperty(PropertyName = "name")]
        public string Name { get; set; }







        /// <summary>
        /// Клонирование (только сериализуемые свойства)
        /// </summary>
        /// <returns></returns>
        public BaseElement Clone(bool forceCreate = false)
        {
            if (_cachedValue == null || forceCreate)
                _cachedValue = (BaseElement)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(this), this.GetType());

            return _cachedValue; 
        }

        private BaseElement _cachedValue = null;
    }
}
