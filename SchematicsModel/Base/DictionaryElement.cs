﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

using SchematicsModel; 

namespace SchematicsModel
{
    public abstract class DictionaryElement : BaseElement
    {
        /// <summary>
        /// Описание
        /// </summary>
        [JsonProperty("description")]
        [BsonElement("description")]
        [DocumentProperty(PropertyName = "description")]
        public string Description { get; set; }
    }
}
