﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

using SchematicsModel; 

namespace SchematicsModel
{
    public abstract class GraphElement : BaseElement
    {
        /// <summary>
        /// Тип элемента-родителя {"node"|"link"}
        /// </summary>
        [JsonProperty("parent-type")]
        [BsonElement("parent-type")]
        [DocumentProperty(PropertyName = "parent-type")]
        public string ParentType { get; set; }
    
        
        
        
        /// <summary>
        /// Id элемента-родителя
        /// </summary>
        [JsonProperty("parent-id")]
        [BsonElement("parent-id")]
        [DocumentProperty(PropertyName = "parent-id")]
        public string ParentId { get; set; }
    }
}
