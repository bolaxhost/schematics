﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  

using SchematicsModel; 

namespace SchematicsModel
{
    public abstract class SchematicsElement : BaseElement
    {
        /// <summary>
        /// Тип элемента
        /// </summary>
        [JsonProperty("type")]
        [BsonElement("type")]
        [DocumentProperty(PropertyName = "type")]
        public string Type { get; set; }






        /// <summary>
        /// Состояние элемента
        /// </summary>
        [JsonProperty("state")]
        [BsonElement("state")]
        [DocumentProperty(PropertyName = "state")]
        public string State { get; set; }





        /// <summary>
        /// Примечание
        /// </summary>
        [JsonProperty("comments")]
        [BsonElement("comments")]
        [DocumentProperty(PropertyName = "comments")]
        public string Comments { get; set; }







        /// <summary>
        /// Геометрия
        /// </summary>
        [JsonProperty("geometry")]
        [BsonElement("geometry")]
        [DocumentProperty(PropertyName = "geometry")]
        public Spatial.Geometry[] Geometry { get; set; }

        



        /// <summary>
        /// Подписи
        /// </summary>
        [JsonProperty("labels")]
        [BsonElement("labels")]
        [DocumentProperty(PropertyName = "labels")]
        public double[][] Labels { get; set; }

        
        

        
        /// <summary>
        /// Источник подписей
        /// </summary>
        [JsonProperty("label-source")]
        [BsonElement("label-source")]
        [DocumentProperty(PropertyName = "label-source")]
        public int? LabelSource { get; set; }
    }
}
