﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchematicsModel
{
    public abstract class KeyGeneratorValue<T>
        where T : struct
    {
        public T CurrentValue { get; set; }
    }

    public class LongKeyGeneratorValue : KeyGeneratorValue<long>
    {

    }
}
