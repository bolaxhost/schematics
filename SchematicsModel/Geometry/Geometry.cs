﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  


namespace SchematicsModel.Spatial
{
    public class Geometry
    {
        [JsonProperty("type")]
        [BsonElement("type")]
        [DocumentProperty(PropertyName = "type")]
        public string GeometryType { get; set; }

        
        
        
        [JsonProperty("coordinates")]
        [BsonElement("coordinates")]
        [DocumentProperty(PropertyName = "coordinates")]
        public double[] Coordinates { get; set; }





        [JsonProperty("coordinates-string")]
        [BsonElement("coordinates-string")]
        [DocumentProperty(PropertyName = "coordinates-string")]
        public double[][] CoordinatesString { get; set; }





        [JsonProperty("coordinates-strings")]
        [BsonElement("coordinates-strings")]
        [DocumentProperty(PropertyName = "coordinates-strings")]
        public double[][][] CoordinatesStrings { get; set; }






        [JsonProperty("coordinates-strings-array")]
        [BsonElement("coordinates-strings-array")]
        [DocumentProperty(PropertyName = "coordinates-strings-array")]
        public double[][][][] CoordinatesStringsArray { get; set; }




        public static void Rotate(Geometry[] geometry, double[] center, double angle)
        {
            if (geometry == null || center == null)
                return;

            foreach (var g in geometry)
                g.Rotate(center, angle);
        }

        public void Rotate(double[] center, double angle)
        {
            if (angle == 0)
                return;

            if (this.Coordinates != null && this.Coordinates.Any())
                Rotate(center, angle, this.Coordinates);

            if (this.CoordinatesString != null && this.CoordinatesString.Any())
                Rotate(center, angle, this.CoordinatesString);

            if (this.CoordinatesStrings != null && this.CoordinatesStrings.Any())
                Rotate(center, angle, this.CoordinatesStrings);

            if (this.CoordinatesStringsArray != null && this.CoordinatesStringsArray.Any())
                Rotate(center, angle, this.CoordinatesStringsArray);
        }

        public static void Rotate(double[] center, double angle, double[] coordinates)
        {
            if (angle == 0)
                return;

            double dx = coordinates[0] - center[0];
            double dy = coordinates[1] - center[1];

            double angleR = angle / 180 * Math.PI;

            coordinates[0] = center[0] + dx * Math.Cos(angleR) - dy* Math.Sin(angleR);
            coordinates[1] = center[1] + dx * Math.Sin(angleR) + dy * Math.Cos(angleR);
        }

        private void Rotate(double[] center, double angle, double[][] coordinatesString)
        {
            for (var i = 0; i < coordinatesString.Length; i++)
                Rotate(center, angle, coordinatesString[i]);
        }

        private void Rotate(double[] center, double angle, double[][][] coordinatesStrings)
        {
            for (var i = 0; i < coordinatesStrings.Length; i++)
                Rotate(center, angle, coordinatesStrings[i]);
        }

        private void Rotate(double[] center, double angle, double[][][][] coordinatesStringsArray)
        {
            for (var i = 0; i < coordinatesStringsArray.Length; i++)
                Rotate(center, angle, coordinatesStringsArray[i]);;
        }




        public static void Move(Geometry[] geometry, double[] from, double[] to)
        {
            if (geometry == null || from == null || to == null)
                return;

            var dx = to[0] - from[0];
            var dy = to[1] - from[1];

            if (dx == 0 && dy == 0)
                return;

            foreach (var g in geometry)
                g.Move(dx, dy);
        }

        public static void Move(Geometry[] geometry, double dx, double dy)
        {
            if (geometry == null)
                return;

            if (dx == 0 && dy == 0)
                return;

            foreach (var g in geometry)
                g.Move(dx, dy); 
        }

        public void Move(double[] from, double[] to)
        {
            Move(to[0] - from[0], to[1] - from[1]);
        }

        public void Move(double dx, double dy)
        {
            if (this.Coordinates != null && this.Coordinates.Any())
                Move(dx, dy, this.Coordinates);

            if (this.CoordinatesString != null && this.CoordinatesString.Any())
                Move(dx, dy, this.CoordinatesString);

            if (this.CoordinatesStrings != null && this.CoordinatesStrings.Any())
                Move(dx, dy, this.CoordinatesStrings);

            if (this.CoordinatesStringsArray != null && this.CoordinatesStringsArray.Any())
                Move(dx, dy, this.CoordinatesStringsArray);
        }

        public static void Move(double dx, double dy, double[] coordinates)
        {
            coordinates[0] = coordinates[0] + dx;
            coordinates[1] = coordinates[1] + dy;
        }

        private void Move(double dx, double dy, double[][] coordinatesString)
        {
            for (var i = 0; i < coordinatesString.Length; i++)
                Move(dx, dy, coordinatesString[i]);
        }

        private void Move(double dx, double dy, double[][][] coordinatesStrings)
        {
            for (var i = 0; i < coordinatesStrings.Length; i++)
                Move(dx, dy, coordinatesStrings[i]);
        }

        private void Move(double dx, double dy, double[][][][] coordinatesStringsArray)
        {
            for (var i = 0; i < coordinatesStringsArray.Length; i++)
                Move(dx, dy, coordinatesStringsArray[i]);
        }
    }
}
