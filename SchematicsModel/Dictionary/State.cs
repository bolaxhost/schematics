﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  



namespace SchematicsModel.Dictionary
{
    public class State : DictionaryElement
    {
        /// <summary>
        /// Состояние связи: 0 - закрыто, 1 - открыто
        /// </summary>
        [JsonProperty("graph-state-id")]
        [BsonElement("graph-state-id")]
        [DocumentProperty(PropertyName = "graph-state-id")]
        public short? GraphStateId { get; set; }
    }
}
