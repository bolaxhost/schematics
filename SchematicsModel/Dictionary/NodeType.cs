﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Newtonsoft.Json;
using ArangoDB.Client;
using MongoDB.Bson.Serialization.Attributes;  



namespace SchematicsModel.Dictionary
{
    public class NodeType : DictionaryElement
    {
        /// <summary>
        /// Тип представления: 0 - символ, 1 - линейная геометрия
        /// </summary>
        [JsonProperty("view-type")]
        [BsonElement("view-type")]
        [DocumentProperty(PropertyName = "view-type")]
        public short? ViewType { get; set; }
    }
}
