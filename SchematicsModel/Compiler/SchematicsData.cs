﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Net;
using SchematicsModel.Styles;  
using SchematicsModel.Spatial;

using Newtonsoft.Json;

namespace SchematicsModel.Compiler
{
    public class SchematicsData
    {
        [JsonProperty("active")]
        public SchematicsPrimitive[] Active { get; set; }
        
        [JsonProperty("snap")]
        public SchematicsPrimitive[] Snap { get; set; }

        [JsonProperty("labels")]
        public SchematicsPrimitive[] Labels { get; set; }

        [JsonProperty("nodesCount")]
        public int NodesCount { get; set; }

        [JsonProperty("linksCount")]
        public int LinksCount { get; set; }

        [JsonProperty("groupsCount")]
        public int GroupsCount { get; set; }

        [JsonProperty("styles")]
        public SchematicsDictionaryItem<Style>[] Styles { get; set; }

        [JsonProperty("full")]
        public bool isFull { get; set; }
    }

    public struct SchematicsDictionaryItem<T>
        where T: class
    {
        [JsonProperty("n")]
        public string name;

        [JsonProperty("v")]
        public T value;
    }

    public class SchematicsPrimitive
    {
        public SchematicsPrimitive()
        {

        }

        public SchematicsPrimitive(SchematicsElement element)
        {
            ElementType = element.GetType().Name.ToLower();
            ElementId = element.Id;
        }

        public SchematicsPrimitive(Terminal terminal, SchematicsPrimitive nodePrimitive)
        {
            ElementType = terminal.GetType().Name.ToLower();
            ElementId = terminal.Id;

            Parent = nodePrimitive;
        }

        [JsonProperty("t")]
        public string ElementType { get; set; }
        

        [JsonProperty("id")]
        public string ElementId { get; set; }
                
        
        [JsonProperty("g")]
        public Spatial.Geometry[] Geometry { get; set; }
        

        [JsonProperty("r")]
        public double Rotation { get; set; }
        
        
        [JsonProperty("s")]
        public string Style { get; set; }
                

        [JsonProperty("p")]
        public SchematicsPrimitive Parent { get; set; }


        [JsonProperty("txt")]
        public string text = null;

    }
}
