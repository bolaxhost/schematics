﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration; 

using SchematicsModel.Net;
using SchematicsModel.Styles;
using SchematicsModel.Storage;   

namespace SchematicsModel.Compiler
{
    public static class SchematicsCompiler
    {
        public static SchematicsData Compile(INetStorage net, IStylesStorage styles, double[] extent, int? maxItemsCount = null)
        {
            return Compile(net, styles, extent, net.NodesWithin(extent), maxItemsCount);
        }

        public static SchematicsData Compile(INetStorage net, IStylesStorage styles, double lon, double lat, double xc, double yc, double radius, int? maxItemsCount = null)
        {
            return Compile(net, styles, new double[4] { xc - radius, yc - radius, xc + radius, yc + radius }, net.NodesWithin(lon, lat, radius), maxItemsCount);
        }

        public static SchematicsData Compile(IStylesStorage styles, string typeId, string stateId, double[] coordinates)
        {
            var node = new Node() { Name = "Узел", Type = typeId, State = stateId, Position = coordinates };

            List<SchematicsPrimitive> _active = new List<SchematicsPrimitive>();
            List<SchematicsPrimitive> _snap = new List<SchematicsPrimitive>();
            List<SchematicsPrimitive> _labels = new List<SchematicsPrimitive>();

            var nstyles = styles.FindStylesFor(node);
            foreach (var s in nstyles)
            {
                if (s.Geometry != null && s.BindingPoint != null)
                {
                    Spatial.Geometry.Move(s.Geometry, -s.BindingPoint[0], -s.BindingPoint[1]);
                    s.UseStyleGeometry = true;
                }

                SchematicsPrimitive nodeActive = new SchematicsPrimitive(node);
                nodeActive.Geometry = new Spatial.Geometry[] { new Spatial.Geometry() { GeometryType = "Point", Coordinates = node.Position } };
                nodeActive.Style = s.Id;

                _active.Add(nodeActive);

                if (s.TerminalPoints != null && s.TerminalPoints.Any())
                {
                    foreach (var tp in s.TerminalPoints)
                    {
                        SchematicsPrimitive nodeSnap = new SchematicsPrimitive(new Terminal(), nodeActive);
                        nodeSnap.Geometry = new Spatial.Geometry[] { new Spatial.Geometry() { GeometryType = "Point", Coordinates = tp } };

                        _snap.Add(nodeSnap);
                    }
                }
            }
            
            //label
            if (nstyles.Length > 0)
            {
                AppendLabels(_labels, node, nstyles[0]);
            }


            return new SchematicsData()
            {
                NodesCount = 1,
                LinksCount = 0,
                GroupsCount = 0,

                Active = _active.ToArray(),
                Snap = _snap.ToArray(),
                Labels = _labels.ToArray(),
                Styles = nstyles.Select(i => new SchematicsDictionaryItem<Style> { name = i.Id, value = i }).ToArray(),

                isFull = true,
            };
        }


        private static SchematicsData Compile(INetStorage net, IStylesStorage styles, double[] extent, IQueryable<Node> nodesScope, int? maxItemsCount = null)
        {
            var linksScope = net.LinksWithin(extent);
            var groupsScope = net.GroupsWithin(extent);

            if (maxItemsCount.HasValue)
            {
                nodesScope = nodesScope.Take(maxItemsCount.Value);
                linksScope = linksScope.Take(maxItemsCount.Value);
                groupsScope = groupsScope.Take(maxItemsCount.Value);
            }

            Node[] nodes = null;
            Link[] links = null;
            Group[] groups = null;

            Task.WaitAll(new Task[] {
                Task.Run(() => nodes = nodesScope.ToArray()),
                Task.Run(() => links = linksScope.ToArray()),
                Task.Run(() => groups = groupsScope.ToArray()),
            });

            Dictionary<string, Style> stylesd = new Dictionary<string, Style>();

            //Добавление специальных стилей
            var lineConnectionStyle = styles.Styles.DataSet.FirstOrDefault(i => i.For == 1001);
            if (lineConnectionStyle != null)
            {
                stylesd.Add(lineConnectionStyle.Id, lineConnectionStyle);
            }

            List<SchematicsPrimitive> _active = new List<SchematicsPrimitive>();
            List<SchematicsPrimitive> _snap = new List<SchematicsPrimitive>();
            List<SchematicsPrimitive> _labels = new List<SchematicsPrimitive>();

            //Terminal positions
            Dictionary<string, SchematicsPrimitive> _terminalPos = new Dictionary<string, SchematicsPrimitive>();

            //Node Terminals
            var tnodes = nodes.Where(n => n.Terminals == null || n.Terminals.Length == 0).ToDictionary(i => i.Id, i => i); 

            //nodes
            foreach (var node in nodes)
            {
                SchematicsPrimitive mainNodeActive = null;

                var nstyles = styles.FindStylesFor(node);
                foreach (var s in nstyles)
                {
                    if (!stylesd.ContainsKey(s.Id))
                    {
                        if (s.Geometry != null && s.BindingPoint != null)
                        {
                            Spatial.Geometry.Move(s.Geometry, -s.BindingPoint[0], -s.BindingPoint[1]);
                            s.UseStyleGeometry = true;
                        }
  
                        stylesd.Add(s.Id, s);
                    }

                    SchematicsPrimitive nodeActive = new SchematicsPrimitive(node);

                    if (node.Position != null)
                    {
                        nodeActive.Geometry = new Spatial.Geometry[] { new Spatial.Geometry() { GeometryType = "Point", Coordinates = new double[] { node.Position[0], node.Position[1] } } };
                        nodeActive.Rotation = node.Rotation;
                    } else
                    {
                        nodeActive.Geometry = node.Geometry;

                        _snap.Add(new SchematicsPrimitive(node) {
                            Geometry = node.Geometry
                        });
                    }

                    nodeActive.Style = s.Id;

                    _active.Add(nodeActive);

                    if (mainNodeActive == null)
                        mainNodeActive = nodeActive;
                }

                if (node.Terminals != null && node.Terminals.Any())
                {
                    foreach (var t in node.Terminals)
                    {
                        SchematicsPrimitive nodeSnap = new SchematicsPrimitive(t, mainNodeActive);
                        nodeSnap.Geometry = new Spatial.Geometry[] { new Spatial.Geometry() { GeometryType = "Point", Coordinates = new double[] { t.Coordinates[0], t.Coordinates[1] } } };

                        if (!_terminalPos.ContainsKey(t.Id))
                        {
                            _terminalPos.Add(t.Id, nodeSnap);

                            if (lineConnectionStyle != null && tnodes.ContainsKey(t.Id))
                            {
                                nodeSnap.Style = lineConnectionStyle.Id;
                            }
                        } else
                        {
                            if (lineConnectionStyle != null)
                            {
                                _terminalPos[t.Id].Style = nodeSnap.Style = lineConnectionStyle.Id;
                            }
                        }

                        _snap.Add(nodeSnap);
                    }
                }

                //label
                if (nstyles.Length > 0)
                {
                    AppendLabels(_labels, node, nstyles[0]);
                }
            }

            //links
            foreach (var link in links)
            {
                SchematicsPrimitive linkActive = new SchematicsPrimitive(link);
                linkActive.Geometry = link.Geometry;

                //update links terminal points
                if (linkActive.Geometry != null && linkActive.Geometry.Length > 0)
                {
                    var cs = linkActive.Geometry[0].CoordinatesString;
                    if (cs != null && cs.Length > 1)
                    {
                        if (!string.IsNullOrWhiteSpace(link.TerminalFrom) && _terminalPos.ContainsKey(link.TerminalFrom))
                        {
                            var t0 = _terminalPos[link.TerminalFrom];
                            var p0 = t0.Geometry[0].Coordinates;
                            cs[0][0] = p0[0];
                            cs[0][1] = p0[1];
                            
                            if (lineConnectionStyle != null)
                            {
                                t0.Style = lineConnectionStyle.Id;  
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(link.TerminalTo) && _terminalPos.ContainsKey(link.TerminalTo))
                        {
                            var t1 = _terminalPos[link.TerminalTo];
                            var p1 = t1.Geometry[0].Coordinates;
                            cs[cs.Length - 1][0] = p1[0];
                            cs[cs.Length - 1][1] = p1[1];

                            if (lineConnectionStyle != null)
                            {
                                t1.Style = lineConnectionStyle.Id;
                            }
                        }
                    }
                }

                var style = styles.FindStyleFor(link);
                if (style != null)
                {
                    if (!stylesd.ContainsKey(style.Id))
                        stylesd.Add(style.Id, style);

                    linkActive.Style = style.Id;
                }

                _active.Add(linkActive);
            }

            //groups
            foreach (var group in groups)
            {
                SchematicsPrimitive groupActive = new SchematicsPrimitive(group)
                {
                    Geometry = new Spatial.Geometry[] 
                    {
                        new Spatial.Geometry() 
                        { 
                            GeometryType = "Polygon", 
                            CoordinatesStrings = new double[][][] 
                            {
                                new double [][] 
                                {
                                   new double[] {group.Extent[0], group.Extent[1]},
                                   new double[] {group.Extent[0], group.Extent[3]},
                                   new double[] {group.Extent[2], group.Extent[3]},
                                   new double[] {group.Extent[2], group.Extent[1]},
                                   new double[] {group.Extent[0], group.Extent[1]},
                                }
                            }
                        }
                    }
                };

                _active.Add(groupActive);
            }

            return new SchematicsData()
            {
                NodesCount = nodes.Length,
                LinksCount = links.Length,
                GroupsCount = groups.Length,

                Active = _active.ToArray(),
                Snap = _snap.ToArray(),
                Labels = _labels.ToArray(),
                Styles = stylesd.Select(i => new SchematicsDictionaryItem<Style> { name = i.Key, value = i.Value }).ToArray(),

                isFull = true,
            };
        }


        private static void AppendLabels(List<SchematicsPrimitive> layer, Node node, Style style)
        {
            if (style.Labels == null || style.LabelSource == null || style.LabelSource == 0)
                return;

            if (style.BindingPoint == null)
                return;

            foreach (var l in style.Labels)
            {
                var g = new Spatial.Geometry() { GeometryType = "Point", Coordinates = new double[] { l[0], l[1] }};

                g.Move(style.BindingPoint, node.Position);
                g.Rotate(node.Position, node.Rotation);

                layer.Add(new SchematicsPrimitive(node) 
                {
                    Geometry = new Spatial.Geometry[] {g},
                    Style = style.Id,
                    text = style.LabelSource == 1 ? node.Name : node.Comments,
                    Rotation = node.Rotation * Math.PI / 180, 
                });

            }
        }
    }
}
