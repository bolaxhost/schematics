﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Styles;

namespace SchematicsModel.Storage
{
    public abstract class BaseStylesStorage : IStylesStorage
    {
        private string _name;
        public string Name
        {
            get { return _name; }
        }


        public BaseStylesStorage(string name)
        {
            _name = name;
        }




        public abstract IStorageCollection<Styles.Style> Styles { get; }





        public void Commit()
        {
            this.Styles.Commit();  
        }

        public void Rollback()
        {
            this.Styles.Rollback(); 
        }




        private List<Style> _cache = null;
        public Styles.Style FindStyleFor(SchematicsElement element)
        {
            if (_cache == null)
                _cache = this.Styles.DataSet.ToList();

            var allStyles = _cache;
            var typeStyles = allStyles.Where(s => s.Type == element.Type);
            var stateStyles = typeStyles.Where(s => s.State == element.State);

            if (stateStyles.Any())
                return stateStyles.FirstOrDefault();

            if (typeStyles.Any())
                return typeStyles.FirstOrDefault();

            var defaultStyles = allStyles.Where(s => (s.Type == null || s.Type.Length == 0) && s.For == ((element is SchematicsModel.Net.Node) ? 1 : 2));

            if (defaultStyles.Any())
                return defaultStyles.FirstOrDefault();

            return allStyles.FirstOrDefault();
        }

        public Styles.Style[] FindStylesFor(SchematicsElement element)
        {
            if (_cache == null)
                _cache = this.Styles.DataSet.ToList();

            var allStyles = _cache;
            var typeStyles = allStyles.Where(s => s.Type == element.Type);
            var stateStyles = typeStyles.Where(s => s.State == element.State);

            if (stateStyles.Any())
                return stateStyles.Union(typeStyles.Where(t => string.IsNullOrWhiteSpace(t.State))).ToArray();

            if (typeStyles.Any())
                return typeStyles.ToArray();

            return new List<Styles.Style>() 
            {
                allStyles.FirstOrDefault()
            }.ToArray();
        }
    }
}
