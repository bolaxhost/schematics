﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;
using SchematicsModel.DocumentStorageReference;

using Newtonsoft.Json;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentStorageCollection<T> : BaseStorageCollection<T>
        where T : BaseElement, new()
    {
        private DocumentStorageContractClient _storage;
        internal DocumentStorageContractClient Storage
        {
            get
            {
                return _storage;
            }
        }

        private string _path = "";
        internal string Path
        {
            get
            {
                return _path; 
            }
        }




        internal FileDocumentStorageCollection(DocumentStorageContractClient storage, string path, string name)
        {
            _storage = storage;

            if (!path.EndsWith("\\"))
                _path = _path + "\\";

            _path = string.Format("{0}{1}\\", path, name);
        }




        private IQueryable<T> _dataSet = null; 
        public override IQueryable<T> DataSet
        {
            get 
            {
                return _dataSet?? (_dataSet = Storage.GetDocuments(Path).AsQueryable().Select(i => JsonConvert.DeserializeObject<T>(i.Content)));
            }
        }
        public override T Find(string Id)
        {
            if (_dataSet == null)
                return JsonConvert.DeserializeObject<T>(Storage.GetDocument(Path, Guid.Parse(Id)).Content);

            return _dataSet.FirstOrDefault(i => i.Id == Id); 
        }





        private string Serialize(object item)
        {
            return JsonConvert.SerializeObject(item, Formatting.Indented);
        }

        protected override void DoInsert(T item)
        {
            Storage.AddDocument(Path, Guid.Parse(item.Id), Serialize(item));  
        }

        protected override void DoUpdate(T item)
        {
            Storage.UpdateDocument(Path, Guid.Parse(item.Id), Serialize(item));
        }

        protected override void DoDelete(T item)
        {
            Storage.DeleteDocument(Path, Guid.Parse(item.Id));  
        }
    }
}
