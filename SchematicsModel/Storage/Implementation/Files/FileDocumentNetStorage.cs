﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentNetStorage : BaseNetStorage 
    {
        private FileDocumentNodeCollection _nodes;
        private FileDocumentLinkCollection _links;
        private FileDocumentGroupCollection _groups;
        private FileDocumentVertexCollection _vertices;
        private FileDocumentEdgeCollection _edges;

        public FileDocumentNetStorage(FileDocumentSchematicsStorage storage, string name)
            : base(name)
        {
            var path = string.Format("nets\\{0}\\", name);

            _nodes = new FileDocumentNodeCollection(storage.Storage, path);
            _links = new FileDocumentLinkCollection(storage.Storage, path);
            _groups = new FileDocumentGroupCollection(storage.Storage, path);
            _vertices = new FileDocumentVertexCollection(storage.Storage, path);
            _edges = new FileDocumentEdgeCollection(storage.Storage, path);
        }




        




        public override IStorageCollection<Net.Node> Nodes
        {
            get { return _nodes; }
        }

        public override IStorageCollection<Net.Link> Links
        {
            get { return _links; }
        }

        public override IStorageCollection<Net.Group> Groups
        {
            get { return _groups; }
        }

        public override IStorageCollection<Net.Vertex> Vertices
        {
            get { return _vertices; }
        }

        public override IStorageCollection<Net.Edge> Edges
        {
            get { return _edges; }
        }
    }
}
