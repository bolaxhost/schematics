﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;
using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentStateCollection : FileDocumentStorageCollection<State>
    {
        internal FileDocumentStateCollection(DocumentStorageContractClient storage, string path) : base(storage, path, "states") { }
    }
}
