﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentSchematicsStorage : ISchematicsStorage
    {
        private DocumentStorageContractClient _storage;
        internal DocumentStorageContractClient Storage
        {
            get
            {
                return _storage;
            }
        }




        public FileDocumentSchematicsStorage()
        {
            _storage = new DocumentStorageContractClient();
        }








        private Dictionary<string, INetStorage> _netStorages = new Dictionary<string, INetStorage>(); 
        public INetStorage GetNetStorage(string name, IDictionaryStorage dictionary, IStylesStorage styles)
        {
            if (_netStorages.ContainsKey(name))
                return _netStorages[name];

            var newStorage = new FileDocumentNetStorage(this, name) {
                Dictionary = dictionary,
                Styles = styles
            };

            _netStorages.Add(name, newStorage);

            return newStorage;
        }




        private Dictionary<string, IStylesStorage> _stylesStorages = new Dictionary<string, IStylesStorage>();
        public IStylesStorage GetStylesStorage(string name)
        {
            if (_stylesStorages.ContainsKey(name))
                return _stylesStorages[name];

            var newStorage = new FileDocumentStylesStorage(this, name);
            _stylesStorages.Add(name, newStorage);

            return newStorage;
        }




        private Dictionary<string, IDictionaryStorage> _dictionaryStorages = new Dictionary<string, IDictionaryStorage>();
        public IDictionaryStorage GetDictionaryStorage(string name)
        {
            if (_dictionaryStorages.ContainsKey(name))
                return _dictionaryStorages[name];

            var newStorage = new FileDocumentDictionaryStorage(this, name);
            _dictionaryStorages.Add(name, newStorage);

            return newStorage;
        }
    }
}
