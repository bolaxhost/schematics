﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;
using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentVertexCollection : FileDocumentStorageCollection<Vertex>
    {
        internal FileDocumentVertexCollection(DocumentStorageContractClient storage, string path) : base(storage, path, "vertices") { }
    }
}
