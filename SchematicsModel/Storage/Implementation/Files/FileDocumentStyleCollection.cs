﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Styles;
using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentStyleCollection : FileDocumentStorageCollection<Style>
    {
        internal FileDocumentStyleCollection(DocumentStorageContractClient storage, string path) : base(storage, path, "styles") { }
    }
}
