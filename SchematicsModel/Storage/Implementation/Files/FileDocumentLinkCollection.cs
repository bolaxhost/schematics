﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;
using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentLinkCollection : FileDocumentStorageCollection<Link>
    {
        internal FileDocumentLinkCollection(DocumentStorageContractClient storage, string path) : base(storage, path, "links") { }
    }
}
