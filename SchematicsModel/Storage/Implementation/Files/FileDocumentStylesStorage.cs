﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentStylesStorage : BaseStylesStorage 
    {
        private FileDocumentStyleCollection _styles;

        public FileDocumentStylesStorage(FileDocumentSchematicsStorage storage, string name)
            : base(name)
        {
            var path = string.Format("styles\\{0}\\", name);

            _styles = new FileDocumentStyleCollection(storage.Storage, path);
        }




        




        public override IStorageCollection<Styles.Style> Styles
        {
            get { return _styles; }
        }
    }
}
