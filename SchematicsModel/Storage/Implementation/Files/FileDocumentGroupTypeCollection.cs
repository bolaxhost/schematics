﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;
using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentGroupTypeCollection : FileDocumentStorageCollection<GroupType>
    {
        internal FileDocumentGroupTypeCollection(DocumentStorageContractClient storage, string path) : base(storage, path, "group-types") { }
    }
}
