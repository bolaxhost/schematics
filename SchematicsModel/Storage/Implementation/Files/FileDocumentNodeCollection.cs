﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;
using SchematicsModel.DocumentStorageReference;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentNodeCollection : FileDocumentStorageCollection<Node>
    {
        internal FileDocumentNodeCollection(DocumentStorageContractClient storage, string path) : base(storage, path, "nodes") { }
    }
}
