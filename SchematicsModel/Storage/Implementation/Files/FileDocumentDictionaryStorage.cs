﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

namespace SchematicsModel.Storage.FileStorage
{
    public class FileDocumentDictionaryStorage : BaseDictionaryStorage 
    {
        private FileDocumentStateCollection _states;
        private FileDocumentNodeTypeCollection _nodeTypes;
        private FileDocumentLinkTypeCollection _linkTypes;
        private FileDocumentGroupTypeCollection _groupTypes;

        public FileDocumentDictionaryStorage(FileDocumentSchematicsStorage storage, string name)
            : base(name)
        {
            var path = string.Format("dictionary\\{0}\\", name);

            _states = new FileDocumentStateCollection(storage.Storage, path);
            _nodeTypes = new FileDocumentNodeTypeCollection(storage.Storage, path);
            _linkTypes = new FileDocumentLinkTypeCollection(storage.Storage, path);
            _groupTypes = new FileDocumentGroupTypeCollection(storage.Storage, path);
        }









        public override IStorageCollection<State> States
        {
            get { return _states; }
        }

        public override IStorageCollection<NodeType> NodeTypes
        {
            get { return _nodeTypes; }
        }

        public override IStorageCollection<LinkType> LinkTypes
        {
            get { return _linkTypes; }
        }

        public override IStorageCollection<GroupType> GroupTypes
        {
            get { return _groupTypes; }
        }
    }
}
