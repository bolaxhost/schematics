﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

namespace SchematicsModel.Storage
{
    public abstract class BaseStorageCollection<T> : IStorageCollection<T>
        where T : BaseElement, new()

    {
        public Func<string> KeyGenerator { get; set; } = null;
        public virtual string GenerateNewKey()
        {
            if (KeyGenerator == null)
                return Guid.NewGuid().ToString();

            return KeyGenerator();
        }





        private Dictionary<string, T> _newItems = null;
        protected Dictionary<string, T> NewItems
        {
            get
            {
                return _newItems ?? (_newItems = new Dictionary<string, T>());
            }
        }

        private Dictionary<string, T> _updatedItems = null;
        protected Dictionary<string, T> UpdatedItems
        {
            get
            {
                return _updatedItems ?? (_updatedItems = new Dictionary<string, T>());
            }
        }

        private Dictionary<string, T> _deletedItems = null;
        protected Dictionary<string, T> DeletedItems
        {
            get
            {
                return _deletedItems ?? (_deletedItems = new Dictionary<string, T>());
            }
        }






        public abstract IQueryable<T> DataSet { get; }
        public abstract T Find(string Id);

        public virtual void Add(T item)
        {
            if (string.IsNullOrWhiteSpace(item.Id))
                item.Id = this.GenerateNewKey();
            else
            {
                if (NewItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} уже добавлен в список новых элементов!", item.Id));

                if (UpdatedItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} уже добавлен в список обновляемых элементов!", item.Id));

                if (DeletedItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} добавлен в список удаляемых элементов и не может быть добавлен как новый!", item.Id));
            }

            NewItems.Add(item.Id, item);
        }

        public virtual void Update(T item)
        {
            if (string.IsNullOrWhiteSpace(item.Id))
                throw new InvalidOperationException("Операция Update невозможна для элементов без Id!");
            else
            {
                if (UpdatedItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} уже добавлен в список обновляемых элементов!", item.Id));

                if (NewItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} уже добавлен в список новых элементов!", item.Id));

                if (DeletedItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} добавлен в список удаляемых элементов и не может быть обновлен!", item.Id));
            }

            UpdatedItems.Add(item.Id, item);
        }

        public virtual void Delete(T item)
        {
            if (string.IsNullOrWhiteSpace(item.Id))
                throw new InvalidOperationException("Операция Delete невозможна для элементов без Id!");
            else
            {
                if (DeletedItems.ContainsKey(item.Id))
                    throw new InvalidOperationException(string.Format("Элемент {0} уже добавлен в список удаляемых элементов!", item.Id));

                if (NewItems.ContainsKey(item.Id))
                {
                    NewItems.Remove(item.Id);
                    return;
                }

                if (UpdatedItems.ContainsKey(item.Id))
                    UpdatedItems.Remove(item.Id);
            }

            DeletedItems.Add(item.Id, item);
        }

        public virtual void Detach(T item)
        {
            if (string.IsNullOrWhiteSpace(item.Id))
                throw new InvalidOperationException("Для элемента не определен Id!");

            if (NewItems.ContainsKey(item.Id))
            {
                NewItems.Remove(item.Id);
                return;
            }

            if (UpdatedItems.ContainsKey(item.Id))
            {
                UpdatedItems.Remove(item.Id);
                return;
            }

            if (DeletedItems.ContainsKey(item.Id))
            {
                DeletedItems.Remove(item.Id);
                return;
            }
        }

        public virtual void AddOrUpdate(T item)
        {
            if (string.IsNullOrWhiteSpace(item.Id)) //Нет Id
            {
                this.Add(item);
                return;
            }

            if (NewItems.ContainsKey(item.Id) || UpdatedItems.ContainsKey(item.Id)) //Уже добавляется или обновляется
            {
                return;
            }


            if (DeletedItems.ContainsKey(item.Id)) //Если удаляется и добавляется снова, то заменяем на обновление
            {
                DeletedItems.Remove(item.Id);
                this.Update(item);
            } else
            {
                this.Add(item); //Если элемента нет в списках, то добавляем
            }
        }

        public virtual void Commit()
        {
            foreach (var i in DeletedItems.Select(i => i.Value))
                DoDelete(i);

            foreach (var i in UpdatedItems.Select(i => i.Value))
                DoUpdate(i);

            foreach (var i in NewItems.Select(i => i.Value))
                DoInsert(i);

            ClearContext();
        }

        public virtual void Rollback()
        {
            ClearContext();
        }

        protected virtual void ClearContext()
        {
            NewItems.Clear();
            UpdatedItems.Clear();
            DeletedItems.Clear();
        }

        protected abstract void DoInsert(T item);
        protected abstract void DoUpdate(T item);
        protected abstract void DoDelete(T item);


        public IEnumerable<T> ItemsToAdd
        {
            get { return NewItems.Select(i => i.Value); }
        }

        public IEnumerable<T> ItemsToUpdate
        {
            get { return UpdatedItems.Select(i => i.Value); }
        }

        public IEnumerable<T> ItemsToDelete
        {
            get { return DeletedItems.Select(i => i.Value); }
        }
    }
}
