﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

namespace SchematicsModel.Storage
{
    public abstract class BaseNetStorage : INetStorage
    {
        private string _name;
        public string Name
        {
            get { return _name; }
        }

        public IDictionaryStorage Dictionary { get; set; }
        public IStylesStorage Styles { get; set; }

        public BaseNetStorage(string name)
        {
            _name = name;
        }


        public ZeroLengthLinksHandlingMode ZeroLengthLinksHandling { get; set; } = ZeroLengthLinksHandlingMode.ShareTerminals; 



        public abstract IStorageCollection<Node> Nodes { get; }

        public abstract IStorageCollection<Link> Links { get; }

        public abstract IStorageCollection<Group> Groups { get; }

        public abstract IStorageCollection<Vertex> Vertices { get; }

        public abstract IStorageCollection<Edge> Edges { get; }





        #region специальные структуры
        private Dictionary<string, Node> _affectedNodes = new Dictionary<string, Node>();

        private IEnumerable<Node> OriginalInstances(IEnumerable<Node> nodes)
        {
            List<Node> result = new List<Node>();
            foreach (var n in nodes)
            {
                if (_affectedNodes.ContainsKey(n.Id))
                    result.Add(_affectedNodes[n.Id]);
                else if (Nodes.ItemsToUpdate.Any(i => i.Id == n.Id))
                    result.Add(Nodes.ItemsToUpdate.FirstOrDefault(i => i.Id == n.Id));
                else result.Add(n);

            }

            return result;
        }

        private void AppendAffectedNode(Node node)
        {
            if (string.IsNullOrWhiteSpace(node.Id))
                return;

            if (_affectedNodes.ContainsKey(node.Id))
                return;

            if (Nodes.ItemsToUpdate.Any(i => i.Id == node.Id))
                return;

            _affectedNodes.Add(node.Id, node);  
        }

        private void UpdateAffectedNodes()
        {
            while (_affectedNodes.Any())
            {
                var affected = _affectedNodes.Select(i => i.Value).ToList();
                foreach (var node in affected)
                {
                    if (!Nodes.ItemsToUpdate.Any(i => i.Id == node.Id))
                    {
                        CompleteNode(node);
                        Nodes.Update(node); 
                    };

                    _affectedNodes.Remove(node.Id);  
                }
            }
        }

        protected virtual void ClearTemporaryData()
        {
            _affectedNodes.Clear();  
        }
        #endregion







        public void Commit()
        {
            UpdateAffectedNodes();

            //Обработка удаляемых групп
            foreach (var g in this.Groups.ItemsToDelete.ToList())
            {
                //Удаление содержимого группы, если для нее определен тип
                if (!string.IsNullOrWhiteSpace(g.Type))
                {
                    _DeleteGroup(g);
                }
            }

            foreach (var n in this.Nodes.ItemsToDelete.Union(this.Nodes.ItemsToUpdate))
                _DeleteNode(n);

            foreach (var n in this.Nodes.ItemsToUpdate.Union(this.Nodes.ItemsToAdd).Where(n => n.Terminals == null || n.Terminals.Length == 0))
                _InsertNode(n);

            foreach (var n in this.Nodes.ItemsToUpdate.Union(this.Nodes.ItemsToAdd).Where(n => n.Terminals != null && n.Terminals.Length > 0))
                _InsertNode(n);

            foreach (var l in this.Links.ItemsToDelete.Union(this.Links.ItemsToUpdate))
                _DeleteLink(l);

            foreach (var l in this.Links.ItemsToUpdate.Union(this.Links.ItemsToAdd))
                _InsertLink(l);

            ClearTemporaryData();

            this.Nodes.Commit();
            this.Links.Commit();
            this.Groups.Commit();
            this.Nodes.Commit();
            this.Vertices.Commit();
            this.Edges.Commit();
        }

        public void Rollback()
        {
            ClearTemporaryData();

            this.Nodes.Rollback();
            this.Links.Rollback();
            this.Groups.Rollback();
            this.Nodes.Rollback();
            this.Vertices.Rollback();
            this.Edges.Rollback();
        }




        public virtual IQueryable<Node> NodesWithin(double lon, double lat, double radius)
        {
            throw new NotImplementedException();
        }

        public virtual IQueryable<Node> NodesWithin(double[] extent)
        {
            return this.Nodes.DataSet
                .Where(n => (n.Extent != null && !(n.Extent[2] < extent[0] || n.Extent[0] > extent[2] || n.Extent[3] < extent[1] || n.Extent[1] > extent[3]))
                            ||
                            (n.Position != null && (n.Position[0] > extent[0] && n.Position[0] < extent[2] && n.Position[1] > extent[1] && n.Position[1] < extent[3]))
                );
        }

        public virtual IQueryable<Link> LinksWithin(double[] extent)
        {
            return this.Links.DataSet
                .Where(l => (l.CoordinatesFrom[0] > extent[0] && l.CoordinatesFrom[0] < extent[2] && l.CoordinatesFrom[1] > extent[1] && l.CoordinatesFrom[1] < extent[3])
                || (l.CoordinatesTo[0] > extent[0] && l.CoordinatesTo[0] < extent[2] && l.CoordinatesTo[1] > extent[1] && l.CoordinatesTo[1] < extent[3])
                );
        }

        public virtual IQueryable<Group> GroupsWithin(double[] extent)
        {
            return this.Groups.DataSet
                .Where(g => g.Extent != null &&
                     !(g.Extent[2] < extent[0] || g.Extent[0] > extent[2] || g.Extent[3] < extent[1] || g.Extent[1] > extent[3])
                );
        }

        public virtual NetScope Copy(double[] extent)
        {
            var nodes = NodesWithin(extent);
            var nodeIds = nodes.ToDictionary(i => "Node/" + i.Id, i => i.Id);

            var links = LinksWithin(extent).ToList();

            var internalLinks = links.Where(l => nodeIds.ContainsKey(l.From) && nodeIds.ContainsKey(l.To));
            var externalLinks = links.Where(l => !internalLinks.Contains(l));

            return new NetScope()
            {
                Nodes = nodes.ToArray(),
                Groups = GroupsWithin(extent).ToArray(),
                ExternalLinks = externalLinks.ToArray(),
                InternalLinks = internalLinks.ToArray(),
                Extent = extent,
            };
        }

        public virtual NetScope Copy(string groupId)
        {
            var group = this.Groups.Find(groupId);
            var result = Copy(group.Extent);

            if (string.IsNullOrWhiteSpace(group.Type)) 
            {
               result.Groups = result.Groups.Where(g => g.Id != groupId).ToArray(); 
            }

            return result;
        }




        public virtual void Paste(NetScope scope)
        {
            Dictionary<string, string> ids = new Dictionary<string, string>();
            Dictionary<string, string> idts = new Dictionary<string, string>();

            foreach (var n in scope.Nodes.Where(i => i.Terminals == null || i.Terminals.Length == 0)
                .Union(scope.Nodes.Where(i => i.Terminals != null && i.Terminals.Length > 0)).Select(i => (Node)i.Clone()))
            {
                ids.Add("Node/" + n.Id, n.Id = this.Nodes.GenerateNewKey());

                if (n.Terminals != null)
                {
                    foreach (var tc in n.Terminals)
                    {
                        if (!idts.ContainsKey(tc.Id))
                            if (!ids.ContainsKey("Node/" + tc.Id))
                                idts.Add(tc.Id, tc.Id = this.Nodes.GenerateNewKey());
                            else
                            {
                                tc.Id = ids["Node/" + tc.Id];
                                tc.IsIncident = true;
                            }
                        else
                        {
                            tc.Id = idts[tc.Id];
                            tc.IsIncident = true;
                        }
                    }
                }

                this.Nodes.Add(n);
            }

            foreach (var l in scope.InternalLinks.Select(i => (Link)i.Clone()))
            {
                l.Id = this.Links.GenerateNewKey();

                l.From = "Node/" + ids[l.From];
                l.To = "Node/" + ids[l.To];

                if (!string.IsNullOrWhiteSpace(l.TerminalFrom)) l.TerminalFrom = idts[l.TerminalFrom];
                if (!string.IsNullOrWhiteSpace(l.TerminalTo)) l.TerminalTo = idts[l.TerminalTo];

                this.Links.Add(l); 
            }

            foreach (var gr in scope.Groups.Select(i => (Group)i.Clone()))
            {
                gr.Id = this.Groups.GenerateNewKey();

                this.Groups.Add(gr);  
            }
        }

        public virtual void Rotate(NetScope scope, double angle)
        {
            Rotate(scope, angle, new double[] { (scope.Extent[0] + scope.Extent[2]) / 2, (scope.Extent[1] + scope.Extent[3]) / 2 });
        }

        public virtual void Rotate(NetScope scope, double angle, double[] center)
        {
            angle = angle % 360;

            foreach (var n in scope.Nodes)
            {
                if (n.Geometry != null && n.Geometry.Length > 0)
                {
                    Spatial.Geometry.Rotate(n.Geometry, center, angle);
                    n.Extent = _BuildExtent(n.Geometry[0]);
                }

                if (n.Position != null)
                {
                    double[] oldPosition = new double[] { n.Position[0], n.Position[1] };
                    Spatial.Geometry.Rotate(center, angle, n.Position);

                    if (n.Terminals != null)
                    {
                        foreach (var t in n.Terminals)
                        {
                            Spatial.Geometry.Move(n.Position[0] - oldPosition[0], n.Position[1] - oldPosition[1], t.Coordinates);
                            Spatial.Geometry.Rotate(n.Position, angle, t.Coordinates);
                        }

                        n.Extent = _BuildExtent(new Spatial.Geometry() { CoordinatesString = n.Terminals.Select(t => t.Coordinates).ToArray() });
                    }
                }

                n.Rotation += angle;

                if (n.Rotation > 360)
                    n.Rotation -= 360;

                if (n.Rotation < 0)
                    n.Rotation += 360;
            }

            foreach (var l in scope.InternalLinks)
            {
                Spatial.Geometry.Rotate(l.Geometry, center, angle);
                Spatial.Geometry.Rotate(center, angle, l.CoordinatesFrom);
                Spatial.Geometry.Rotate(center, angle, l.CoordinatesTo);
            }

            foreach (var l in scope.ExternalLinks)
            {
                if (l.Geometry != null && l.Geometry.Length > 0 && l.Geometry[0].CoordinatesString != null)
                {
                    foreach (var c in l.Geometry[0].CoordinatesString)
                    {
                        if (c[0] > scope.Extent[0] && c[0] < scope.Extent[2] && c[1] > scope.Extent[1] && c[1] < scope.Extent[3])
                        {
                            Spatial.Geometry.Rotate(center, angle, c);
                        }
                    }
                }
            }

            foreach (var gr in scope.Groups)
            {
                if (gr.Geometry != null && gr.Geometry.Length > 0)
                {
                    Spatial.Geometry.Rotate(gr.Geometry, center, angle);
                    gr.Extent = _BuildExtent(gr.Geometry[0]);
                }
            }
        }


        public virtual void Move(NetScope scope, double dx, double dy)
        {
            foreach (var n in scope.Nodes)
            {
                Spatial.Geometry.Move(n.Geometry, dx, dy);

                if (n.Position != null)
                    Spatial.Geometry.Move(dx, dy, n.Position);

                if (n.Terminals != null)
                {
                    foreach (var t in n.Terminals)
                        Spatial.Geometry.Move(dx, dy, t.Coordinates);
                }

                _MoveExtent(n.Extent, dx, dy);

            }

            foreach (var l in scope.InternalLinks)
            {
                Spatial.Geometry.Move(l.Geometry, dx, dy);
                Spatial.Geometry.Move(dx, dy, l.CoordinatesFrom);
                Spatial.Geometry.Move(dx, dy, l.CoordinatesTo);
            }

            foreach (var l in scope.ExternalLinks)
            {
                if (l.Geometry != null && l.Geometry.Length > 0 && l.Geometry[0].CoordinatesString != null) 
                {
                    foreach (var c in l.Geometry[0].CoordinatesString)
                    {
                        if (c[0] > scope.Extent[0] && c[0] < scope.Extent[2] && c[1] > scope.Extent[1] && c[1] < scope.Extent[3])
                        {
                            Spatial.Geometry.Move(dx, dy, c);
                        }
                    }
                }
            }

            foreach (var gr in scope.Groups)
            {
                Spatial.Geometry.Move(gr.Geometry, dx, dy);
                _MoveExtent(gr.Extent, dx, dy);
            }
        }

        public virtual void Move(NetScope scope, double[] from, double[] to)
        {
            Move(scope, to[0] - from[0], to[1] - from[1]);
        }


        public virtual void CompleteNode(Node node)
        {
            if (ZeroLengthLinksHandling == ZeroLengthLinksHandlingMode.ShareTerminals)
            {
                //Добавление узлов, которые были рядом ДО редактирования, в список на обновление
                if (node.Extent != null)
                {
                    foreach (var n in NodesWithin(node.Extent).Where(n => n.Id != node.Id))
                    {
                        AppendAffectedNode(n); 
                    }
                }
            }

            if (node.Geometry != null && node.Geometry.Length > 0)
            {
                node.Extent = _BuildExtent(node.Geometry[0]);
            }

            if (node.Position == null)
            {
                return;
            }

            var style = Styles.FindStyleFor(node);
            if (style != null && style.BindingPoint != null && style.TerminalPoints != null)
            {
                node.ConnectivityType = style.ConnectivityType;
                node.Connectivity = style.Connectivity;

                var dx = node.Position[0] - style.BindingPoint[0];
                var dy = node.Position[1] - style.BindingPoint[1];

                var tOldIds = new Dictionary<string, Terminal>(); 

                if (node.Terminals == null)
                {
                    var terminals = new List<Terminal>();
                    foreach (var t in style.TerminalPoints)
                    {
                        var coordinates = new double[] { t[0] + dx, t[1] + dy };
                        Spatial.Geometry.Rotate(node.Position, node.Rotation, coordinates);

                        terminals.Add(new Terminal()
                        {
                            Id = this.Nodes.GenerateNewKey(),
                            Coordinates = coordinates,
                        });
                    }

                    node.Terminals = terminals.ToArray();
                } 
                else
                {
                    tOldIds = node.Terminals.Where(t => !string.IsNullOrWhiteSpace(t.Id)).ToDictionary(t => t.Id, t => t);  

                    if (node.Terminals.Length != style.TerminalPoints.Length)
                    {
                        var old = node.Terminals;

                        var terminals = new List<Terminal>();
                        for (var i = 0; i < style.TerminalPoints.Length; i++)
                            terminals.Add(new Terminal());

                        node.Terminals = terminals.ToArray();
                        for (var i = 0; i < Math.Min(old.Length, node.Terminals.Length); i++)
                        {
                            node.Terminals[i].Id = old[i].Id;
                        }
                    }

                    for (var i = 0; i < node.Terminals.Length; i++)
                    {
                        var tp = style.TerminalPoints[i];
                        var t = node.Terminals[i]; 

                        t.Coordinates = new double[] { tp[0] + dx, tp[1] + dy };
                        Spatial.Geometry.Rotate(node.Position, node.Rotation, t.Coordinates);

                        if (string.IsNullOrWhiteSpace(t.Id))
                        {
                            t.Id = this.Nodes.GenerateNewKey(); 
                        }
                    }
                }

                //Формирование охвата по терминалам
                node.Extent = _BuildExtent(new Spatial.Geometry() { CoordinatesString = node.Terminals.Select(t => t.Coordinates).ToArray() });

                //Формирование нулевых связей
                if (this.ZeroLengthLinksHandling != ZeroLengthLinksHandlingMode.DoNothing)
                {
                    bool newNodeId = false;

                    if (string.IsNullOrWhiteSpace(node.Id))
                    {
                        node.Id = this.Nodes.GenerateNewKey();
                        newNodeId = true;
                    }

                    //Соприкасающиеся узлы
                    var nodesNear = OriginalInstances(NodesWithin(node.Extent).Where(n => n.Id != node.Id)).ToArray();
                    //Из них с терминалами
                    var nodesNearWithTerminals = nodesNear.Where(n => n.Terminals != null && n.Terminals.Length > 0);
                    //и без
                    var nodesNearWithoutTerminals = nodesNear.Except(nodesNearWithTerminals)
                        .Where(n => n.Extent != null && n.Geometry != null && n.Geometry.Length > 0 && n.Geometry[0].CoordinatesString != null) ;  

                    //Автоматические присоединение
                    foreach (var t in node.Terminals)
                    {
                        Func<Terminal, bool> fnt = (nt) => nt.Coordinates[0] == t.Coordinates[0] && nt.Coordinates[1] == t.Coordinates[1];

                        var inode = nodesNearWithTerminals
                                    .FirstOrDefault(n => n.Terminals.Any(fnt));

                        if (inode != null)
                        {
                            var it = inode.Terminals.First(fnt);

                            switch (ZeroLengthLinksHandling)
                            {
                                case ZeroLengthLinksHandlingMode.CreateLinks:
                                    {
                                        bool addLink = newNodeId
                                            || !tOldIds.ContainsKey(t.Id)
                                            || this.Links.DataSet.FirstOrDefault(l => (l.TerminalFrom == t.Id && l.TerminalTo == it.Id) || (l.TerminalFrom == it.Id && l.TerminalTo == t.Id)) == null;

                                        if (addLink)
                                        {
                                            Link newLink = new Link()
                                            {
                                                From = node.Id,
                                                To = inode.Id,
                                                TerminalFrom = t.Id,
                                                TerminalTo = it.Id,
                                                Geometry = new Spatial.Geometry[]
                                                {
                                                    new Spatial.Geometry {
                                                        GeometryType = "LineString",
                                                        CoordinatesString = new double[][] {
                                                            t.Coordinates,
                                                            it.Coordinates
                                                        }
                                                    }
                                                }
                                            };

                                            CompleteLink(newLink);

                                            Links.Add(newLink);
                                        }
                                        break;
                                    }

                                case ZeroLengthLinksHandlingMode.ShareTerminals:
                                    {
                                        if (string.Compare(node.Id, inode.Id, true) < 0)
                                        {
                                            t.Id = it.Id;
                                            t.IsIncident = true;
                                        }
                                        else
                                        {
                                            it.Id = t.Id;
                                            it.IsIncident = true;
                                        }

                                        AppendAffectedNode(inode);
                                        break;
                                    }
                            }
                        }

                        double x0 = t.Coordinates[0];
                        double y0 = t.Coordinates[1];

                        var inodes = nodesNearWithoutTerminals
                                     .Where(n => n.Extent[0] <= x0 && n.Extent[2] >= x0 && n.Extent[1] <= y0 && n.Extent[3] >= y0);

                        inode = null;
                        double?[] lcp = new double?[] { null, null }; 
                        foreach (var n in inodes)
                        {
                            var cs = n.Geometry[0].CoordinatesString;
                            for (var i = 1; i < cs.Length; i++)
                            {
                                double x1 = cs[i - 1][0];
                                double x2 = cs[i][0];

                                double y1 = cs[i - 1][1];
                                double y2 = cs[i][1];

                                if ((x2 != x1) && (x1 <= x0 && x2 >= x0 || x2 <= x0 && x1 >= x0))
                                {
                                    lcp[0] = x0;
                                    lcp[1] = (x0 == x1) ? y1 : (x0 == x2) ? y2 : y1 + (y2 - y1) * (x0 - x1) / (x2 - x1);

                                    if (lcp[0] == x0 && lcp[1] == y0)
                                    {
                                        inode = n;
                                        break;
                                    }
                                }

                                if ((y2 != y1) && (y1 <= y0 && y2 >= y0 || y2 <= y0 && y1 >= y0))
                                {
                                    lcp[0] = (y0 == y1) ? x1 : (y0 == y2) ? x2 : x1 + (x2 - x1) * (y0 - y1) / (y2 - y1);
                                    lcp[1] = y0;

                                    if (lcp[0] == x0 && lcp[1] == y0)
                                    {
                                        inode = n;
                                        break;
                                    }
                                }
                            }

                            if (inode != null)
                            {
                                break;
                            }
                        }

                        if (inode != null)
                        {
                            switch (ZeroLengthLinksHandling)
                            {
                                case ZeroLengthLinksHandlingMode.CreateLinks:
                                    {
                                        bool addLink = newNodeId
                                            || !tOldIds.ContainsKey(t.Id)
                                            || this.Links.DataSet.FirstOrDefault(l => (l.From == node.Id && l.To == inode.Id) || (l.From == inode.Id && l.To == node.Id)) == null;

                                        if (addLink)
                                        {
                                            Link newLink = new Link()
                                            {
                                                From = node.Id,
                                                To = inode.Id,
                                                TerminalFrom = t.Id,
                                                TerminalTo = null,
                                                Geometry = new Spatial.Geometry[]
                                                {
                                                    new Spatial.Geometry {
                                                        GeometryType = "LineString",
                                                        CoordinatesString = new double[][] {
                                                            t.Coordinates,
                                                            t.Coordinates
                                                        }
                                                    }
                                                }
                                            };

                                            CompleteLink(newLink);

                                            Links.Add(newLink);
                                        }
                                        break;
                                    }

                                case ZeroLengthLinksHandlingMode.ShareTerminals:
                                    {
                                        t.Id = inode.Id;
                                        t.IsIncident = true;
                                        AppendAffectedNode(inode);
                                        break;
                                    }
                            }
                        }
                    }
                }
            }
        }

        public virtual void CompleteLink(Link link)
        {
            link.From = string.Format("Node/{0}", link.From.Replace("Node/", ""));
            link.To = string.Format("Node/{0}", link.To.Replace("Node/", ""));

            if (link.Geometry == null || link.Geometry.Length == 0)
                return;

            link.CoordinatesFrom = link.Geometry[0].CoordinatesString[0];
            link.CoordinatesTo = link.Geometry[0].CoordinatesString[link.Geometry[0].CoordinatesString.Length - 1];
        }

        public virtual void CompleteGroup(Group group)
        {
            if (group.Geometry == null || group.Geometry.Length == 0)
                return;

            group.Extent = _BuildExtent(group.Geometry[0]);
        }

        public virtual void UpdateConnectedLinks(Node node)
        {
            if (node.Terminals == null)
                return;

            //connected links
            var clinks = new Dictionary<string, Link>();

            //node terminals dictionary
            var tcs = new Dictionary<string, double[]>();
            foreach (var t in node.Terminals)
            {
                double[] tc = new double[2];
                tc[0] = t.Coordinates[0];
                tc[1] = t.Coordinates[1];

                SchematicsModel.Spatial.Geometry.Rotate(node.Position, node.Rotation, tc);

                tcs.Add(t.Id, tc);
            }

            //source connected links
            var slinks = this.Links.DataSet.Where(l => l.From == "Node/" + node.Id);
            foreach (var l in slinks.ToList().Where(l => !string.IsNullOrWhiteSpace(l.TerminalFrom)))
            {
                var t = tcs.ContainsKey(l.TerminalFrom) ? tcs[l.TerminalFrom] : null;
                if (t != null && l.Geometry != null && l.Geometry.Length > 0 && l.Geometry[0].CoordinatesString != null && l.Geometry[0].CoordinatesString.Length > 0)
                {
                    var p = l.Geometry[0].CoordinatesString[0];
                    if (t[0] != p[0] || t[1] != p[1])
                    {
                        p[0] = t[0];
                        p[1] = t[1];

                        if (!clinks.ContainsKey(l.Id))
                            clinks.Add(l.Id, l);
                    }
                }
            }

            //destination connected links
            var dlinks = this.Links.DataSet.Where(l => l.To == "Node/" + node.Id);
            foreach (var l in dlinks.ToList().Where(l => !string.IsNullOrWhiteSpace(l.TerminalTo)))
            {
                var t = tcs.ContainsKey(l.TerminalTo) ? tcs[l.TerminalTo] : null;
                if (t != null && l.Geometry != null && l.Geometry.Length > 0 && l.Geometry[0].CoordinatesString != null && l.Geometry[0].CoordinatesString.Length > 0)
                {
                    var p = l.Geometry[0].CoordinatesString[l.Geometry[0].CoordinatesString.Length - 1];
                    if (t[0] != p[0] || t[1] != p[1])
                    {
                        p[0] = t[0];
                        p[1] = t[1];

                        if (!clinks.ContainsKey(l.Id))
                            clinks.Add(l.Id, l);
                    }
                }
            }

            foreach (var l in clinks.Select(i => i.Value))
                this.Links.Update(l);
        }


        public void ValidateDelete(Node node)
        {
            var clink = this.Links.DataSet.Where(l => l.From == "Node/" + node.Id || l.To == "Node/" + node.Id).FirstOrDefault();

            if (clink != null)
            {
                throw new InvalidOperationException("Удаление узла запрещено, поскольку имеются связи с другими узлами!");
            }
        }

        public void ValidateDelete(Link link)
        {
            return;
        }

        public void ValidateDelete(Group group)
        {
            return;
        }










        //Connectivity updates
        private short _GetConnectivityState(string stateId)
        {
            var state = Dictionary.States.DataSet.FirstOrDefault(i => i.Id == stateId);
            return (short)((state == null) ? 1 : (state.GraphStateId.HasValue) ? state.GraphStateId.Value : 1);
        }

        private void _InsertNode(Node node)
        {
            var state = _GetConnectivityState(node.State);
            var ct = node.ConnectivityType.HasValue ? node.ConnectivityType.Value : 0;  

            //Список вершин
            List<Vertex> v = new List<Vertex>();

            //Список ребер
            List<Edge> e = new List<Edge>();

            if (node.Position != null && node.Terminals != null)
            {
                //Специальная обработка для режима "Разделяемые терминалы"
                if (ZeroLengthLinksHandling == ZeroLengthLinksHandlingMode.ShareTerminals)
                {
                    //Если для терминала не было удаленной вершины, значит он был разделяемый и ему нужно установить новый Id
                    foreach (var t in node.Terminals)
                    {
                        var it = this.Vertices.ItemsToAdd.Union(this.Vertices.ItemsToUpdate).FirstOrDefault(i => i.Id == t.Id);
                        if (it != null && !t.IsIncident)
                        {
                            t.Id = Nodes.GenerateNewKey();
                        }
                    }
                }

                foreach (var t in node.Terminals.Where(t => !t.IsIncident))
                {
                    v.Add(new Vertex()
                    {
                        Id = t.Id,
                        ParentType = "node",
                        ParentId = node.Id,
                        Position = t.Coordinates,
                    });
                }

                //Формирование ребер
                switch (ct)
                {
                    case 0: //Последовательное соединение
                        {
                            var va = v.ToArray();
                            for (var i = 1; i < va.Length; i++)
                            {
                                e.Add(new Edge()
                                {
                                    ParentType = "node",
                                    ParentId = node.Id,
                                    From = "Vertex/" + va[i - 1].Id,
                                    To = "Vertex/" + va[i].Id,
                                    State = state
                                });
                            }

                            break;
                        }
                    case 1: //Звезда
                        {
                            var c = new Vertex()
                            {
                                Id = node.Id,
                                ParentType = "node",
                                ParentId = node.Id,
                                Position = node.Position,
                            };

                            foreach (var i in v)
                            {
                                e.Add(new Edge()
                                {
                                    ParentType = "node",
                                    ParentId = node.Id,
                                    From = "Vertex/" + c.Id,
                                    To = "Vertex/" + i.Id,
                                    State = state
                                });
                            }

                            v.Add(c);

                            break;
                        }
                    case 2: //Произвольный
                        {
                            if (node.Connectivity != null)
                            {
                                var va = v.ToArray();
                                foreach (var cn in node.Connectivity)
                                {
                                    if (cn.Length == 2)
                                    {
                                        if (cn[0] >= 0 && cn[0] < node.Terminals.Length && cn[1] >= 0 && cn[1] < node.Terminals.Length)
                                        {
                                            e.Add(new Edge()
                                            {
                                                ParentType = "node",
                                                ParentId = node.Id,
                                                From = "Vertex/" + va[cn[0]].Id,
                                                To = "Vertex/" + va[cn[1]].Id,
                                                State = state
                                            });
                                        }
                                    }
                                }
                            }

                            break;
                        }
                }
            }
            else
            {
                //Создание вершины по центру экстента
                v.Add(new Vertex()
                {
                    Id = node.Id,
                    ParentType = "node",
                    ParentId = node.Id,
                    Position = node.Extent != null ? new double[] { (node.Extent[0] + node.Extent[2]) / 2, (node.Extent[1] + node.Extent[3]) / 2 } : null
                });

                //Создание петли
                e.Add(new Edge()
                {
                    ParentType = "node",
                    ParentId = node.Id,
                    From = "Vertex/" + node.Id,
                    To = "Vertex/" + node.Id,
                    State = state
                });
            }


            foreach (var i in v)
                this.Vertices.AddOrUpdate(i);

            foreach (var i in e)
                this.Edges.AddOrUpdate(i);
        }

        private void _InsertLink(Link link)
        {
            this.Edges.AddOrUpdate(new Edge()
            {
                ParentType = "link",
                ParentId = link.Id,
                From = "Vertex/" + (string.IsNullOrWhiteSpace(link.TerminalFrom) ? link.From.Replace("Node/", "") : link.TerminalFrom),
                To = "Vertex/" + (string.IsNullOrWhiteSpace(link.TerminalTo) ? link.To.Replace("Node/", "") : link.TerminalTo),
                State = _GetConnectivityState(link.State)
            });
        }

        private void _DeleteNode(Node node)
        {
            _DeleteConnectivityOf(node.Id); 
        }

        private void _DeleteLink(Link link)
        {
            _DeleteConnectivityOf(link.Id); 
        }

        private void _DeleteConnectivityOf(string id)
        {
            foreach (var v in this.Vertices.DataSet.Where(i => i.ParentId == id).ToList())
                this.Vertices.Delete(v);

            foreach (var e in this.Edges.DataSet.Where(i => i.ParentId == id).ToList())
                this.Edges.Delete(e);
        }

        private void _DeleteGroup(Group g)
        {
            this.Groups.Detach(g);
            this.Groups.Delete(g);

            var size = (g.Extent[2] - g.Extent[0]) * (g.Extent[3] - g.Extent[1]);
            var gc = Copy(g.Extent);

            foreach (var n in gc.Nodes)
            {
                this.Nodes.Detach(n);
                this.Nodes.Delete(n);
            }

            foreach (var l in gc.InternalLinks)
            {
                this.Links.Detach(l);
                this.Links.Delete(l);
            }

            foreach (var gi in gc.Groups.Where(i => i.Id != g.Id && (i.Extent[2] - i.Extent[0]) * (i.Extent[3] - i.Extent[1]) < size))
            {
                _DeleteGroup(gi);
            }
        }

        private double[] _BuildExtent(Spatial.Geometry geometry)
        {
            var cs = (geometry.CoordinatesString == null)
                     ? (geometry.CoordinatesStrings == null || geometry.CoordinatesStrings.Length == 0) 
                        ? null 
                        : geometry.CoordinatesStrings[0]
                     : geometry.CoordinatesString;

            if (cs == null && cs.Length == 0)
            {
                return null;
            }

            var extent = new double?[] { null, null, null, null };

            foreach (var c in cs)
            {
                if (extent[0] == null || c[0] < extent[0])
                    extent[0] = c[0];

                if (extent[1] == null || c[1] < extent[1])
                    extent[1] = c[1];

                if (extent[2] == null || c[0] > extent[2])
                    extent[2] = c[0];

                if (extent[3] == null || c[1] > extent[3])
                    extent[3] = c[1];
            }

            return extent.Select(i => i.Value).ToArray();
        }

        private void _MoveExtent(double[] extent, double dx, double dy)
        {
            if (extent == null || extent.Length != 4)
                return;

            extent[0] += dx;
            extent[1] += dy;
            extent[2] += dx;
            extent[3] += dy;
        }

    }
}
