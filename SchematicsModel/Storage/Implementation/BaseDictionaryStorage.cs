﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Dictionary;

namespace SchematicsModel.Storage
{
    public abstract class BaseDictionaryStorage : IDictionaryStorage
    {
        private string _name;
        public string Name
        {
            get { return _name; }
        }


        public BaseDictionaryStorage(string name)
        {
            _name = name;
        }




        public abstract IStorageCollection<State> States { get; }

        public abstract IStorageCollection<NodeType> NodeTypes { get; }

        public abstract IStorageCollection<LinkType> LinkTypes { get; }

        public abstract IStorageCollection<GroupType> GroupTypes { get; }




        public void Commit()
        {
            this.States.Commit();
            this.NodeTypes.Commit();
            this.LinkTypes.Commit();
            this.GroupTypes.Commit();
        }

        public void Rollback()
        {
            this.States.Rollback();
            this.NodeTypes.Rollback();
            this.LinkTypes.Rollback();
            this.GroupTypes.Rollback();
        }
    }
}
