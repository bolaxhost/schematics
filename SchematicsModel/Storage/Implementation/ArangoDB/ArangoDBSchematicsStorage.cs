﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; 

using ArangoDB.Client;
using ArangoDB.Client.Data;

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBSchematicsStorage : ISchematicsStorage
    {
        internal IArangoDatabase GetStorage(string name)
        {
            var settings = new DatabaseSharedSetting()
            {
                Database = name,
                Url = _url,
                Credential = new NetworkCredential(_account, _password),
                SystemDatabaseCredential = new NetworkCredential(_account, _password),

            };

            using (var db = new ArangoDatabase(settings))
            {
                if (!db.ListDatabases().Contains(name))
                    db.CreateDatabase(settings.Database, new List<DatabaseUser>
                            {
                                new DatabaseUser
                                {
                                    Username = _account,
                                    Passwd = _password
                                }
                            });

                return db;
            }
        }



        private string _url;
        private string _account;
        private string _password;
        public ArangoDBSchematicsStorage(string url, string account, string password)
        {
            _url = url;
            _account = account;
            _password = password;
        }








        private Dictionary<string, INetStorage> _netStorages = new Dictionary<string, INetStorage>(); 
        public INetStorage GetNetStorage(string name, IDictionaryStorage dictionary, IStylesStorage styles)
        {
            if (_netStorages.ContainsKey(name))
                return _netStorages[name];

            var newStorage = new ArangoDBNetStorage(this, name) {
                Dictionary = dictionary,
                Styles = styles
            };

            _netStorages.Add(name, newStorage);

            return newStorage;
        }


        private Dictionary<string, IStylesStorage> _stylesStorages = new Dictionary<string, IStylesStorage>();
        public IStylesStorage GetStylesStorage(string name)
        {
            if (_stylesStorages.ContainsKey(name))
                return _stylesStorages[name];

            var newStorage = new ArangoDBStylesStorage(this, name);
            _stylesStorages.Add(name, newStorage);

            return newStorage;
        }




        private Dictionary<string, IDictionaryStorage> _dictionaryStorages = new Dictionary<string, IDictionaryStorage>();
        public IDictionaryStorage GetDictionaryStorage(string name)
        {
            if (_dictionaryStorages.ContainsKey(name))
                return _dictionaryStorages[name];

            var newStorage = new ArangoDBDictionaryStorage(this, name);
            _dictionaryStorages.Add(name, newStorage);

            return newStorage;
        }
    }
}
