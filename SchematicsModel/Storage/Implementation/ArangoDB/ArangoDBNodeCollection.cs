﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;
using ArangoDB.Client.Advanced;   

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBNodeCollection : ArangoDBStorageCollection<Node>
    {
        internal ArangoDBNodeCollection(IArangoDatabase storage) : base(storage, CollectionType.Document) { }

        internal override void AfterCreateCollection()
        {
            base.AfterCreateCollection();
            new AdvancedOperation(this.Storage).EnsureIndex(this.Name, new EnsureIndexData
            {
                Type = IndexType.Geo,
                Fields = new[] { "location" },
                GeoJson = true,
            });
        }
    }
}
