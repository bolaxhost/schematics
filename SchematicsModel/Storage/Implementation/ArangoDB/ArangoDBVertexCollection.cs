﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;
using ArangoDB.Client.Advanced;   

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBVertexCollection : ArangoDBStorageCollection<Vertex>
    {
        internal ArangoDBVertexCollection(IArangoDatabase storage) : base(storage, CollectionType.Document) { }
    }
}
