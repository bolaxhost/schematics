﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

using ArangoDB.Client;
using ArangoDB.Client.Data;

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBNodeTypeCollection : ArangoDBStorageCollection<NodeType>
    {
        internal ArangoDBNodeTypeCollection(IArangoDatabase storage) : base(storage) { }
    }
}
