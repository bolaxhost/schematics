﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

using ArangoDB.Client;
using ArangoDB.Client.Data;
using ArangoDB.Client.Graph; 

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBDictionaryStorage : BaseDictionaryStorage 
    {
        private IArangoDatabase _storage;

        private ArangoDBStateCollection _states;
        private ArangoDBNodeTypeCollection _nodeTypes;
        private ArangoDBLinkTypeCollection _linkTypes;
        private ArangoDBGroupTypeCollection _groupTypes;

        public ArangoDBDictionaryStorage(ArangoDBSchematicsStorage storage, string name) 
            : base(name)
        {
            _storage = storage.GetStorage(string.Format("{0}", name));

            _states = new ArangoDBStateCollection(_storage);
            _nodeTypes = new ArangoDBNodeTypeCollection(_storage);
            _linkTypes = new ArangoDBLinkTypeCollection(_storage);
            _groupTypes = new ArangoDBGroupTypeCollection(_storage);
        }









        public override IStorageCollection<State> States
        {
            get { return _states; }
        }

        public override IStorageCollection<NodeType> NodeTypes
        {
            get { return _nodeTypes; }
        }

        public override IStorageCollection<LinkType> LinkTypes
        {
            get { return _linkTypes; }
        }

        public override IStorageCollection<GroupType> GroupTypes
        {
            get { return _groupTypes; }
        }
    }
}
