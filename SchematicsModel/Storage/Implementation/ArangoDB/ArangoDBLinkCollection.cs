﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBLinkCollection : ArangoDBStorageCollection<Link>
    {
        internal ArangoDBLinkCollection(IArangoDatabase storage) : base(storage, CollectionType.Edge) { }
    }
}
