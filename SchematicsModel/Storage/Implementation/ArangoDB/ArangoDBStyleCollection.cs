﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Styles;

using ArangoDB.Client;
using ArangoDB.Client.Data;  

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBStyleCollection : ArangoDBStorageCollection<Style>
    {
        internal ArangoDBStyleCollection(IArangoDatabase storage) : base(storage, CollectionType.Document) { }
    }
}
