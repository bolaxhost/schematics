﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBGroupCollection : ArangoDBStorageCollection<Group>
    {
        internal ArangoDBGroupCollection(IArangoDatabase storage) : base(storage, CollectionType.Document) { }
    }
}
