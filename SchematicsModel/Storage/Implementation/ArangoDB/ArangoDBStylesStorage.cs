﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;
using ArangoDB.Client.Graph; 

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBStylesStorage : BaseStylesStorage 
    {
        private IArangoDatabase _storage;

        private ArangoDBStyleCollection _styles;

        public ArangoDBStylesStorage(ArangoDBSchematicsStorage storage, string name) 
            : base(name)
        {
            _storage = storage.GetStorage(string.Format("{0}", name));

            _styles = new ArangoDBStyleCollection(_storage);
        }




        




        public override IStorageCollection<Styles.Style> Styles
        {
            get { return _styles; }
        }
    }
}
