﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;
using ArangoDB.Client.Graph; 

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBNetStorage : BaseNetStorage 
    {
        private IArangoDatabase _storage;

        private ArangoDBNodeCollection _nodes;
        private ArangoDBLinkCollection _links;
        private ArangoDBGroupCollection _groups;
        private ArangoDBVertexCollection _vertices;
        private ArangoDBEdgeCollection _edges;

        public ArangoDBNetStorage(ArangoDBSchematicsStorage storage, string name) 
            : base(name)
        {
            _storage = storage.GetStorage(string.Format("{0}", name));

            _nodes = new ArangoDBNodeCollection(_storage);
            _links = new ArangoDBLinkCollection(_storage);
            _groups = new ArangoDBGroupCollection(_storage);
            _vertices = new ArangoDBVertexCollection(_storage);
            _edges = new ArangoDBEdgeCollection(_storage);

            //Net
            if (!_storage.ListGraphs().Any(i => i.Key == "Net"))
            {
                var graph = new ArangoGraph(_storage, "Net");
                graph.Create(new List<EdgeDefinitionTypedData> 
                {
                    new EdgeDefinitionTypedData 
                    {
                         Collection = typeof(Link),
                         From = new List<System.Type> { typeof(Node) },
                         To = new List<System.Type> { typeof(Node) }
                    }
                });
            }

            //Connectivity
            if (!_storage.ListGraphs().Any(i => i.Key == "Connectivity"))
            {
                var graph = new ArangoGraph(_storage, "Connectivity");
                graph.Create(new List<EdgeDefinitionTypedData> 
                {
                    new EdgeDefinitionTypedData 
                    {
                         Collection = typeof(Edge),
                         From = new List<System.Type> { typeof(Vertex) },
                         To = new List<System.Type> { typeof(Vertex) }
                    }
                });
            }
        }




        




        public override IStorageCollection<Net.Node> Nodes
        {
            get { return _nodes; }
        }

        public override IStorageCollection<Net.Link> Links
        {
            get { return _links; }
        }

        public override IStorageCollection<Net.Group> Groups
        {
            get { return _groups; }
        }

        public override IStorageCollection<Net.Vertex> Vertices
        {
            get { return _vertices; }
        }

        public override IStorageCollection<Net.Edge> Edges
        {
            get { return _edges; }
        }





        public override IQueryable<Node> NodesWithin(double lon, double lat, double radius)
        {
            return this._storage.Collection<Node>().Within(lat, lon, radius).AsEnumerable().AsQueryable();
        }
    }
}
