﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using ArangoDB.Client;
using ArangoDB.Client.Data;

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBStorageCollection<T> : BaseStorageCollection<T>
        where T : BaseElement, new()
    {
        CollectionType _type = CollectionType.Document; 


        private IArangoDatabase _storage;
        internal IArangoDatabase Storage
        {
            get
            {
                return _storage;
            }
        }



        string _name = "";
        internal string Name
        {
            get
            {
                return _name;
            }
        }
               


        internal ArangoDBStorageCollection(IArangoDatabase storage, CollectionType type = CollectionType.Document)
        {
            _storage = storage;
            _type = type;
            _name = typeof(T).Name;

            if (!_storage.ListCollections().Any(i => i.Name == Name))
            {
                _storage.CreateCollection(Name, type: _type);
                AfterCreateCollection();
            }

            string keyGeneratorColName = typeof(LongKeyGeneratorValue).Name;
            if (!_storage.ListCollections().Any(i => i.Name == keyGeneratorColName))
            {
                _storage.CreateCollection(keyGeneratorColName, type: CollectionType.Document);
            }

            this.KeyGenerator = () => 
            {
                var key = Storage.Query<LongKeyGeneratorValue>().FirstOrDefault();                
                if (key == null)
                {
                    key = new LongKeyGeneratorValue() { CurrentValue = 1 };
                    Storage.Insert<LongKeyGeneratorValue>(key);
                }
                else
                {
                    key.CurrentValue++;
                    Storage.Update<LongKeyGeneratorValue>(key);
                }

                return key.CurrentValue.ToString();
            };
        }

        internal virtual void AfterCreateCollection()
        {

        }





        private IQueryable<T> _dataSet = null; 
        public override IQueryable<T> DataSet
        {
            get 
            {
                return _dataSet?? (_dataSet = Storage.Query<T>());
            }
        }
        public override T Find(string Id)
        {
            return this.Storage.Document<T>(Id);  
        }








        protected override void DoInsert(T item)
        {
            Storage.Insert<T>(item);   
        }

        protected override void DoUpdate(T item)
        {
            Storage.UpdateById<T>(item.Id, item);  
        }

        protected override void DoDelete(T item)
        {
            Storage.Remove<T>(item);  
        }
    }
}
