﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

using ArangoDB.Client;
using ArangoDB.Client.Data;

namespace SchematicsModel.Storage.ArangoDB
{
    public class ArangoDBLinkTypeCollection : ArangoDBStorageCollection<LinkType>
    {
        internal ArangoDBLinkTypeCollection(IArangoDatabase storage) : base(storage) { }
    }
}
