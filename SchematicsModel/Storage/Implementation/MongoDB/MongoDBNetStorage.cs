﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBNetStorage : BaseNetStorage 
    {
        private IMongoDatabase _storage;

        private MongoDBNodeCollection _nodes;
        private MongoDBLinkCollection _links;
        private MongoDBGroupCollection _groups;
        private MongoDBVertexCollection _vertices;
        private MongoDBEdgeCollection _edges;

        public MongoDBNetStorage(MongoDBSchematicsStorage storage, string name) 
            : base(name)
        {
            _storage = storage.GetStorage(string.Format("{0}", name));

            _nodes = new MongoDBNodeCollection(_storage);
            _links = new MongoDBLinkCollection(_storage);
            _groups = new MongoDBGroupCollection(_storage);
            _vertices = new MongoDBVertexCollection(_storage);
            _edges = new MongoDBEdgeCollection(_storage);
        }




        




        public override IStorageCollection<Net.Node> Nodes
        {
            get { return _nodes; }
        }

        public override IStorageCollection<Net.Link> Links
        {
            get { return _links; }
        }

        public override IStorageCollection<Net.Group> Groups
        {
            get { return _groups; }
        }

        public override IStorageCollection<Net.Vertex> Vertices
        {
            get { return _vertices; }
        }

        public override IStorageCollection<Net.Edge> Edges
        {
            get { return _edges; }
        }
    }
}
