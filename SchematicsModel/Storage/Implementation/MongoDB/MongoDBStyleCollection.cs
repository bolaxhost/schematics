﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Styles;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBStyleCollection : MongoDBStorageCollection<Style>
    {
        internal MongoDBStyleCollection(IMongoDatabase storage) : base(storage) { }
    }
}
