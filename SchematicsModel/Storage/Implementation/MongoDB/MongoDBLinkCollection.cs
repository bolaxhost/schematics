﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBLinkCollection : MongoDBStorageCollection<Link>
    {
        internal MongoDBLinkCollection(IMongoDatabase storage) : base(storage) { }
    }
}
