﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBStylesStorage : BaseStylesStorage 
    {
        private IMongoDatabase _storage;

        private MongoDBStyleCollection _styles;

        public MongoDBStylesStorage(MongoDBSchematicsStorage storage, string name) 
            : base(name)
        {
            _storage = storage.GetStorage(string.Format("{0}", name));

            _styles = new MongoDBStyleCollection(_storage);
        }




        




        public override IStorageCollection<Styles.Style> Styles
        {
            get { return _styles; }
        }
    }
}
