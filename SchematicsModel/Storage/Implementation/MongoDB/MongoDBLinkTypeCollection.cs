﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBLinkTypeCollection : MongoDBStorageCollection<LinkType>
    {
        internal MongoDBLinkTypeCollection(IMongoDatabase storage) : base(storage) { }
    }
}
