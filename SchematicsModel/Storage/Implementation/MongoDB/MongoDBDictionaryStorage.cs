﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBDictionaryStorage : BaseDictionaryStorage 
    {
        private IMongoDatabase _storage;

        private MongoDBStateCollection _states;
        private MongoDBNodeTypeCollection _nodeTypes;
        private MongoDBLinkTypeCollection _linkTypes;
        private MongoDBGroupTypeCollection _groupTypes;

        public MongoDBDictionaryStorage(MongoDBSchematicsStorage storage, string name) 
            : base(name)
        {
            _storage = storage.GetStorage(string.Format("{0}", name));

            _states = new MongoDBStateCollection(_storage);
            _nodeTypes = new MongoDBNodeTypeCollection(_storage);
            _linkTypes = new MongoDBLinkTypeCollection(_storage);
            _groupTypes = new MongoDBGroupTypeCollection(_storage);
        }









        public override IStorageCollection<State> States
        {
            get { return _states; }
        }

        public override IStorageCollection<NodeType> NodeTypes
        {
            get { return _nodeTypes; }
        }

        public override IStorageCollection<LinkType> LinkTypes
        {
            get { return _linkTypes; }
        }

        public override IStorageCollection<GroupType> GroupTypes
        {
            get { return _groupTypes; }
        }
    }
}
