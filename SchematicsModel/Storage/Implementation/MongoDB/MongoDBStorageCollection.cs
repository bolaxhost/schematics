﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Net;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBStorageCollection<T> : BaseStorageCollection<T>
        where T : BaseElement, new()
    {
        private IMongoDatabase _storage;
        internal IMongoDatabase Storage
        {
            get
            {
                return _storage;
            }
        }



        string _name = "";
        internal string Name
        {
            get
            {
                return _name;
            }
        }


        IMongoCollection<T> _collection = null; 
        internal MongoDBStorageCollection(IMongoDatabase storage)
        {
            _storage = storage;
            _name = typeof(T).Name;

            if (!_storage.ListCollections(new ListCollectionsOptions { Filter = new BsonDocument("name", _name) }).Any())
            {
                _storage.CreateCollection(Name);
                AfterCreateCollection();
            }

            _collection = _storage.GetCollection<T>(_name);
        }

        internal virtual void AfterCreateCollection()
        {

        }





        private IQueryable<T> _dataSet = null; 
        public override IQueryable<T> DataSet
        {
            get 
            {
                return _dataSet?? (_dataSet = _collection.AsQueryable());
            }
        }
        public override T Find(string Id)
        {
            return _collection.Find(i => i.Id == Id).FirstOrDefault();  
        }








        protected override void DoInsert(T item)
        {
            _collection.InsertOne(item);
        }

        protected override void DoUpdate(T item)
        {
            _collection.ReplaceOne(Builders<T>.Filter.Eq(i => i.Id, item.Id), item); 
        }

        protected override void DoDelete(T item)
        {
            _collection.DeleteOne(Builders<T>.Filter.Eq(i => i.Id, item.Id));    
        }    
    }
}
