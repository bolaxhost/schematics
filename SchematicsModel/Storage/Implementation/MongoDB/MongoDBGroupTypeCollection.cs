﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchematicsModel.Dictionary;

using MongoDB.Bson;
using MongoDB.Driver;

namespace SchematicsModel.Storage.MongoDB
{
    public class MongoDBGroupTypeCollection : MongoDBStorageCollection<GroupType>
    {
        internal MongoDBGroupTypeCollection(IMongoDatabase storage) : base(storage) { }
    }
}
