﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using SchematicsModel.Net;

namespace SchematicsModel.Storage
{
    public class NetScope
    {
        [JsonProperty("nodes")]
        public Node[] Nodes { get; set; }

        [JsonProperty("internalLinks")]
        public Link[] InternalLinks { get; set; }

        [JsonProperty("externalLinks")]
        public Link[] ExternalLinks { get; set; }
        
        [JsonProperty("groups")]
        public Group[] Groups { get; set; }

        [JsonProperty("extent")]
        public double[] Extent { get; set; }
    }
}
