﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Dictionary;

namespace SchematicsModel.Storage
{
    public interface IDictionaryStorage
    {
        string Name { get; }

        IStorageCollection<State>  States { get; }
        IStorageCollection<NodeType> NodeTypes { get; }
        IStorageCollection<LinkType> LinkTypes { get; }
        IStorageCollection<GroupType> GroupTypes { get; }

        void Commit();
        void Rollback();
    }
}
