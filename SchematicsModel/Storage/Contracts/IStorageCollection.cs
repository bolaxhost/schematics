﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Net;

namespace SchematicsModel.Storage
{
    public interface IStorageCollection<T>
        where T : BaseElement 
    {
        Func<string> KeyGenerator { get; set; }
        string GenerateNewKey();


        IQueryable<T> DataSet { get; }       
        T Find(string Id);
        void Add(T item);
        void Update(T item);
        void Delete(T item);
        void Detach(T item);
        void AddOrUpdate(T item);
        void Commit();
        void Rollback();

        IEnumerable<T> ItemsToAdd { get; }
        IEnumerable<T> ItemsToUpdate { get; }
        IEnumerable<T> ItemsToDelete { get; } 
    }
}
