﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Styles;

namespace SchematicsModel.Storage
{
    public interface IStylesStorage
    {
        string Name { get; }
        IStorageCollection<Style>  Styles { get; }

        Style FindStyleFor(SchematicsElement element);
        Style[] FindStylesFor(SchematicsElement element);

        void Commit();
        void Rollback();
    }
}
