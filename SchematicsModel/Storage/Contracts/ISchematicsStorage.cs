﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Net;
using SchematicsModel.Styles;
using SchematicsModel.Dictionary;

namespace SchematicsModel.Storage
{
    public interface ISchematicsStorage
    {
        INetStorage GetNetStorage(string name, IDictionaryStorage dictionary, IStylesStorage styles);
        IStylesStorage GetStylesStorage(string name);
        IDictionaryStorage GetDictionaryStorage(string name);
    }
}
