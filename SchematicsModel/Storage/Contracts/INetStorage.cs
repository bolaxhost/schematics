﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SchematicsModel.Net;

namespace SchematicsModel.Storage
{
    public enum ZeroLengthLinksHandlingMode
    {
        DoNothing,
        CreateLinks,
        ShareTerminals,
    }

    public interface INetStorage
    {
        string Name { get; }
        ZeroLengthLinksHandlingMode ZeroLengthLinksHandling { get; set; }

        IDictionaryStorage Dictionary { get; set; }
        IStylesStorage Styles { get; set; }

        IStorageCollection<Node>  Nodes { get; }
        IStorageCollection<Link> Links { get; }
        IStorageCollection<Group> Groups { get; }
        IStorageCollection<Vertex> Vertices { get; }
        IStorageCollection<Edge> Edges { get; }

        IQueryable<Node> NodesWithin(double lon, double lat, double radius);
        IQueryable<Node> NodesWithin(double[] extent);
        IQueryable<Link> LinksWithin(double[] extent);
        IQueryable<Group> GroupsWithin(double[] extent);

        NetScope Copy(double[] extent);
        NetScope Copy(string groupId);
        void Paste(NetScope scope);
        void Move(NetScope scope, double dx, double dy);
        void Move(NetScope scope, double[] from, double[] to);
        void Rotate(NetScope scope, double angle);
        void Rotate(NetScope scope, double angle, double[] center);

        void CompleteNode(Node node);
        void CompleteLink(Link link);
        void CompleteGroup(Group group);

        void ValidateDelete(Node node);
        void ValidateDelete(Link link);
        void ValidateDelete(Group group);

        void UpdateConnectedLinks(Node node);

        void Commit();
        void Rollback();
    }
}
